

      subroutine ialpop

C +------------------------------------------------------------------------+
C | ICE ALGAE POPULATION                                 07-06-2018-AT MAR |
C |   SubRoutine ialpop is used to calculate population size of ice        |
C |      algae community on bare ice sectors.                              |
C |                                                                        |
C |    Hard-coded for mosaic pixel == 1                                    |
C +------------------------------------------------------------------------+

      implicit none


C +--Global Variables
C +  ================    

      include 'MARCTR.inc'
      include 'MARphy.inc'
      include 'MARdim.inc'
      include 'MARgrd.inc' ! dt
      include 'MAR_GE.inc' ! Timings
C      include 'MAR_DY.inc' ! air tmperature
C      include 'MAR_RA.inc' ! SWD radiation
C      include 'MAR_HY.inc' ! For rain
C      include 'MAR_CA.inc' ! For convective rain
      include 'MAR_SV.inc'
      include 'MARsSN.inc'
      include 'MARxSV.inc' ! SISVAT px indexes
      include 'MARySV.inc'

!$OMP threadprivate(/xSISVAT_I/,/xSISVAT_R/,/ySISVAT_I/,/ySISVAT_R/)


C +--Local  Variables
C +  ================

      integer ikl, kk, isn
      real    tth1, tth2
      real    tihial
      integer index_ice
      real    snh1, snh2
      real    today_growth, prod_rate, prod_hrs, new_pop, pop_loss

      integer run_hr
      real    SnHmax, SWdmin, popmin, mltmin, ploss
      data    run_hr / 12      / ! Hour of day at which to update algae population
      data    SnHmax / 0.1    / ! Maximum snow height beneath which algal growth can occur
      data    SWdmin / 50      / ! Minimum SWD value required for sufficient photosynthetically-active-radiation
      data    mltmin / 1.5     / ! Minimum melting (mmWE) required in hour to count as melting.
      data    ploss /  0.11    /
c      data    popmin / 179.457 / ! Minimum/starting population size ng/ml DW
      !data    popmin / 2000.   / ! Minimum/starting population size ng/ml DW

      ! See also hard-coded logistic growth rate curve; see [REF] for further details of derivation.

C       write(6,*) 'iterun: ', iterun
C       write(6,*) 'time: ', jhlrGE(ii__SV(1),jj__SV(1))
C       write(6,*) 'ii:',ii__SV,'jj:',jj__SV


C +--Initialise min population if start of experiment
C +  ================================================
C      if (itexpe .le. 1) then
C         do ikl=1,klonv
C            biomsv(ikl) = popmin
C         enddo
C      endif



C +--Initialise variables if start of chunk
C +  ======================================
      if (iterun .le. 1) then

          do ikl=1,klonv

            iamssv(ikl) = 0
            iapssv(ikl) = 0
            iarssv(ikl) = 0
            ialdsv(ikl) = 0
            ialtsv(ikl) = itexpe
            
            iamcsv(ikl) = 0
            iapcsv(ikl) = 0

          enddo

      endif
       


C +-- Sum up values
C +   =============

      do ikl=1,klonv

        ! Surface melting
        iamssv(ikl) = iamssv(ikl) + wem_SV(ikl)   

        ! Shortwave radiation
        iapssv(ikl) = iapssv(ikl) + sol_SV(ikl)
      
      enddo



C +-- Calculate hourly values
C +   =======================
      ikl = 1
      if ( (jhlrSV(ikl) .gt. ialhsv(ikl)) .or. 
     .   (jhlrSV(ikl) .eq. zero .and. 
     .     ialhsv(ikl) .eq. 23) )  then

          do ikl=1,klonv

          ! Identify whether melting above threshold occurred
          tth1 = int(max(zero, sign(unun, ((iamssv(ikl)*-1)-mltmin) )))
          iamcsv(ikl) = iamcsv(ikl) + tth1
          iamssv(ikl) = 0
         
          ! Identify whether SWd was gt threshold required to be PAR
          tth1 = (iapssv(ikl) / real(itexpe-ialtsv(ikl))) - SWdmin
          tth2 = int(max(zero, sign(unun,tth1)))
          iapcsv(ikl) = iapcsv(ikl) + tth2
          iapssv(ikl) = 0

          ! Update time-of-calculation stores
          ialhsv(ikl) = jhlrSV(ikl)
          ialtsv(ikl) = itexpe

        enddo

      endif



C +-- Update algae population once per day
C +   ====================================
      ikl = 1
      if ( (jhlrSV(ikl) .eq. run_hr) .and.          ! Time now == desired time of day to run model
     .  ((njyrGE(mmarGE)+jdarGE) .gt. ialdsv(ikl))  ! Day now is later than the last day of model run
     .   .or. 
     .  (((jdarGE+njyrGE(mmarGE)) .eq. 1) .and.                         ! Start of a new year
     .     (ialdsv(ikl) .gt. 1)) ) then             ! Yesterday was in previous year

C          tihial = jhurGE+real(minuGE)/100.+real(jsecGE)/10000.

          do ikl=1,klonv

            ! Take popmin from the input start population mask
            popmin = ialmSV(ikl)

            ! Before trying to compute growth, make sure population 
            ! is at least at minimum specified size.
            ! (This is a fail-safe that should never be needed)
            biomsv(ikl) = max(popmin, biomsv(ikl))

            ! Calculate snow thickness at this pixel
            snh1 = 0
            index_ice = 1
            do isn=1, isnoSV(ikl)
              if(ro__SV(ikl,isn) .ge. ro_Ice) index_ice = isn
            enddo
            if (index_ice < isnoSV(ikl)) then
              do isn=index_ice, isnoSV(ikl)
                snh1 = snh1 + dzsnSV(ikl,isn)
              enddo
            endif

            ! Is snow thicker than tolerable threshold?
            snh2 = max(zero, (sign(unun, snh1 - SnHmax) * -1) )
               ! -- snh2 now =0 if snow present above threshold, =1 if bare/below threshold

            prod_hrs = min( iamcsv(ikl), iapcsv(ikl) )
     .           * snh2 ! No productive hours if snow depth > threshold

            ! Calculate productivity rate as a function of existing population
            prod_rate = 2936.966 / (1 + ((2936.966-796.239) / 796.239) 
     .           * exp(-0.000232 * biomsv(ikl) ) )

            today_growth = (prod_rate / 24) * prod_hrs
            today_growth = max(zero, today_growth)

            ! Calculate population loss
            pop_loss = biomsv(ikl) * ploss

            ! Accumulate growth and loss
            new_pop = max((biomsv(ikl)+today_growth-pop_loss), popmin)
            
            ! Save back to biomass store
            biomsv(ikl) = new_pop

            ! Reset counters
            iamcsv(ikl) = 0
            iapcsv(ikl) = 0
        
            ! Update flag to show when algal production last run
            ialdsv(ikl) = njyrGE(mmarGE)+jdarGE

        enddo

      endif


      return
      end
