 
 
      subroutine OUTice
 
C +------------------------------------------------------------------------+
C | MAR OUTPUT                                              30-11-2021 MAR |
C |   SubRoutine OUTice is used to initialize                              |
C |                            and perform Ouput of Surface Mass Balance   |
C |                            (Netcdf files)                              |
C |                                                                        |
C |   CAUTION: the Estimation of the Optical Depth codIB is obsolescent    |
C |   ^^^^^^^                                                              |
C +------------------------------------------------------------------------+
 
 
      IMPLICIT NONE
 
 
C +--Global Variables
C +  ================
 
 
      include 'MARCTR.inc'
      include 'MARphy.inc'
      include 'MARdim.inc'
      include 'MARgrd.inc'
      include 'MAR_GE.inc'
      include 'MAR_DY.inc'
      include 'MAR_RA.inc'
      include 'MAR_SL.inc'
      include 'MAR_SV.inc'
      include 'MAR_TV.inc'
      include 'MAR_HY.inc'
      include 'MAR_CA.inc'
      include 'MARsSN.inc'
      include 'MAR_IB.inc'
      include 'MARsIB.inc'
      include 'MAR_WK.inc'
      include 'MAR_IO.inc'
      include 'MARdSV.inc'
      include 'NetCDF.inc'
      include 'MAR_TE.inc'
      include 'MAR_AO.inc'
      include 'MAR0SV.inc'
      include 'MARial.inc' !*AJT*
 
C +--Local  Variables
C +  ================
 
      integer    Lfnam,     Ltit,     Luni,     Lnam,     Llnam
      PARAMETER (Lfnam= 40, Ltit= 90, Luni= 90, Lnam= 13, Llnam=50)
C +...Length of char strings
 
      integer    NdimNC_ice
      PARAMETER (NdimNC_ice = 12)
C +...Number of defined spatial dimensions (exact)
 
      integer    MXdim
      PARAMETER (MXdim = 20000)
C +...Maximum Number of all dims: recorded Time Steps
C +   and also maximum of spatial grid points for each direction.
 
      integer    MX_var
      PARAMETER (MX_var = 200)
C +...Maximum Number of Variables
C +
      integer    NattNC_ice
      PARAMETER (NattNC_ice = 1)
C +...Number of real attributes given to all variables
 
      real              yearNC_ice(MXdim)
      real              dateNC_ice(MXdim)
      real              timeNC_ice(MXdim)
      real              VALdim(MXdim,0:NdimNC_ice)
      real              tmp(3),tmp1z(mz),tmp2z(mz),tmp3,tmp4
      real              znsn1(nsno+1),znsn2(nsno+1),snwae(nsno+1)
      real              tmp1_OK,tmp2_OK,tmp3_OK,avlwc,factim
 
      real              xyllx1(mx,my,llx)   ,xyllx2(mx,my,llx)
      real              xyllx3(mx,my,llx)   ,xyllx4(mx,my,llx)
      real              xymi1  (mx,my,mi)   ,xymi2  (mx,my,mi)
      real              xymi3  (mx,my,mi)   ,xymi4  (mx,my,mi)
      real              xymi5  (mx,my,mi)   ,xymi6  (mx,my,mi)
      real              xymi7  (mx,my,mi)
      real              xynsno1(mx,my,nsno) ,xynsno2(mx,my,nsno)
      real              xynsno3(mx,my,nsno) ,xynsno4(mx,my,nsno)
      real              xynsno5(mx,my,nsno) ,xynsno6(mx,my,nsno)
      real              xynsno7(mx,my,nsno) ,xynsno8(mx,my,nsno)
      real              xynsx0 (mx,my,nsx)
      real              xynsx1 (mx,my,nsx)  ,xynsx2 (mx,my,nsx)
      real              xynsx3 (mx,my,nsx)  ,xynsx4 (mx,my,nsx)
      real              xynsx5 (mx,my,nsx)  ,xynsx6 (mx,my,nsx)
      real              xynsx7 (mx,my,nsx)  ,xymlhh (mx,my,mlhh)
      real              xynsx8 (mx,my,nsx)  ,xynsx9 (mx,my,nsx)
      real              xynsx10(mx,my,nsx)  ,xynsx11(mx,my,nsx)
      real              xynsx12(mx,my,nsx)  ,xynsx13(mx,my,nsx)
      real              xynsx14(mx,my,nsx)
      real              depthsnow(nsno),depthSNo,dater
      real              pLev,pUp,pDown,pMiddle,distUp
      real              zLev,zUp,zDown,zMiddle
      real              q,qst,r,rst,rh,qsat0D
 
      integer           nDFdim  (0:NdimNC_ice)
      integer            NvatNC_ice(NattNC_ice)
      integer             dayNC_ice(MXdim)
      integer           monthNC_ice(MXdim),RCODE
      integer           n1000 ,n100a ,n100  ,n10_a ,n10   ,n1    ,m10
      integer           n     ,jd10  ,jd1   ,nk    ,kk    ,nx
      integer           it    ,month  ,mill  ,iu
      integer           iSBLmx(mz),jSBLmx(mz)
      integer           ID__nc_ice,itotNC_ice,NtotNC_ice
      integer           dt_ICE,nbr_dt_ICE,dt_ICE2,index,njmo
      integer           kp,kz,kUp,kDown,kMiddle,OutdyIB0
 
      character*(Lfnam) fnamNC_ice,fnamNC_ics,fnamNC_tmp
      character*(Lnam)  NAMdim(0:NdimNC_ice)
      character*(Luni)  UNIdim(0:NdimNC_ice)
      character*(Lnam)  SdimNC_ice(4,MX_var)
      character*(Luni)  unitNC_ice(MX_var)
      character*(Lnam)  nameNC_ice(MX_var)
      character*(Llnam) lnamNC_ice(MX_var)
      character*(Ltit ) tit_NC_ice
      character*(Lnam)  NAMrat(NattNC_ice)
      character*120     tmpINP
      character*1       sector
      integer*8         date,date0
      integer           iyrrIB,mmarIB,jdarIB,jhurIB,minuIB
 
      common/OUTice_r/  yearNC_ice,dateNC_ice
      common/OUTice_i/  dt_ICE,dt_ICE2,nDFdim,OutdyIB0,
     .                  iyrrIB,mmarIB,jdarIB,jhurIB,minuIB
      common/OUTice_i8/ date,date0
      common/OUTice_c/  fnamNC_ice,fnamNC_ics
 
      real rhh,num,den
      integer i_hi,i_lo
      real*8 refrac_h,refrac_w
      real p_hi,p_lo,z_hi,z_lo,kappa
 
      real,parameter :: k1p = 7.76e-7
      real,parameter :: k2p = 3.73e-3
      real,parameter :: Md = 28.9644
      real,parameter :: Mv = 18.0153
      real,parameter :: E = Mv/Md
 
      ! for having a ICS*.nc file with snapshots
      ! when ICE*.nc is written
 
      logical,parameter :: snapshot=.false.
      integer           :: ss
 
C ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C ++ 1. Initialisation  ++++++++++++++++++++++++++++++++++++++++++++++++++++
C ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 
 
      ID__nc_ice = -1 ! NetCDF File is not open
 
      IF   (itexpe.le.1)                                          THEN
        DO k=1,nsx
        DO j=1,my
        DO i=1,mx
          wei0IB (i,j,k)    = 0.   ! Bottom Ice added
          wee_IB (i,j,k,:)  = 0.   ! Evapo/subli
          wem_IB (i,j,k)    = 0.   ! Melting
          wer_IB (i,j,k)    = 0.   ! Refreezing
          weu_IB (i,j,k)    = 0.   ! Run-off
          weo_IB (i,j,k,:)  = 0.   ! Run-off
          weacIB (i,j,k)    = 0.
          weerIB (i,j,k)    = 0.
        ENDDO
 
        ENDDO
        ENDDO
      END IF
 
 
      IF   (iterun.le.1)                                          THEN
 
 
C +--1.1 Initialization of all variables
C +  ===================================
 
 
                   ss=0
      if(snapshot) ss=1
 
800   continue
 
        itrdIB              = 0
        dt_ICE2             = 0
        timehIB             = 0.
        OutdyIB0            = min(1,max(0,OutdyIB-1))
        iyrrIB              = 0
 
        DO k=1,nsx
        DO j=1,my
        DO i=1,mx
 
          wet_IB (i,j,k)    = 0.   ! Total Mass Balance
          wec0IB (i,j,k)    = CaWaTV(i,j,k) ! canopy water content
          wel0IB (i,j,k)    = 0.   ! soil   water content
          wee_IB (i,j,k,:)  = 0.   ! Evapo/Subli
          wee0IB (i,j,k,:)  = 0.
          wem_IB (i,j,k)    = 0.   ! onlyMelting
          wem0IB (i,j,k)    = 0.
          weu_IB (i,j,k)    = 0.   ! run-off
          weo_IB (i,j,k,:)  = 0.   ! run-off
          weu0IB (i,j,k)    = 0.
          weo0IB (i,j,k,:)  = 0.
          wer_IB (i,j,k)    = 0.   ! Refreezing
          wer0IB (i,j,k)    = 0.
          wesw0IB(i,j,k)    = max(0.,SWaSNo(i,j,k))
          werr0IB(i,j)      = 0. ! max(0.,rainHY(i,j))
          wesf0IB(i,j)      = 0. ! max(0.,snowHY(i,j))
          wecp0IB(i,j)      = 0. ! max(0.,rainCA(i,j)+snowCA(i,j))
 
        DO kk=1,ml
          snf0IB (i,j,kk) = 0. ! atmospheric snowfall
          sbl0IB (i,j,kk) = 0. ! atmospheric snowfall sublimation
         qssbl0IB(i,j,kk) = 0. ! atmospehric sublimation ratio
          dep0IB (i,j,kk) = 0. ! atmospheric snowfall deposition
          rnf0IB (i,j,kk) = 0. ! atmospheric rainfall
          evp0IB (i,j,kk) = 0. ! atmospheric rainfall evaporation
          smt0IB (i,j,kk) = 0. ! int. snow mass transport
        ENDDO
 
        DO kk=1,ml
          snf_IB (i,j,kk) = 0.   ! atmospheric snowfall
          sbl_IB (i,j,kk) = 0.   ! atmospheric snowfall sublimation
          qssbl_IB (i,j,kk) = 0. ! atmospheric sublimation ratio
          dep_IB (i,j,kk) = 0.   ! atmospheric snowfall deposition
          rnf_IB (i,j,kk) = 0.   ! atmospheric rainfall
          evp_IB (i,j,kk) = 0.   ! atmospheric rainfall evaporation
          smt_IB (i,j,kk) = 0.   ! integrated snow mass transport
        ENDDO
 
          wero0IB(i,j)      = max(0.,runoTV(i,j))
          weacIB (i,j,k)    = 0.
          weerIB (i,j,k)    = 0.
          weac0IB (i,j,k)   = 0.
          weer0IB (i,j,k)   = 0.
 
          if (mw .eq. 5) then
          tt_intIB(i,j,k)   = 0.   !*CL* Interpolated temperature
          qq_intIB(i,j,k)   = 0.   !*CL* Interpolated spec. hum.
          endif
 
          WKxy2(i,j)        = 0.
          DO n=1,nsx
           WKxy2(i,j)       =   WKxy2(i,j)
     .                      +  SLsrfl(i,j,n) * snohSN(i,j,n) / 1000.
           weh0IB(i,j,n)    = 0.
          END DO
          wesf0IB(i,j)      = wesf0IB(i,j) ! - WKxy2(i,j) no bluffer
          prh0IB (i,j)      = 0.
          meh0IB (i,j)      = 0.
          suh0IB (i,j)      = 0.
          snfh0IB(i,j)      = 0.
          cph0IB (i,j)      = 0.
          ruh0IB (i,j)      = 0.
 
          swdIB  (i,j)      = 0.   ! Shortwave incoming Radiation
          swuIB  (i,j)      = 0.   ! Shortwave outgoing Radiation
          lwdIB  (i,j)      = 0.   ! Longwave  incoming Radiation
          lwuIB  (i,j)      = 0.   ! Longwave  outgoing Radiation
          swdtIB (i,j)      = 0.   ! TOA Shortwave incoming Radiation
          swutIB (i,j)      = 0.   ! TOA Shortwave outgoing Radiation
          lwutIB (i,j)      = 0.   ! TOA Longwave  outgoing Radiation
          sunIB  (i,j)      = 0.   ! Sunshine
          shfIB  (i,j)      = 0.   ! Sensible  Heat
          lhfIB  (i,j)      = 0.   ! Latent    Heat
          alIB   (i,j)      = 0.   ! Albedo
          as1_IB (i,j)      = 0.   ! Albedo
          as2_IB (i,j)      = 0.   ! Albedo
          as3_IB (i,j)      = 0.   ! Albedo
          al1IB  (i,j,k)    = 0.   ! Albedo
          al2IB  (i,j,k)    = 0.   ! Albedo
          sicIB  (i,j)      = 0.   ! Sea ice fraction
          frvIB  (i,j,k)    = 0.   ! ifratv
          stIB   (i,j)      = 0.   ! Surface Temperature
          st2IB  (i,j,k)    = 0.   ! Surface Temperature
          spIB   (i,j)      = 0.   ! Surface Pressure
          slpIB  (i,j)      = 0.   ! Sea Surface Pressure
          if (mw .eq. 5) then
          gradTIB(i,j)      = 0.   ! *CL* Local temp. gradient
          gradQIB(i,j)      = 0.   ! *CL* Local hum. gradient
          endif
          z0IB   (i,j,k)    = 0.   ! Roughness length for Moment.
          r0IB   (i,j,k)    = 0.   ! Roughness length for Heat
          uusIB  (i,j,k)    = 0.   ! Friction Velocity
          uusthIB(i,j,k)    = 0.   ! Threshold Friction Velocity
          utsIB  (i,j,k)    = 0.   ! Sfc Pot. Tp. Turb. Flux
          uqsIB  (i,j,k)    = 0.   ! Water Vapor Flux
          ussIB  (i,j,k)    = 0.   ! Blowing Snow Flux	  	
          ccIB   (i,j)      = 0.   ! Cloud Cover
          cuIB   (i,j)      = 0.   ! Cloud Cover
          cmIB   (i,j)      = 0.   ! Cloud Cover
          cdIB   (i,j)      = 0.   ! Cloud Cover
          codIB  (i,j)      = 0.   ! Cloud Optical Depth
          qwIB   (i,j)      = 0.   ! Cloud Dropplets Concent
          qiIB   (i,j)      = 0.   ! Cloud Ice Crystals Concent.
          qsIB   (i,j)      = 0.   ! Cloud Snow Flakes Concent.
          qrIB   (i,j)      = 0.   ! Cloud Rain Concentration
         wvpIB   (i,j)      = 0.   ! Water Vapour Path
         cwpIB   (i,j)      = 0.   ! Condensed Water Path
         iwpIB   (i,j)      = 0.   ! Ice Water Path
         pblIB   (i,j,k)    = 0.   ! Height of Boundary Layer (2)
         qbrIB   (i,j)      = 0.   ! Snow ratio between kb level and surface
         zn4IB   (i,j,k)    = 0.   ! snowheight change due to compaction
         zn5IB   (i,j,k)    = 0.   ! snowheight change due to melting
         zn6IB   (i,j,k)    = 0.   ! snowheight total
                                  
                                   !output from coupling

         st2aoIB (i,j,k)    = 0.   !surface temperature from OASIS
         sicaoIB (i,j)      = 0.   ! SIC from OASIS
         albaoIB (i,j,k)    = 0.   !albedo from OASIS
         sitaoIB (i,j)      = 0.   !sea ice thickness
         sntaoIB (i,j)      = 0.   !snow on  sea ice thickness

        ENDDO
        ENDDO
        ENDDO
 
        DO kk=1,ml
        DO j=1,my
        DO i=1,mx
          mintIB (i,j,kk)   =  99. ! Minimum Temp of the Day
          maxtIB (i,j,kk)   = -99. ! Maximum Temp of the Day
          maxwIB (i,j,kk)   = 0.   ! Maximum wind of the Day
          if (mw .eq. 5) then
          mingrTIB (i,j)    = 5.   ! *CL* Maximum temp gradient the Day
          maxgrTIB (i,j)    = -5.  ! *CL* Maximum temp gradient of the Day
          mingrQIB (i,j)    = 100. ! *CL* Maximum spec hum gradient the Day
          maxgrQIB (i,j)    = -100.! *CL* Maximum spec hum gradient of the Day
          endif
          ttIB   (i,j,kk)   = 0.   ! Temperature
          tdIB   (i,j,kk)   = 0.   ! Temperature
          uuIB   (i,j,kk)   = 0.   ! x-Wind Speed component
          vvIB   (i,j,kk)   = 0.   ! y-Wind Speed component
          wwIB   (i,j,kk)   = 0.   ! z-Wind Speed component
          uvIB   (i,j,kk)   = 0.   ! Horizontal Wind Speed

          ruuIB   (i,j,kk)   = 0.   ! x-Wind Speed component (reg grid)
          rvvIB   (i,j,kk)   = 0.   ! y-Wind Speed component (reg grid)
          ruvIB   (i,j,kk)   = 0.   ! Horizontal Wind Speed  (reg grid)

          qqIB   (i,j,kk)   = 0.   ! Specific Humidity
          rolvIB (i,j,kk)   = 0.   ! Air Density	
          rhIB   (i,j,kk)   = 0.   ! Relative Humidity
          zzIB   (i,j,kk)   = 0.   ! Model Levels Height
          tkeIB  (i,j,kk)   = 0.   ! TKE
          lqsIB  (i,j,kk)   = 0.   ! Snow flakes content at first levels
          lqiIB  (i,j,kk)   = 0.   ! Ice flakes content at first levels
          lqwIB  (i,j,kk)   = 0.   ! Water flakes content at first levels
          lqrIB  (i,j,kk)   = 0.   ! Rain flakes content at first levels
          qsbIB  (i,j,kk)   = 0.   ! Sublimation of Qs
          lsbIB  (i,j,kk)   = 0.   ! Total Vertical Integration of LH (k=1) + LH
          swn3DIB (i,j,kk)  = 0.   ! SW net per atm. level
          lwn3DIB (i,j,kk)  = 0.   ! LW net per atm. level
          swnc3DIB(i,j,kk)  = 0.   ! Clear-sky SW net per atm. level
          lwnc3DIB(i,j,kk)  = 0.   ! Clear-sky LW net per atm. level
          cod3DIB (i,j,kk)  = 0.   ! Cloud Optical Depth per atm. level
          cc3DIB (i,j,kk)   = 0.   ! Cloud Cover per atm. level
          pddIB  (i,j)      = 0.   ! Positive degree day quantity
        END DO
        END DO
        END DO
 
        DO j=1,my
        DO i=1,mx
           txhIB0 (i,j)     = -99. ! Maximum Temp of the hour
           tnhIB0 (i,j)     =  99. ! Minimum Temp of the hour
c	   tnh    (i,j,kk)  = tnhIB0(i,j)
c	   txh    (i,j,kk)  = txhIB0(i,j)
        END DO
        END DO
 
        DO kp=1,mp
        DO j=1,my
        DO i=1,mx
          nbpIB   (i,j,kp)   = 0   ! Count valid data on pressure levels
          ttpIB   (i,j,kp)   = 0.  ! Temperature
          uupIB   (i,j,kp)   = 0.  ! x-Wind Speed component
          vvpIB   (i,j,kp)   = 0.  ! y-Wind Speed component
          wwpIB   (i,j,kp)   = 0.  ! w-Wind Speed component
          uvpIB   (i,j,kp)   = 0.  ! Horizontal Wind Speed
          qqpIB   (i,j,kp)   = 0.  ! Specific Humidity  	
          zzpIB   (i,j,kp)   = 0.  ! Model Levels Height
        END DO
        END DO
        END DO
 
        DO kz=1,mztq
        DO j=1,my
        DO i=1,mx
          ttzIB   (i,j,kz)   = 0.  ! Temperature
          qqzIB   (i,j,kz)   = 0.  ! Specific Humidity  	
        END DO
        END DO
        END DO
 
        DO kz=1,mzuv
        DO j=1,my
        DO i=1,mx
          uuzIB   (i,j,kz)   = 0.  ! x-Wind Speed component
          vvzIB   (i,j,kz)   = 0.  ! y-Wind Speed component	
          u2zIB   (i,j,kz)   = 0.  ! x-Wind Speed component
          v2zIB   (i,j,kz)   = 0.  ! y-Wind Speed component
          uvzIB   (i,j,kz)   = 0.  ! Horizontal Wind Speed
          rozIB   (i,j,kz)   = 0.  ! Air density
        END DO
        END DO
        END DO
 
        DO kk=1,llx
        DO k=1,nsx
        DO j=1,my
        DO i=1,mx
          sltIB (i,j,k,kk)  = 0    ! Soil Temperature
          slqIB (i,j,k,kk)  = 0    ! Soil Humidity Content
        END DO
        END DO
        END DO
        END DO
 
        DO kk=1,mi
        DO k=1,nsx
        DO j=1,my
        DO i=1,mx
          agIB   (i,j,k,kk) = 0.   ! Average ag
          g1IB   (i,j,k,kk) = 0.   ! Average g1
          g2IB   (i,j,k,kk) = 0.   ! Average g2
          roIB   (i,j,k,kk) = 0.   ! Average ro
          tiIB   (i,j,k,kk) = 0.   ! Average ti
          waIB   (i,j,k,kk) = 0.   ! Avegage wa
        END DO
        END DO
        END DO
        END DO
 
 
C +--1.2 Output Netcdf Initialisation
C +  ================================
 
        n1000 = 1 +     iyrrGE/1000
        n100a =     mod(iyrrGE,1000)
        n100  = 1 +     n100a /100
        n10_a =     mod(n100a ,100)
        n10   = 1 +     n10_a /10
        n1    = 1 + mod(n10_a ,10)
        m10   = 1 +     mmarGE/10
        m1    = 1 + mod(mmarGE,10)
        jd10  = 1 +     jdarGE/10
        jd1   = 1 + mod(jdarGE,10)
 
 
C +--1.2.1 Output File Label
C +  -----------------------
 
        fnamNC_ice = 'ICE.'
     .            // labnum(n1000) // labnum(n100)
     .            // labnum(  n10) // labnum(  n1)
     .            // labnum(  m10) // labnum(  m1)
     .            // labnum( jd10) // labnum( jd1)
     .            // '.' // explIO
     .            // '.nc    '
 
        fnamNC_ics = 'ICS.'
     .            // labnum(n1000) // labnum(n100)
     .            // labnum(  n10) // labnum(  n1)
     .            // labnum(  m10) // labnum(  m1)
     .            // labnum( jd10) // labnum( jd1)
     .            // '.' // explIO
     .            // '.nc    '
 
        fnamNC_tmp = fnamNC_ice
 
      if(snapshot.and.ss==0) then
        fnamNC_tmp = fnamNC_ics
      endif
 
C +--1.2.3 Output Title
C +  ------------------
 
        tit_NC_ice = 'ICE'
     .            // ' - Exp: ' // explIO
     .            // ' - '
     .            // labnum(n1000) // labnum(n100)
     .            // labnum(  n10) // labnum(  n1)
     .            // labnum(  m10) // labnum(  m1)
     .            // labnum( jd10) // labnum( jd1)
 
 
C +--1.2.4 Time Variable (hour)
C +  --------------------------
 
            dt_ICE = 0
        nbr_dt_ICE = nterun*dt*OutdyIB/86400           ! Nbr of Outputs
 
        nDFdim(0)  = nbr_dt_ICE
        nDFdim(0)  = 0
        NAMdim(0)  = 'time'
        UNIdim(0)  = 'HOURS since 1901-01-15 00:00:00'
 
        n1000 = 1 +     iyr0GE/1000
        n100a =     mod(iyr0GE,1000)
        n100  = 1 +     n100a /100
        n10_a =     mod(n100a ,100)
        n10   = 1 +     n10_a /10
        n1    = 1 + mod(n10_a ,10)
        m10   = 1 +     mma0GE/10
        m1    = 1 + mod(mma0GE,10)
        jd10  = 1 +     jda0GE/10
        jd1   = 1 + mod(jda0GE,10)
 
        UNIdim(0)  = 'MINUTES since '
     .            // labnum(n1000) // labnum(n100)
     .            // labnum(  n10) // labnum(  n1)//'-'
     .            // labnum(  m10) // labnum(  m1)//'-'
     .            // labnum( jd10) // labnum( jd1)//' 00:00:00'
 
        IF(OutdyIB<=24)
     .  UNIdim(0)  = 'HOURS since '
     .            // labnum(n1000) // labnum(n100)
     .            // labnum(  n10) // labnum(  n1)//'-'
     .            // labnum(  m10) // labnum(  m1)//'-'
     .            // labnum( jd10) // labnum( jd1)//' 00:00:00'
 
        IF(OutdyIB==1)
     .  UNIdim(0)  = 'DAYS since '
     .            // labnum(n1000) // labnum(n100)
     .            // labnum(  n10) // labnum(  n1)//'-'
     .            // labnum(  m10) // labnum(  m1)//'-'
     .            // labnum( jd10) // labnum( jd1)//' 00:00:00'
 
 
        IF (nbr_dt_ICE.gt.MXdim)
     &  STOP '*** OUTice - ERROR : MXdim to low ***'
 
c       date   = (351      +(iyrrGE  -1902) *365       ! Nb Days before iyrrGE
c    .                     +(iyrrGE  -1901) /  4       ! Nb Leap Years
c    .                     + njyrGE(mmarGE)            ! Nb Days before mmarGE
c    .                     + njybGE(mmarGE)            ! (including Leap Day)
c    .                     * max(0,1-mod(iyrrGE,4))    !
c    .                     + jdarGE     -1      )*  24 !
c    .                     + jhurGE                    !
c    .                     + (minuGE *60+jsecGE )/3600.!
 
                      date = nint(real(itexpe)*dt/60.)
      IF(OutdyIB<=24) date = nint(real(itexpe)*dt/(3600.))
      IF(OutdyIB==1)  date = nint(real(itexpe)*dt/(3600.*24.))
                      date0= date
 
        DO it = 1,nbr_dt_ICE
              timeNC_ice(it)   = jhurGE    + minuGE/  60.0 ! Starting Time
     .                         + jsecGE/3600.0
     .                         + (it-1) * 24.0 !
     .                         / real(max(OutdyIB,1))      !
              VALdim(it,0)     = date   + (it-1) * 24.0    !
     .                         / real(max(OutdyIB,1))      ! values of dim.time
              dateNC_ice(it)   =          timeNC_ice(it)   ! Time Variabl
              dayNC_ice(it)    = jdarGE + timeNC_ice(it)   !
     .                         / 24.0
        END DO
 
              month            =  mmarGE
              mill             =  iyrrGE
        DO it = 1,nbr_dt_ICE
          IF     (month        .eq.2           .AND.
     .        mod(mill,4)      .eq.0           )                  THEN
                  njmo         =  njmoGE(month) + 1
          ELSE
                  njmo         =  njmoGE(month)
          END IF
          IF   (dayNC_ice(it)  .gt.njmo        )                  THEN
            DO iu=it,nbr_dt_ICE
                dayNC_ice(iu)  =  dayNC_ice(iu) - njmo
            END DO
              month            =  month + 1
            IF (month.gt.12)                                      THEN
              month            =          1
              mill             =  mill  + 1
            END IF
          END IF
              monthNC_ice(it)  =  month
              yearNC_ice(it)   =  mill
 
          IF (dateNC_ice(it)   .gt.24.0-epsi)                     THEN
            DO iu=it,nbr_dt_ICE
              dateNC_ice(iu)   = mod(dateNC_ice(iu),24.0)
            END DO
          END IF
        END DO
 
        DO it = 1,nbr_dt_ICE
              dateNC_ice(it)   =         dateNC_ice(it)
     .                         + 1.d+2 *  dayNC_ice(it)
     .                         + 1.d+4 *monthNC_ice(it)
     .                         + 1.d+6 * yearNC_ice(it)
        END DO
 
 
C +--1.2.5 Define horizontal spatial dimensions
C +  ------------------------------------------
 
        DO i = 1, mx
          VALdim(i,1) = xxkm(i)
         !VALdim(i,1) = xxkm2(i) ! true distance
        END DO
          nDFdim(1)= mx
          NAMdim(1)= 'x'
          UNIdim(1)= 'km'
 
        DO j = 1, my
          VALdim(j,2) = yykm(j)
         !VALdim(j,2) = yykm2(j) ! true distance
        END DO
          nDFdim(2)= my
          NAMdim(2)= 'y'
          UNIdim(2)= 'km'
 
        DO k = 1, nsx
          VALdim(k,3) = k
        END DO
          nDFdim(3)= nsx
          NAMdim(3)= 'sector'
          UNIdim(3)= 'level'
 
        DO k = 1, ml
          VALdim(k,4) = sigma(mz-k+1)
        END DO
          nDFdim(4)= ml
          NAMdim(4)= 'atmlay'
          UNIdim(4)= 'sigma_level'
 
        DO k = 1, mlhh
          VALdim(k,5) = k*(24./real(mlhh))
        END DO
          nDFdim(5)= mlhh
          NAMdim(5)= 'atmxh'
          UNIdim(5)= 'hours'
 
        DO k = 1, mi
          VALdim(k,6) = OutshIB(k)
        END DO
          nDFdim(6)= mi
          NAMdim(6)= 'outlay'
          UNIdim(6)= 'm'
 
        DO k = 1, llx
          VALdim(k,7) = k
        END DO
          nDFdim(7)= llx
          NAMdim(7)= 'sollay'
          UNIdim(7)= 'layer'
 
        DO k = 1, nsno
          VALdim(k,8) = k
        END DO
          nDFdim(8)= nsno
          NAMdim(8)= 'snolay'
          UNIdim(8)= 'layer'
 
         DO k = 1, mp
           VALdim(k,9) = OutPLevIB(k)
         END DO
           nDFdim(9)= mp
           NAMdim(9)= 'plev'
           UNIdim(9)= 'hPa'
 
         DO k = 1, mztq
           VALdim(k,10) = OutZTQLevIB(k)
         END DO
           nDFdim(10)= mztq
           NAMdim(10)= 'ztqlev'
           UNIdim(10)= 'm'
 
         DO k = 1, mzuv
           VALdim(k,11) = OutZUVLevIB(k)
         END DO
           nDFdim(11)= mzuv
           NAMdim(11)= 'zuvlev'
           UNIdim(11)= 'm'
 
         if( mlb>ml) then
          print *,"ERROR mlb > ml"
          stop
         endif
 
         DO k = 1, mlb
           VALdim(k,12) = sigma(mz-k+1)
         END DO
           nDFdim(12)= mlb
           NAMdim(12)= 'blev'
           UNIdim(12)= 'sigma_level'
 
C +--1.2.6 Variable's Choice (Table ICEvou.dat)
C +  ------------------------------------------
 
        OPEN(unit=10,status='unknown',file='ICEvou.dat')
 
        itotNC_ice = 0
 980    CONTINUE
        READ (10,'(A120)',end=990) tmpINP
        IF (tmpINP(1:4).eq.'    ')                                THEN
          itotNC_ice = itotNC_ice + 1
          READ (tmpINP,'(4x,5A9,A12,A50)')
     .          nameNC_ice(itotNC_ice)  , ! Name
     .          SdimNC_ice(1,itotNC_ice), ! Names of Selected Dimensions
     .          SdimNC_ice(2,itotNC_ice), ! (max.4/variable)
     .          SdimNC_ice(3,itotNC_ice),
     .          SdimNC_ice(4,itotNC_ice),
     .          unitNC_ice(itotNC_ice)  , ! Units
     .          lnamNC_ice(itotNC_ice)    ! Description of the variable
 
        if(snapshot.and.ss==0) then
         if(nameNC_ice(itotNC_ice)(1:2)=="MB") itotNC_ice=itotNC_ice-1
         if(nameNC_ice(itotNC_ice)(3:1)=="h")  itotNC_ice=itotNC_ice-1
        endif
 
        ENDIF
        GOTO 980
 990    CONTINUE
 
        CLOSE(unit=10)
 
        NtotNC_ice = itotNC_ice                 ! Total number of variables
C +                                             ! writen     in NetCDF file.
 
 
C +--1.2.7 List of NetCDF attributes given to all variables
C +  ------------------------------------------------------
 
        NAMrat    (1)          = 'actual_range' ! "actual_range" is (min,max)
        NvatNC_ice(1)          = 2              ! of all data for each variable
 
        if (NattNC_ice .eq. 2) then
          NAMrat    (NattNC_ice) = '[var]_range'
          NvatNC_ice(NattNC_ice) = 2
        endif
 
 
C +--1.2.8 Automatic Generation of the NetCDF File Structure
C +  -------------------------------------------------------
 
C +     **************
        CALL UNscreate (fnamNC_tmp,tit_NC_ice,
     .                  NdimNC_ice,nDFdim    , MXdim     ,
     .                  NAMdim    ,UNIdim    , VALdim    ,
     .                  MX_var    ,NtotNC_ice, nameNC_ice,
     .                  SdimNC_ice,unitNC_ice, lnamNC_ice,
     .                  NattNC_ice,NAMrat    , NvatNC_ice,
     .                  ID__nc_ice)
C +     **************
 
        print *, "Creation of "//trim(fnamNC_tmp)
 
 
C +--1.2.9 Computation of inital mass balance variables (at itexpe=0)
C +  ----------------------------------------------------------------
 
           tmp2_OK               = 1.0
        DO j=jp11,my1
        DO i=ip11,mx1
        DO k=1   ,nsx
           tmp2_OK               = min(tmp2_OK,
     .                             max(zero,sign(unun,-mb0IB(i,j,k))))
        END DO
        END DO
        END DO
 
        DO j=jp11,my1
        DO i=ip11,mx1
        DO k=1   ,nsx
          IF (    nssSNo(i,j,k).ge.1)                               THEN
            znsn1(nssSNo(i,j,k)) = dzsSNo(i,j,k,nssSNo(i,j,k))
            snwae(nssSNo(i,j,k)) = rosSNo(i,j,k,nssSNo(i,j,k))
     .                           * dzsSNo(i,j,k,nssSNo(i,j,k))
     .                           * 1.e3 / ro_Wat
     .                           * (1. + 0.*wasSNo(i,j,k,nssSNo(i,j,k)))
     .                           + SWaSNo(i,j,k)
            DO nk=nssSNo(i,j,k)-1,1,-1
            znsn1(nk)            = dzsSNo(i,j,k,nk) + znsn1 (      nk+1)
            snwae(nk)            = rosSNo(i,j,k,nk) * dzsSNo(i,j,k,nk  )
     .                           * 1.e3 / ro_Wat
     .                           * (1 + 0.*wasSNo(i,j,k,nk))
     .                           + snwae (nk+1)
            END DO
            tmp1_OK              = max(0  ,sign(1  ,nisSNo(i,j,k)-1))
            mb0IB(i,j,k)         =        tmp2_OK
     .              * (snwae (1) - snwae (nisSNo(i,j,k)+1) * tmp1_OK)
     .                           +    (1.-tmp2_OK) * mb0IB(i,j,k)
            zn0IB(i,j,k)         =        tmp2_OK
     .              * (znsn1 (1) - znsn1 (nisSNo(i,j,k)+1) * tmp1_OK)
     .                           +    (1.-tmp2_OK) * zn0IB(i,j,k)
C +...      tmp1_OK              = 1 if ice
C +         tmp2_OK              = 0 if mb0IB is initialised
            wet_IB(i,j,k)        = snwae(1)
            wet0IB(i,j,k)        = snwae(1)
            if(k==1) smbh0IB(i,j)= snwae(1)
            if(k==1)  swh0IB(i,j)= SWaSNo(i,j,k)
            S_m_IB(i,j,k)        =
     .                (snwae (1) - snwae (nisSNo(i,j,k)+1) * tmp1_OK)
            S_h_IB(i,j,k)        =
     .                (znsn1 (1) - znsn1 (nisSNo(i,j,k)+1) * tmp1_OK)
            SIm_IB(i,j,k)        = snwae(1)
            SIh_IB(i,j,k)        = znsn1(1)
 
            do nk=1,nsno
             zn6IB   (i,j,k) = dzsSNo(i,j,k,nk)   + zn6IB(i,j,k)
            enddo
            zn6IB   (i,j,k)  = zn6IB(i,j,k)       - zn0IB(i,j,k)
 
 
          ELSE
            mb0IB (i,j,k)        = 0.
            zn0IB (i,j,k)        = 0.
            wet_IB(i,j,k)        = 0.
            wet0IB(i,j,k)        = 0.
            S_m_IB(i,j,k)        = 0.
            S_h_IB(i,j,k)        = 0.
            SIm_IB(i,j,k)        = 0.
            SIh_IB(i,j,k)        = 0.
          END IF
            xynsx1(i,j,k)        = 1.  !  1.  above 1st superimposed Ice Layer
            SSh_IB(i,j,k)        = 0.  !  H (*  without superimposed Ice)
        END DO
        END DO
        END DO

c #EU   zn0IB   = 0.
c #EU   mb0IB   = 0.
 
        DO kk=nsno,1,-1
        DO k=1,nsx
        DO j=1,my
        DO i=1,mx
            xynsx1(i,j,k) =                          xynsx1(i,j,k)
     .                    * max(zero,sign(unun,850. -rosSNo(i,j,k,kk)))
            SSh_IB(i,j,k) = dzsSNo(i,j,k,kk)        *xynsx1(i,j,k)
     .                    +                          SSh_IB(i,j,k)
        END DO
        END DO
        END DO
        END DO
 
 
C +--1.2.10 Write Time - Constants
C +  -----------------------------
 
        DO j=1,my
        DO i=1,mx
          Wkxy1(i,j)   =  GElonh(i,j) * 15.d0  ! Hour->degrees
          WKxy2(i,j)   =  GElatr(i,j) / degrad ! rad ->degree
          WKxy3(i,j)   =  real(mskSNo(i,j,1))  ! REAL type
          DO k=1,nsx
          xynsx2(i,j,k)=  real(mskSNo(i,j,k))
          END DO
          WKxy4(i,j)   =  real(isolTV(i,j))    ! REAL type	
          WKxy5(i,j)   =  real(isolSL(i,j))    ! REAL type
        END DO
        END DO
 
C +     ************
        CALL UNwrite (ID__nc_ice,'LON', 1, mx, my,   1, Wkxy1)
        CALL UNwrite (ID__nc_ice,'LAT', 1, mx, my,   1, Wkxy2)
        CALL UNwrite (ID__nc_ice,'SH',  1, mx, my,   1, sh)
        CALL UNwrite (ID__nc_ice,'AREA',1, mx, my,   1, area)
        CALL UNwrite (ID__nc_ice,'DX',1, mx, my,   1, dx3/1000.)
        CALL UNwrite (ID__nc_ice,'DY',1, mx, my,   1, dy3/1000.)
        CALL UNwrite (ID__nc_ice,'SLO', 1, mx, my,   1, slopGE)
 
        if(mw.ne.5) then
        CALL UNwrite (ID__nc_ice,'MSK', 1, mx, my,   1, WKxy3)
        else
        CALL UNwrite (ID__nc_ice,'MSK', 1, mx, my, nsx, xynsx2)
        endif
 
        CALL UNwrite (ID__nc_ice,'SOL', 1, mx, my,   1, Wkxy4)
        CALL UNwrite (ID__nc_ice,'SRF', 1, mx, my,   1, Wkxy5)
C +     ************
 
        DO k=1,min(nvx,nsx)
        DO j=1,my
        DO i=1,mx
          Wkxy1(i,j)    = real(czenGE(i,j))
          WKxy2(i,j)    = real(AlbSTV(i,j))
          xynsx1(i,j,k) = real(ivegTV(i,j,k))
          xynsx2(i,j,k) = real(ifraTV(i,j,k))
        END DO
        END DO
        END DO
 
        DO j=1,my
        DO i=1,mx
        DO k=1,nsx
           slqmIB(i,j,k) = 0
           slqcIB(i,j,k) = 0
          DO kk=-nsol,0
           slqmIB(i,j,k) = EtadSV(isolTV(i,j))*1000. * dzAvSV(kk)
     .                                               + slqmIB(i,j,k)
           slqcIB(i,j,k) = Eta_TV(i,j,k,1-kk)        * dzAvSV(kk)
     .                   * ro_Wat                    + slqcIB(i,j,k)
          ENDDO
          wel0IB (i,j,k) = slqcIB(i,j,k)
        ENDDO
        ENDDO
        ENDDO


 
C +     ************
        CALL UNwrite (ID__nc_ice,'CZ' , 1, mx, my, 1  , Wkxy1)
        CALL UNwrite (ID__nc_ice,'SAL', 1, mx, my, 1  , WKxy2)
        CALL UNwrite (ID__nc_ice,'VEG', 1, mx, my, nsx, xynsx1)
        CALL UNwrite (ID__nc_ice,'FRV', 1, mx, my, nsx, xynsx2)
        CALL UNwrite (ID__nc_ice,'SLQM',1, mx, my, nsx, slqmIB)
        call NCSNC   (ID__nc_ice, RCODE)
C +     ************
 
c       date      = (351+(iyrrGE  -1902) *365  ! Nb Days before iyrrGE
c    .            +(iyrrGE  -1901) /  4        ! Nb Leap Years
c    .            + njyrGE(mmarGE)             ! Nb Days before mmarGE
c    .            + njybGE(mmarGE)             ! (including Leap Day)
c    .            * max(0,1-mod(iyrrGE,4))     !
c    .            + jdarGE -1 )*  24           !
c    .            + jhurGE                     !
c    .            + (0 *60 +0)/3600            !
 
                      date = nint(max(0.,real(itexpe-1))*dt/60.)
      IF(OutdyIB<=24) date = nint(max(0.,real(itexpe-1))*dt/(3600.))
      IF(OutdyIB==1)  date = nint(max(0.,real(itexpe-1))*dt/(3600.*24.))
                      date0= date
 
       print *,"OUTice Initialization BEGIN",ss
 
       write(6,399) OutdyIB
 399   format(" OUTice: nbr of outputs by day:",i3)
 
       write(6,400) mz,mz-ml
 400   format(" OUTice:     sigma levels kept:",i3,' => ',i3)
 
       write(6,401) int(24./real(mlhh)*60.)
 401   format(" OUTice:  x-hourly outputs every:",i5," minutes")
 
       write(6,402) (int(OutPLevIB(i)),i=mp,1,-1)
 402   format(" OUTice:       Pressure levels:",20i4)
 
       write(6,403) (int(OutZTQLevIB(i)),i=1,mztq)
 403   format(" OUTice:         Height levels:",20i4)
 
       write(6,404) (OutshIB(i),i=1,mi)
 404   format(" OUTice:    Snow height levels:",30f5.1)
 
       write(6,405) nbr_call_outice
 405   format(" OUTice: called every ",i2," time steps in mar.f")
 
       print *,"OUTice Initialization END"
 
 
      if(snapshot.and.ss==1) then
       ss=0
       goto 800
      endif
 
      END IF ! Initialization
 
 
C ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C ++ 2. Every Time  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 
C +--2.1 Re-initialization
C +  =====================
 
                   ss=0
      if(snapshot) ss=1
 
      fnamNC_tmp = fnamNC_ice
 
801   continue
 
      IF (dt_ICE2.eq.-1)                                           THEN
 
        dt_ICE2             = 0
        timehIB             = 0.
        iyrrIB              = 0
 
        DO k=1,nsx
        DO j=1,my
        DO i=1,mx
 
          swdIB  (i,j)      = 0.   ! Shortwave incoming Radiation
          swuIB  (i,j)      = 0.   ! Shortwave outgoing Radiation
          lwdIB  (i,j)      = 0.   ! Longwave  incoming Radiation
          lwuIB  (i,j)      = 0.   ! Longwave  outgoing Radiation
          swdtIB (i,j)      = 0.   ! TOA Shortwave incoming Radiation
          swutIB (i,j)      = 0.   ! TOA Shortwave outgoing Radiation
          lwutIB (i,j)      = 0.   ! TOA Longwave  outgoing Radiation
          sunIB  (i,j)      = 0.   ! Sunshine
          shfIB  (i,j)      = 0.   ! Sensible  Heat
          lhfIB  (i,j)      = 0.   ! Latent    Heat
          alIB   (i,j)      = 0.   ! Albedo
          as1_IB (i,j)      = 0.   ! Albedo
          as2_IB (i,j)      = 0.   ! Albedo
          as3_IB (i,j)      = 0.   ! Albedo
          al1IB  (i,j,k)    = 0.   ! Albedo
          al2IB  (i,j,k)    = 0.   ! Albedo
          sicIB  (i,j)      = 0.   ! Sea ice fraction
          frvIB  (i,j,k)    = 0.   ! ifratv
          stIB   (i,j)      = 0.   ! Surface Temperature
          st2IB  (i,j,k)    = 0.   ! Surface Temperature
          spIB   (i,j)      = 0.   ! Surface Pressure
          slpIB  (i,j)      = 0.   ! Sea Surface Pressure
          if (mw .eq. 5) then
          gradTIB(i,j)      = 0.   ! *CL* local temp. gradient
          gradQIB(i,j)      = 0.   ! *CL* local hum. gradient
         tt_intIB(i,j,k)    = 0.   ! *CL* Interpolated temperature
         qq_intIB(i,j,k)    = 0.   ! *CL* Interpolated spec. hum.
 
          endif
          z0IB   (i,j,k)    = 0.   ! Roughness length for Moment.
          r0IB   (i,j,k)    = 0.   ! Roughness length for Heat
          uusIB  (i,j,k)    = 0.   ! Friction Velocity
          uusthIB(i,j,k)    = 0.   ! Threshold Friction Velocity
          utsIB  (i,j,k)    = 0.   ! Sfc Pot. Tp. Turb. Flux
          uqsIB  (i,j,k)    = 0.   ! Water Vapor Flux
          ussIB  (i,j,k)    = 0.   ! Blowing Snow Flux	  	
          ccIB   (i,j)      = 0.   ! Cloud Cover
          cuIB   (i,j)      = 0.   ! Cloud Cover
          cmIB   (i,j)      = 0.   ! Cloud Cover
          cdIB   (i,j)      = 0.   ! Cloud Cover
          codIB  (i,j)      = 0.   ! Cloud Optical Depth
          qwIB   (i,j)      = 0.   ! Cloud Dropplets Concent
          qiIB   (i,j)      = 0.   ! Cloud Ice Crystals Concent.
          qsIB   (i,j)      = 0.   ! Cloud Snow Flakes Concent.
          qrIB   (i,j)      = 0.   ! Cloud Rain Concentration
         wvpIB   (i,j)      = 0.   ! Water Vapour Path
         cwpIB   (i,j)      = 0.   ! Condensed Water Path
         iwpIB   (i,j)      = 0.   ! Ice Water Path
         pblIB   (i,j,k)    = 0.   ! Height of Boundary Layer (2)
         qbrIB   (i,j)      = 0.   ! Snow ratio between kb level and surface
         zn4IB   (i,j,k)    = 0.   ! snowheight change due to compaction
         zn5IB   (i,j,k)    = 0.   ! snowheight change due to melting

                                   ! output from coupling
         st2aoIB (i,j,k)    = 0.   ! surface temperature from OASIS
         sicaoIB (i,j)      = 0.   ! SIC from OASIS
         albaoIB (i,j,k)    = 0.   ! albedo from OASIS 
         sitaoIB (i,j)      = 0.   ! sea ice thickness
         sntaoIB (i,j)      = 0.   ! snow on  sea ice thickness

        ENDDO
        ENDDO
        ENDDO
 
        DO kk=1,ml
        DO j=1,my
        DO i=1,mx
          mintIB (i,j,kk)   =  60. ! Minimum Temp of the Day
          maxtIB (i,j,kk)   = -60. ! Maximum Temp of the Day
          maxwIB (i,j,kk)   = 0.   ! Maximum Wind of the Day
          if (mw .eq. 5) then
          mingrTIB (i,j)    = 5.   ! *CL* Maximum temp gradient the Day
          maxgrTIB (i,j)    = -5.  ! *CL* Maximum temp gradient of the Day
          mingrQIB (i,j)    = 100. ! *CL* Maximum spec hum gradient the Day
          maxgrQIB (i,j)    = -100.! *CL* Maximum spec hum gradient of the Day
          endif
          ttIB   (i,j,kk)   = 0.   ! Temperature
          tdIB   (i,j,kk)   = 0.   ! Temperature
          uuIB   (i,j,kk)   = 0.   ! x-Wind Speed component
          vvIB   (i,j,kk)   = 0.   ! y-Wind Speed component
          wwIB   (i,j,kk)   = 0.   ! w-Wind Speed component
          uvIB   (i,j,kk)   = 0.   ! Horizontal Wind Speed

          ruuIB   (i,j,kk)   = 0.   ! x-Wind Speed component (reg grid)
          rvvIB   (i,j,kk)   = 0.   ! y-Wind Speed component (reg grid)
          ruvIB   (i,j,kk)   = 0.   ! Horizontal Wind Speed  (reg grid)

          qqIB   (i,j,kk)   = 0.   ! Specific Humidity
          rolvIB (i,j,kk)   = 0.   ! Air Density	
          rhIB   (i,j,kk)   = 0.   ! Relative Humidity
          zzIB   (i,j,kk)   = 0.   ! Model Levels Height
          tkeIB  (i,j,kk)   = 0.   ! TKE
          lqsIB  (i,j,kk)   = 0.   ! Snow flakes content at first levels
          lqiIB  (i,j,kk)   = 0.   ! Ice flakes content at first levels
          lqrIB  (i,j,kk)   = 0.   ! Rain flakes content at first levels
          lqwIB  (i,j,kk)   = 0.   ! Water flakes content at first levels
          qsbIB  (i,j,kk)   = 0.   ! Sublimation of Qs
          lsbIB  (i,j,kk)   = 0.   ! Total Vertical Integration of LH (k=1) + LH
          swn3DIB (i,j,kk)  = 0.   ! SW net per atm. level
          lwn3DIB (i,j,kk)  = 0.   ! LW net per atm. level
          swnc3DIB(i,j,kk)  = 0.   ! Clear-sky SW net per atm. level
          lwnc3DIB(i,j,kk)  = 0.   ! Clear-sky LW net per atm. level
          cod3DIB (i,j,kk)  = 0.   ! Cloud Optical Depth per atm. level
          cc3DIB (i,j,kk)   = 0.   ! Cloud Cover per atm. level
          pddIB   (i,j)     = 0.   ! Positive degree day quantity
        END DO
        END DO
        END DO
 
        DO j=1,my
        DO i=1,mx
           tnhIB0 (i,j)     =  99. ! Minimum Temp of the hour
           txhIB0 (i,j)     = -99. ! Maximum Temp of the hour
        END DO
        END DO
 
        DO kp=1,mp
        DO j=1,my
        DO i=1,mx
          nbpIB   (i,j,kp)   = 0   ! Count valid data on pressure levels
          ttpIB   (i,j,kp)   = 0.  ! Temperature
          uupIB   (i,j,kp)   = 0.  ! x-Wind Speed component
          vvpIB   (i,j,kp)   = 0.  ! y-Wind Speed component
          wwpIB   (i,j,kp)   = 0.  ! w-Wind Speed component
          uvpIB   (i,j,kp)   = 0.  ! Horizontal Wind Speed
          qqpIB   (i,j,kp)   = 0.  ! Specific Humidity  	
          zzpIB   (i,j,kp)   = 0.  ! Model Levels Height
        END DO
        END DO
        END DO
 
        DO kz=1,mztq
        DO j=1,my
        DO i=1,mx
          ttzIB   (i,j,kz)   = 0.  ! Temperature
          qqzIB   (i,j,kz)   = 0.  ! Specific Humidity  	
        END DO
        END DO
        END DO
 
        DO kz=1,mzuv
        DO j=1,my
        DO i=1,mx
          uuzIB   (i,j,kz)   = 0.  ! x-Wind Speed component
          vvzIB   (i,j,kz)   = 0.  ! y-Wind Speed component	
          u2zIB   (i,j,kz)   = 0.  ! x-Wind Speed component
          v2zIB   (i,j,kz)   = 0.  ! y-Wind Speed component
          uvzIB   (i,j,kz)   = 0.  ! Horizontal Wind Speed
          rozIB   (i,j,kz)   = 0.  ! Air density
        END DO
        END DO
        END DO
 
        DO kk=1,llx
        DO k=1,nsx
        DO j=1,my
        DO i=1,mx
          sltIB (i,j,k,kk)  = 0    ! Soil Temperature
          slqIB (i,j,k,kk)  = 0    ! Soil Humidity Content
        END DO
        END DO
        END DO
        END DO
 
        DO kk=1,mi
        DO k=1,nsx
        DO j=1,my
        DO i=1,mx
          agIB   (i,j,k,kk) = 0.   ! Average ag
          g1IB   (i,j,k,kk) = 0.   ! Average g1
          g2IB   (i,j,k,kk) = 0.   ! Average g2
          roIB   (i,j,k,kk) = 0.   ! Average ro
          tiIB   (i,j,k,kk) = 0.   ! Average ti
          waIB   (i,j,k,kk) = 0.   ! Avegage wa
        END DO
        END DO
        END DO
        END DO
      END IF
 
 
C +--2.2 Putting Values in Matrices
C +  ==============================
 
      dt_ICE2=dt_ICE2+1
 
 
C +--2.2.1 Atmospheric variables
C +  ---------------------------
 
      if(nsx<2) then
       print *,"mw <2!!!!"
       stop
      endif

      do kk=mz/2,mz
      call wind_rot(UairDY(:,:,kk),VairDY(:,:,kk),
     . rotuuIB(:,:,kk),rotvvIB(:,:,kk))
      enddo

!$OMP PARALLEL DO default(shared)
!$OMP.private(i,j,k,kz,zDown,zLev,distUp,kUp,kDown,kMiddle,
!$OMP.        zMiddle,zUp,pDown,pLev,pMiddle,kk,q,qst,r,rst,
!$OMP.        rh,tmp1z,tmp2z,tmp3,tmp4,nk,depthsnow,tmp)
      DO j=1,my
      DO i=1,mx
 
 
       qbrIB(i,j)   =  qbrIB(i,j) + qbs_HY(i,j)
 
        DO kk=1,ml
 
          q         = qvDY(i,j,mz-kk+1)
          qst       = qsat0D(tairDY(i,j,mz-kk+1),
     .                  sigma(mz-kk+1),pstDY(i,j),ptopDY,1)
 
          r         = q   / max(epsi,1.-q)
          rst       = qst / max(epsi,1.-qst)
 
          rh        =  (r/(0.622+r))
     .              / max(epsi,(rst/(0.622+rst))) * 100.
          rh        = max(0.,min(100.,rh))
 
 
          mintIB(i,j,kk)  = min(tairDY(i,j,mz-kk+1)-TfSnow,
     .                          mintIB(i,j,kk))
          maxtIB(i,j,kk)  = max(tairDY(i,j,mz-kk+1)-TfSnow,
     .                          maxtIB(i,j,kk))
 
          maxwIB(i,j,kk)  = max(
     .          (uairDY(i,j,mz-kk+1)**2+vairDY(i,j,mz-kk+1)**2)**0.5,
     .                          maxwIB(i,j,kk))
          if (mw .eq. 5) then
          maxgrTIB(i,j)   = max(gradTM(i,j),maxgrTIB(i,j))       ! *CL*
          mingrTIB(i,j)   = min(gradTM(i,j),mingrTIB(i,j))       ! *CL*
          maxgrQIB(i,j)   = max(gradQM(i,j),maxgrQIB(i,j))       ! *CL*
          mingrQIB(i,j)   = min(gradQM(i,j),mingrQIB(i,j))       ! *CL*
          endif
          ttIB  (i,j,kk)  = ttIB (i,j,kk)+ tairDY(i,j,mz-kk+1)-TfSnow
          tdIB  (i,j,kk)  = tdIB (i,j,kk)+ 243.5*(log(rh/100.)
     .  +((17.67*(tairDY(i,j,mz-kk+1)-TfSnow))
     .   /(243.5+(tairDY(i,j,mz-kk+1)-TfSnow))))/(17.67-Log(rh/100.)
     .  -((17.67*(tairDY(i,j,mz-kk+1)-TfSnow))
     .   /(243.5+(tairDY(i,j,mz-kk+1)-TfSnow))))
          uuIB  (i,j,kk)  = uuIB (i,j,kk)+ uairDY(i,j,mz-kk+1)
          vvIB  (i,j,kk)  = vvIB (i,j,kk)+ vairDY(i,j,mz-kk+1)
          uvIB  (i,j,kk)  = uvIB (i,j,kk)+
     .          (uairDY(i,j,mz-kk+1)**2+vairDY(i,j,mz-kk+1)**2)**0.5
          wwIB  (i,j,kk)  = wwIB (i,j,kk)+ wairDY(i,j,mz-kk+1)

          ruuIB  (i,j,kk)  = ruuIB (i,j,kk)+ rotuuIB(i,j,mz-kk+1)
          rvvIB  (i,j,kk)  = rvvIB (i,j,kk)+ rotvvIB(i,j,mz-kk+1)
          ruvIB  (i,j,kk)  = ruvIB (i,j,kk)+
     .          (rotuuIB(i,j,mz-kk+1)**2+rotvvIB(i,j,mz-kk+1)**2)**0.5

          qqIB  (i,j,kk)  = qqIB (i,j,kk)+   qvDY(i,j,mz-kk+1)*1000.
          rolvIB(i,j,kk)  =rolvIB(i,j,kk)+ rolvDY(i,j,mz-kk+1)*1000.
          rhIB  (i,j,kk)  = rhIB (i,j,kk)+   rh
          zzIB  (i,j,kk)  = zzIB (i,j,kk)+ gplvDY(i,j,mz-kk+1)*grvinv
          tkeIB (i,j,kk)  = tkeIB(i,j,kk)+ ect_TE(i,j,mz-kk+1)
          lqsIB (i,j,kk)  = lqsIB(i,j,kk)+   qsHY(i,j,mz-kk+1)*1000.
          lqiIB (i,j,kk)  = lqiIB(i,j,kk)+   qiHY(i,j,mz-kk+1)*1000.
          lqrIB (i,j,kk)  = lqrIB(i,j,kk)+   qrHY(i,j,mz-kk+1)*1000.
          lqwIB (i,j,kk)  = lqwIB(i,j,kk)+   qwHY(i,j,mz-kk+1)*1000.
          qsbIB (i,j,kk)  = qsbIB(i,j,kk)+ hsubHY(i,j,mz-kk+1)*1000.
          lsbIB (i,j,kk)  = lsbIB(i,j,kk)+ hlatHY(i,j,mz-kk+1)
          swn3DIB(i,j,kk) = swn3DIB(i,j,kk)+ RAfnSO(i,j,mz-kk+1)
          lwn3DIB(i,j,kk) = lwn3DIB(i,j,kk)+ RAfnIR(i,j,mz-kk+1)
          swnc3DIB(i,j,kk)= swnc3DIB(i,j,kk)+ RAfncSO(i,j,mz-kk+1)
          lwnc3DIB(i,j,kk)= lwnc3DIB(i,j,kk)+ RAfncIR(i,j,mz-kk+1)

          !Cloud variables 
          cc3DIB(i,j,kk)  = cc3DIB(i,j,kk) + CldFRA(i,j,mz-kk+1)
          DO k=1,3
            tmp(k)        = 0.0
          ENDDO
          tmp(3)          = (pstDY(i,j)  * sigma(mz-kk+1)+ptopDY)
     .                    / (ra*tairDY(i,j,mz-kk+1)
     .                    * (1.+.608*qvDY(i,j,mz-kk+1)))
     .                    * (gpmiDY(i,j,mz-kk+1)-gpmiDY(i,j,mz-kk+1+1))
          tmp(1)          = tmp(3) * qwHY(i,j,mz-kk+1)
          tmp(2)          = tmp(3) * qiHY(i,j,mz-kk+1)
          cod3DIB(i,j,kk) = 1.5 * ( tmp(1) / 20.d-6
     .                            + tmp(2) / 40.d-6 ) *grvinv
     .                            + cod3DIB (i,j,kk) 
       ENDDO
 
        DO kk=1,mz
          tmp1z(kk)       = ect_TE(i,j,kk)
          tmp2z(kk)       = gplvDY(i,j,kk)*grvinv - sh(i,j)
       ENDDO
 
C +    ************
       call PBLtop (tmp1z,tmp2z,tmp3,tmp4)
C +    ************
 
         pblIB (i,j,1)    = pblIB (i,j,1) + tmp3
         pblIB (i,j,2)    = pblIB (i,j,2) + tmp4
 
          spIB (i,j)      = spIB (i,j) + pstDY(i,j)*10.
 
          if (mw .eq. 5) then
          gradTIB (i,j)   = gradTIB (i,j) + gradTM(i,j) !*CL*
          gradQIB (i,j)   = gradQIB (i,j) + gradQM(i,j) !*CL*
          DO k=1,nsx
          tt_intIB(i,j,k) = tt_intIB(i,j,k)-TfSnow  + tairDY_int(i,j,k) !*CL*
          qq_intIB(i,j,k) = qq_intIB(i,j,k)+ qvDY_int(i,j,k)*1000.      !*CL*
          ENDDO
          endif
 
      END DO
c     END DO
 
C +--2.2.3 Atmospheric variables on pressure levels
C +  ----------------------------------------------
 

c     DO j=1,my
      DO i=1,mx
          pDown = (pstDY(i,j) * sigma(mz) + ptopDY) * 10
          DO kp=1,mp
              pLev=OutPLevIB(kp)
              IF (pLev.le.pDown) THEN
                  nbpIB(i,j,kp) = nbpIB(i,j,kp) + 1
                  kUp = 1
                  kDown = mz
                  DO WHILE (kDown-kUp.gt.1)
                      kMiddle = (kDown + kUp) / 2
                      pMiddle = (pstDY(i,j) * sigma(kMiddle)  +
     .                           ptopDY) * 10
                      IF (pMiddle.ge.pLev) THEN
                          kDown = kMiddle
                      ELSE
                          kUp = kMiddle
                      ENDIF
                  ENDDO
                  pUp    = (pstDY(i,j) * sigma(kUp)   + ptopDY) * 10
                  pDown  = (pstDY(i,j) * sigma(kDown) + ptopDY) * 10
                  distUp = (pLev - pUp) / (pDown - pUp)
                  tairDYp(i,j,kp) = distUp * tairDY(i,j,kDown) +
     .                              (1-distUp) * tairDY(i,j,kUp)
                  qvDYp(i,j,kp) = distUp * qvDY(i,j,kDown) +
     .                            (1-distUp) * qvDY(i,j,kUp)
                  gplvDYp(i,j,kp) = distUp * gplvDY(i,j,kDown) +
     .                            (1-distUp) * gplvDY(i,j,kUp)
                  uairDYp(i,j,kp) = distUp * uairDY(i,j,kDown) +
     .                              (1-distUp) * uairDY(i,j,kUp)
                  vairDYp(i,j,kp) = distUp * vairDY(i,j,kDown) +
     .                              (1-distUp) * vairDY(i,j,kUp)
                  wairDYp(i,j,kp) = distUp * wairDY(i,j,kDown) +
     .                              (1-distUp) * wairDY(i,j,kUp)
                  ! sum for the output
                  ttpIB(i,j,kp) = ttpIB(i,j,kp) +
     .                            tairDYp(i,j,kp) - TfSnow
                  qqpIB(i,j,kp) = qqpIB(i,j,kp) +
     .                            qvDYp(i,j,kp) * 1000.
                  zzpIB(i,j,kp) = zzpIB(i,j,kp) +
     .                            gplvDYp(i,j,kp) * grvinv
                  uupIB(i,j,kp) = uupIB(i,j,kp) + uairDYp(i,j,kp)
                  vvpIB(i,j,kp) = vvpIB(i,j,kp) + vairDYp(i,j,kp)
                  wwpIB(i,j,kp) = wwpIB(i,j,kp) + wairDYp(i,j,kp)
                  uvpIB(i,j,kp) = uvpIB(i,j,kp) +
     .              (uairDYp(i,j,kp)**2 + vairDY(i,j,kp)**2)**0.5
              ENDIF
          ENDDO
      END DO
c     END DO
 
 
C +--2.2.4 Atmospheric variables on heigth levels (z)
C +  ------------------------------------------------
 
C +--2.2.4.1 Temperature

c     DO j=1,my
      DO i=1,mx
          zDown = gplvDY(i,j,mz) * grvinv - sh(i,j)
          DO kz=1,mztq
              zLev= OutZTQLevIB(kz)
              IF (zLev.lt.zDown) THEN
                  distUp = (zDown - zLev) / zDown
                  ttzIB_0(i,j,kz) = distUp * tairSL(i,j) +
     .                              (1-distUp) * tairDY(i,j,mz)
                  qqzIB_0(i,j,kz) = distUp * qvapSL(i,j) +
     .                              (1-distUp) * qvDY(i,j,mz)
                  ppzIB_0(i,j,kz) = (pstDY(i,j)
     .                            * sigma(kUp) +ptopDY) * 10
              ELSE
                  kUp = 1
                  kDown = mz
                  DO WHILE (kDown-kUp.gt.1)
                      kMiddle = (kDown + kUp) / 2
                      zMiddle = gplvDY(i,j,kMiddle) * grvinv - sh(i,j)
                      IF (zMiddle.le.zLev) THEN
                          kDown = kMiddle
                      ELSE
                          kUp = kMiddle
                      ENDIF
                  ENDDO
                  zUp = gplvDY(i,j,kUp) * grvinv - sh(i,j)
                  zDown = gplvDY(i,j,kDown) * grvinv - sh(i,j)
                  distUp = (zUp - zLev) / (zUp - zDown)
                  ttzIB_0(i,j,kz) = distUp * tairDY(i,j,kDown) +
     .                              (1-distUp) * tairDY(i,j,kUp)
                  qqzIB_0(i,j,kz) = distUp * qvDY(i,j,kDown) +
     .                              (1-distUp) * qvDY(i,j,kUp)
                  ppzIB_0(i,j,kz) = distUp *
     .                      (pstDY(i,j) * sigma(kDown)+ptopDY) * 10
     .                            + (1-distUp) *
     .                      (pstDY(i,j) * sigma(kUp)  +ptopDY) * 10
              ENDIF
              ! sum for the output
              ttzIB(i,j,kz) = ttzIB(i,j,kz) +
     .                        ttzIB_0(i,j,kz) - TfSnow
              qqzIB(i,j,kz) = qqzIB(i,j,kz) +
     .                        qqzIB_0(i,j,kz) * 1000.
          END DO
      END DO
c     END DO
 
C +--2.2.4.2 Wind
c     DO j=1,my
      DO i=1,mx
          zDown = gplvDY(i,j,mz) * grvinv - sh(i,j)
          DO kz=1,mzuv
              zLev= OutZUVLevIB(kz)
              IF (zLev.lt.zDown) THEN
                  distUp = (zDown - zLev) / zDown
                  uuzIB_0(i,j,kz) = distUp * 0. +
     .                              (1-distUp) * uairDY(i,j,mz)
                  vvzIB_0(i,j,kz) = distUp * 0. +
     .                              (1-distUp) * vairDY(i,j,mz)
                  u2zIB_0(i,j,kz) = distUp * 0. +
     .                              (1-distUp) * rotuuIB(i,j,mz)
                  v2zIB_0(i,j,kz) = distUp * 0. +
     .                              (1-distUp) * rotvvIB(i,j,mz)
                  rozIB_0(i,j,kz) = distUp * 0. +
     .                              (1-distUp) * rolvDY(i,j,mz)
              ELSE
                  kUp = 1
                  kDown = mz
                  DO WHILE (kDown-kUp.gt.1)
                      kMiddle = (kDown + kUp) / 2
                      zMiddle = gplvDY(i,j,kMiddle) * grvinv - sh(i,j)
                      IF (zMiddle.le.zLev) THEN
                          kDown = kMiddle
                      ELSE
                          kUp = kMiddle
                      ENDIF
                  ENDDO
                  zUp = gplvDY(i,j,kUp) * grvinv - sh(i,j)
                  zDown = gplvDY(i,j,kDown) * grvinv - sh(i,j)
                  distUp = (zUp - zLev) / (zUp - zDown)
                  uuzIB_0(i,j,kz) = distUp * uairDY(i,j,kDown) +
     .                              (1-distUp) * uairDY(i,j,kUp)
                  vvzIB_0(i,j,kz) = distUp * vairDY(i,j,kDown) +
     .                              (1-distUp) * vairDY(i,j,kUp)
                  u2zIB_0(i,j,kz) = distUp * rotuuIB(i,j,kDown) +
     .                              (1-distUp) * rotuuIB(i,j,kUp)
                  v2zIB_0(i,j,kz) = distUp * rotvvIB(i,j,kDown) +
     .                              (1-distUp) * rotvvIB(i,j,kUp)
                  rozIB_0(i,j,kz) = distUp * rolvDY(i,j,kDown) +
     .                              (1-distUp) * rolvDY(i,j,kUp)
              ENDIF
              ! sum for the output
              uuzIB(i,j,kz) = uuzIB(i,j,kz) + uuzIB_0(i,j,kz)
              vvzIB(i,j,kz) = vvzIB(i,j,kz) + vvzIB_0(i,j,kz)
              u2zIB(i,j,kz) = u2zIB(i,j,kz) + u2zIB_0(i,j,kz)
              v2zIB(i,j,kz) = v2zIB(i,j,kz) + v2zIB_0(i,j,kz)
              uvzIB(i,j,kz) = uvzIB(i,j,kz) +
     .             (uuzIB_0(i,j,kz)**2. + vvzIB_0(i,j,kz)**2.)**0.5
              rozIB(i,j,kz) = rozIB(i,j,kz) + rozIB_0(i,j,kz) * 1000.
          END DO
      END DO
c     END DO

C +--2.2.4.3 sea level pressure
c     DO j=1,my
      DO i=1,mx


        ! vertical gradiant between mz-15 and mz-10
c       zUp    = gplvDY(i,j,mz-15) * grvinv
c       zDown  = gplvDY(i,j,mz-10) * grvinv

c       tmp3=
c    .  (tairDY(i,j,mz-10)-tairDY(i,j,mz-15))/
c    .  (zup - zDown) 
c       tmp3=max(0.005,min(0.01,tmp3))
        tmp3=0.0065 ! standard gradiant

        slpIB(i,j)  = slpIB(i,j) +
     .              (pstDY(i,j) * sigma(mz-3) + ptopDY) *10 *
     .              (1 -              tmp3*gplvDY(i,j,mz-3)*grvinv /
     .              (tairDY(i,j,mz-3)+tmp3*gplvDY(i,j,mz-3)*grvinv))
     .              **(-5.257)

      END DO

c     END DO

 
C +--2.2.4.3 X-hourly variables
C +  --------------------------
 
c     DO j=1,my
      DO i=1,mx
 
          tnhIB0(i,j)  = min(tairDY(i,j,mz)-TfSnow,tnhIB0(i,j))
          txhIB0(i,j)  = max(tairDY(i,j,mz)-TfSnow,txhIB0(i,j))
 
      ENDDO
c     ENDDO

 
C +--2.2.4 Surface variables
C +  -----------------------


c     DO j=jp11,my1
      DO i=ip11,mx1
 
          if(RAdsol(i,j)>120)
     .    sunIB(i,j)      = sunIB(i,j)+dt*nbr_call_outice
                     ! outice is called every x time steps
 
          swdIB(i,j)      = RAdsol(i,j)             + swdIB(i,j)
          swuIB(i,j)      = albeSL(i,j)*RAdsol(i,j) + swuIB(i,j)
          lwdIB(i,j)      = RAd_ir(i,j)             + lwdIB(i,j)
          lwuIB(i,j)      = firmSL(i,j)             + lwuIB(i,j)
 
         lwutIB(i,j)      = RAdOLR(i,j)             + lwutIB(i,j)
         swutIB(i,j)      = RAdOSR(i,j)             + swutIB(i,j)
         swdtIB(i,j)      = rsunGE * czenGE(i,j)    + swdtIB(i,j)
          shfIB(i,j)      = -hsenSL(i,j)            + shfIB(i,j)
          lhfIB(i,j)      = -hlatSL(i,j)            + lhfIB(i,j)
           alIB(i,j)      = albeSL(i,j)             +  alIB(i,j)
         as1_IB(i,j)      = alb1IB(i,j)             + as1_IB(i,j)
         as2_IB(i,j)      = alb2IB(i,j)             + as2_IB(i,j)
         as3_IB(i,j)      = alb3IB(i,j)             + as3_IB(i,j)
           stIB(i,j)      = tairsl(i,j)-TfSnow      +  stIB(i,j)
          sicIB(i,j)      = ifratv(i,j,2)           + sicIB(i,j)
          pddIB(i,j)      = pddIB(i,j)+
     .                                (tairDY(i,j,mz)-TfSnow)
     .                    * max(zero,sign(unun,
     .                                 tairDY(i,j,mz)-TfSnow))
       if (coupling_ao .eqv. .true.) then
         sicaoIB (i,j)     = sicsAO(i,j)             + sicaoIB(i,j)  
         sitaoIB (i,j)     = hicao(i,j)              + sitaoIB(i,j)
         sntaoIB (i,j)     = hsnoao(i,j)             + sntaoIB(i,j)
       endif

        DO k=1,nsx
          al1IB(i,j,k)    = RAdsol(i,j)*albxSL(i,j,k)+al1IB(i,j,k)
          al2IB(i,j,k)    = albxSL(i,j,k)           + al2IB(i,j,k)
          frvIB(i,j,k)    = ifratv(i,j,k)           + frvIB(i,j,k)
          st2IB(i,j,k)    = tsrfSL(i,j,k)-TfSnow    + st2IB(i,j,k)
          z0IB (i,j,k)    = SL_Z0 (i,j,k)           + z0IB (i,j,k)
          r0IB (i,j,k)    = SL_R0 (i,j,k)           + r0IB (i,j,k)
          uusIB(i,j,k)    = SLuusl(i,j,k)           + uusIB(i,j,k)
          utsIB(i,j,k)    = SLutsl(i,j,k)           + utsIB(i,j,k)
          uqsIB(i,j,k)    = SLuqsl(i,j,k)           + uqsIB(i,j,k)
          ussIB(i,j,k)    = SLussl(i,j,k)           + ussIB(i,j,k)
          uusthIB(i,j,k)  = SaltSN(i,j,k)           + uusthIB(i,j,k)
          if (coupling_ao .eqv. .true.) then
           st2aoIB (i,j,k)= srftao(i,j,k)           + st2aoIB(i,j,k) 
           albaoIB (i,j,k)= albao(i,j,k)            + albaoIB(i,j,k) 
          endif
        ENDDO
 
      END DO
c     END DO
 
C +--2.2.5 Cloud Variables
C +  ---------------------
 
c     DO j=1,my
      DO i=1,mx
 
        DO k=1,3
          tmp(k)          = 0.0
        ENDDO
 
        DO k = mzhyd,mz
          tmp(3)          = (pstDY(i,j)  * sigma(k)+ptopDY)
     .                    / (ra*tairDY(i,j,k)*(1.+.608   *qvDY(i,j,k)))
     .                    * (   gpmiDY(i,j,k)-          gpmiDY(i,j,k+1))
          tmp(1)          = tmp(1)       + tmp(3)       * qwHY(i,j,k)
          tmp(2)          = tmp(2)       + tmp(3)       * qiHY(i,j,k)
        END DO
 
          codIB(i,j)      = 1.5 * ( tmp(1) / 20.d-6
     .                            + tmp(2) / 40.d-6 ) *grvinv
     .                            + codIB (i,j)
CC#EE     codIB(i,j)      = RAcdtO(i,j)+codIB (i,j)
 
        DO k = mzhyd,mz
         tmp(1)           = rolvDY(i,j,k) * 1000.
     .                    *((gplvDY(i,j,k-1)+gplvDY(i,j,k  ))/2.
     .                    - (gplvDY(i,j,k)  +gplvDY(i,j,k+1))/2.)
     .                    * grvinv
 
         wvpIB(i,j)       = wvpIB(i,j)  +  qvDY(i,j,k) * tmp(1)
         cwpIB(i,j)       = cwpIB(i,j)  + (qwHY(i,j,k)+qrHY(i,j,k))
     .                    * tmp(1)
         iwpIB(i,j)       = iwpIB(i,j)  + (qiHY(i,j,k)+qsHY(i,j,k))
     .                    * tmp(1)
 
        qwIB(i,j)         = qwIB(i,j)   + qwHY(i,j,k) * tmp(1)
        qiIB(i,j)         = qiIB(i,j)   + qiHY(i,j,k) * tmp(1)
        qsIB(i,j)         = qsIB(i,j)   + qsHY(i,j,k) * tmp(1)
        qrIB(i,j)         = qrIB(i,j)   + qrHY(i,j,k) * tmp(1)
 
        ENDDO
 
          ccIB (i,j)      = cld_SL(i,j) + ccIB (i,j)
          cuIB (i,j)      = clduSL(i,j) + cuIB (i,j)
          cmIB (i,j)      = cldmSL(i,j) + cmIB (i,j)
          cdIB (i,j)      = clddSL(i,j) + cdIB (i,j)
      ENDDO
c     ENDDO
 
C +--2.2.6 Soil Variables
C +  --------------------
 
c     DO j=jp11,my1
      DO i=ip11,mx1
        DO k=1,nsx
          DO kk=1,llx
            sltIB(i,j,k,kk) = TsolTV(i,j,k,kk)-TfSnow + sltIB (i,j,k,kk)
            slqIB(i,j,k,kk) = Eta_TV(i,j,k,kk)*1000.  + slqIB (i,j,k,kk)
          ENDDO
        ENDDO
      ENDDO
c     ENDDO
 
 
C +--2.2.7 Snow Pack Variables
C +  -------------------------
 
      DO k=1,nsx
c      DO j=jp11,my1
       DO i=ip11,mx1
        IF(nssSNo(i,j,k).gt.1) THEN
 
         depthsnow(nssSNo(i,j,k))=dzsSNo(i,j,k,nssSNo(i,j,k))/2.
 
         DO nk=nssSNo(i,j,k)-1,1,-1
          depthsnow(nk)=depthsnow(nk+1)+dzsSNo(i,j,k,nk  )/2.
     .                                 +dzsSNo(i,j,k,nk+1)/2.
         enddo
 
         DO kk=1,mi
 
          IF(OutshIB(kk)<=depthsnow(nssSNo(i,j,k))) then
           agIB(i,j,k,kk) = agsSNo(i,j,k,nssSNo(i,j,k))
     .    +agIB(i,j,k,kk)
           g1IB(i,j,k,kk) = g1sSNo(i,j,k,nssSNo(i,j,k))
     .    +g1IB(i,j,k,kk)
           g2IB(i,j,k,kk) = g2sSNo(i,j,k,nssSNo(i,j,k))
     .    +g2IB(i,j,k,kk)
           roIB(i,j,k,kk) = rosSNo(i,j,k,nssSNo(i,j,k))
     .    +roIB(i,j,k,kk)
           tiIB(i,j,k,kk) = tisSNo(i,j,k,nssSNo(i,j,k))
     .    +tiIB(i,j,k,kk)
           waIB(i,j,k,kk) = wasSNo(i,j,k,nssSNo(i,j,k))
     .    +waIB(i,j,k,kk)
          ENDIF
 
          IF(OutshIB(kk)>=depthsnow(1)) then
           agIB(i,j,k,kk) = agsSNo(i,j,k,1)
     .    +agIB(i,j,k,kk)
           g1IB(i,j,k,kk) = g1sSNo(i,j,k,1)
     .    +g1IB(i,j,k,kk)
           g2IB(i,j,k,kk) = g2sSNo(i,j,k,1)
     .    +g2IB(i,j,k,kk)
           roIB(i,j,k,kk) = rosSNo(i,j,k,1)
     .    +roIB(i,j,k,kk)
           tiIB(i,j,k,kk) = tisSNo(i,j,k,1)
     .    +tiIB(i,j,k,kk)
           waIB(i,j,k,kk) = wasSNo(i,j,k,1)
     .    +waIB(i,j,k,kk)
          ENDIF
 
          IF(OutshIB(kk)>depthsnow(nssSNo(i,j,k)).and.
     .       OutshIB(kk)<depthsnow(1)) then
 
           nk=nssSNo(i,j,k)
           DO WHILE(OutshIB(kk)>depthsnow(nk))
            nk=nk-1
           ENDDO
           agIB(i,j,k,kk) = agsSNo(i,j,k,nk+1)+
     .    (agsSNo(i,j,k,nk)-agsSNo(i,j,k,nk+1))/
     .    (depthsnow(nk)-depthsnow(nk+1))*(OutshIB(kk)-depthsnow(nk+1))
     .    +agIB(i,j,k,kk)
 
           g1IB(i,j,k,kk) = g1sSNo(i,j,k,nk+1)+
     .    (g1sSNo(i,j,k,nk)-g1sSNo(i,j,k,nk+1))/
     .    (depthsnow(nk)-depthsnow(nk+1))*(OutshIB(kk)-depthsnow(nk+1))
     .    +g1IB(i,j,k,kk)
 
           g2IB(i,j,k,kk) = g2sSNo(i,j,k,nk+1)+
     .    (g2sSNo(i,j,k,nk)-g2sSNo(i,j,k,nk+1))/
     .    (depthsnow(nk)-depthsnow(nk+1))*(OutshIB(kk)-depthsnow(nk+1))
     .    +g2IB(i,j,k,kk)
 
           tiIB(i,j,k,kk) = tisSNo(i,j,k,nk+1)+
     .    (tisSNo(i,j,k,nk)-tisSNo(i,j,k,nk+1))/
     .    (depthsnow(nk)-depthsnow(nk+1))*(OutshIB(kk)-depthsnow(nk+1))
     .    +tiIB(i,j,k,kk)
 
           roIB(i,j,k,kk) = rosSNo(i,j,k,nk+1)+
     .    (rosSNo(i,j,k,nk)-rosSNo(i,j,k,nk+1))/
     .    (depthsnow(nk)-depthsnow(nk+1))*(OutshIB(kk)-depthsnow(nk+1))
     .    +roIB(i,j,k,kk)
 
           waIB(i,j,k,kk) = wasSNo(i,j,k,nk+1)+
     .    (wasSNo(i,j,k,nk)-wasSNo(i,j,k,nk+1))/
     .    (depthsnow(nk)-depthsnow(nk+1))*(OutshIB(kk)-depthsnow(nk+1))
     .    +waIB(i,j,k,kk)
          ENDIF
 
 
         ENDDO
 
        ENDIF
       ENDDO
       ENDDO
      ENDDO
!$OMP END PARALLEL DO

 
      do kk=1,mlhh
      if(OutdyIB==1.and.iterun>10.and.timehIB(kk)==0.and.(
     .   jhurGE*3600+minuGE*60+jsecGE>=(kk)*86400./real(mlhh) .or.
     .  (jhurGE*3600+minuGE*60+jsecGE<=dt.and.kk==mlhh))) then
        timehIB(kk)     = jhurGE+real(minuGE)/100.+real(jsecGE)/10000.
        print *,"OUTice x-hourly outputs",kk,timehIB(kk)
        do j=1,my
        do i=1,mx
 
cC +--------Soil temperature and humidity (slth,slqch)	
cC +        ------------------------------------------    	
c	   tmp=0.; slth(i,j,kk)=0.; slqch(i,j,kk)=0.
c	   do k=1,llx						
c	      tmp(1)=tmp(1)+dz_dSV(k)				
c	   do n=1,nvx ! Sous-mailles (3)			
c	      slth(i,j,kk) = (TsolTV(i,j,n,k) - TfSnow) * dz_dSV(llx-k)
c     .		   	   * SLsrfl(i,j,n) + slth(i,j,kk)	
c								
c	      slqch(i,j,kk) = (Eta_TV(i,j,n,k)*1000.* dz_dSV(llx-k))
c     .		   	    * SLsrfl(i,j,n) + slqch(i,j,kk)	
c								
c	   enddo						
c	   enddo						
c           slth(i,j,kk) = slth(i,j,kk)/tmp(1)			
c	   slqch(i,j,kk) = slqch(i,j,kk)/tmp(1)		  	
c
cC +--------Convective precipitation (cph)			
cC +        --------------------------------			
c	   cphIB(i,j,kk) = rainCA(i,j) + snowCA(i,j) - cph0IB(i,j)
c	   cph0IB(i,j)   = rainCA(i,j) + snowCA(i,j)
c	   cphIB(i,j,kk) = cphIB(i,j,kk) * 1000.
cC +--------Water Vapor Path (wvph)		           	
cC +        -----------------------             			
c	   wvph(i,j,kk) = 0.					
c	   do k = 2,mz         				
c	      tmp(1) = rolvDY(i,j,k)  				
c     .               * ((gplvDY(i,j,k-1) + gplvDY(i,j,k  )) / 2.
c     .               - (gplvDY(i,j,k)    + gplvDY(i,j,k+1)) / 2.)
c     .               * grvinv  					
c	      wvph(i,j,kk) = wvph(i,j,kk) + qvDY(i,j,k) * 1000.
c     .                                    * tmp(1)
c	   enddo					
c
cC +--------Dew Point Temperature (Td)				
cC +        --------------------------				
c           q   = qvDY(i,j,mz)					
c           qst = qsat0D(tairDY(i,j,mz),sigma(mz),pstDY(i,j),ptopDY,1)
c	    r   = q / max(epsi,1.-q)				
c           rst = qst / max(epsi,1.-qst)			
c	    rhh = (r/(0.622+r)) / max(epsi,(rst/(0.622+rst))) * 100.
c	    rhh = max(0.,min(100.,rhh))				
c	    tdh(i,j,kk) = tairDY(i,j,mz) - ((100-rhh)/5.) - 273.15
c
cC +--------Zenithal variables (ZTD,ZHD,ZWD)			
cC +        --------------------------------			           	
c	    kUp = 1						
c            kDown = mz						
c            kMiddle = (kDown + kUp) / 2.			
c								
c	    zhdh(i,j,kk)=0 ; zwdh(i,j,kk)=0			
c								
c	    do i_hi=1,mz-1					
c	       i_lo = i_hi+1.					
c	       refrac_h = k1p / tairDY(i,j,i_lo)		
c	       refrac_w = k2p * qvDY(i,j,i_lo) / (tairDY(i,j,i_lo)**2.
c     .		              * (E+(1.-E)*qvDY(i,j,i_lo)))
c               p_hi = (pstDY(i,j) * sigma(i_hi) + ptopDY) * 10.
c	       p_lo = (pstDY(i,j) * sigma(i_lo) + ptopDY) * 10.
c!	       z_hi = gpmiDY(i,j,i_hi)/grvinv			
c!	       z_lo = gpmiDY(i,j,i_lo)/grvinv			
c               z_hi = gplvDY(i,j,i_hi) * grvinv
c               z_lo = gplvDY(i,j,i_lo) * grvinv
c	       kappa = log(p_lo/p_hi) / (z_hi-z_lo)
c	       zhdh(i,j,kk) = zhdh(i,j,kk) + refrac_h * (p_lo-p_hi)/kappa
c	       zwdh(i,j,kk) = zwdh(i,j,kk) + refrac_w * (p_lo-p_hi)/kappa
c	    enddo						
c								
c	    ztdh(i,j,kk) = zhdh(i,j,kk) + zwdh(i,j,kk)		
c
c
cC +--------Weighted Mean Temperature (Tm)			
cC +        ------------------------------			      	 							
c     	    num=0 ; den=0					
c								
c	    do i_hi=1,mz-1					
c	       i_lo = i_hi+1.					
c	       refrac_h = qvDY(i,j,i_lo) / (tairDY(i,j,i_lo)
c     .		              * (E+(1.-E)*qvDY(i,j,i_lo)))	
c	       refrac_w = qvDY(i,j,i_lo) / (tairDY(i,j,i_lo)**2.
c     .		              * (E+(1.-E)*qvDY(i,j,i_lo)))	
c	       p_hi = (pstDY(i,j) * sigma(i_hi) + ptopDY) * 10.
c	       p_lo = (pstDY(i,j) * sigma(i_lo) + ptopDY) * 10.
c	       z_hi = gplvDY(i,j,i_hi) * grvinv			
c	       z_lo = gplvDY(i,j,i_lo) * grvinv			
c	       kappa = log(p_lo/p_hi) / (z_hi-z_lo)		
c	       num = num + refrac_h * (p_lo-p_hi)/kappa		
c	       den = den + refrac_w * (p_lo-p_hi)/kappa		
c	    enddo						
c	    tmh(i,j,kk) = num / den				
c	
cC +--------50m-level variables
cC +        -------------------
c
c            kz=3
c            DO k=1,mzuv
c             if(OutZUVLevIB(k)==50) kz=k
c            enddo
c
c            u5hIB(i,j,kk)  =  uuzIB_0(i,j,kz)
c            v5hIB(i,j,kk)  =  vvzIB_0(i,j,kz)
c
c            kz=3
c            DO k=1,mztq
c             if(OutZTQLevIB(k)==50) kz=k
c            enddo
c
c            t5hIB(i,j,kk)  =  ttzIB_0(i,j,kz)-273.15
c            q5hIB(i,j,kk)  =  qqzIB_0(i,j,kz)*1000.
c            p5hIB(i,j,kk)  =  ppzIB_0(i,j,kz)
c
c           capeh(i,j,kk)  = capeCA(i,j)	

            irhoIB(i,j,kk) = 
     .           SUM(wcrhos(i,j,:) * (unun / SUM(lyrthk) * lyrthk)) !*AJT*
 
            sphIB(i,j,kk)  = pstDY(i,j) * 10.
            sthIB(i,j,kk)  = tairsl(i,j)    - TfSnow
            tthIB(i,j,kk)  = tairDY(i,j,mz) - TfSnow
            txhIB(i,j,kk)  = txhIB0(i,j) ; txhIB0(i,j)=-99.
            tnhIB(i,j,kk)  = tnhIB0(i,j) ; tnhIB0(i,j)= 99.
            qqhIB(i,j,kk)  = qvDY(i,j,mz) * 1000.
c           uuhIB(i,j,kk)  = (uairDY(i,j,mz-2)+uairDY(i,j,mz-3))/2. ! ~10m
c           vvhIB(i,j,kk)  = (vairDY(i,j,mz-2)+vairDY(i,j,mz-3))/2. ! ~10m
            uuhIB(i,j,kk)  = uairDY(i,j,mz) ! ~2m
            vvhIB(i,j,kk)  = vairDY(i,j,mz) ! ~2m
            swdhIB(i,j,kk) = RAdsol(i,j)
            lwdhIB(i,j,kk) = RAd_ir(i,j)
            lwuhIB(i,j,kk) = firmSL(i,j)
            shfhIB(i,j,kk) = (-hsenSL(i,j))
            lhfhIB(i,j,kk) = (-hlatSL(i,j))
            alhIB(i,j,kk)  = albeSL(i,j)
            clhIB(i,j,kk)  = cld_SL(i,j)
            snfhIB(i,j,kk) = snowHY(i,j)*1000. - snfh0IB(i,j)
            snfh0IB(i,j)   = snowHY(i,j)*1000.
            prhIB(i,j,kk)  = (rainHY(i,j)+snowHY(i,j)) * 1000.
     .                       - prh0IB(i,j)
            prh0IB(i,j)    = (rainHY(i,j)+snowHY(i,j)) * 1000.
            rfhIB(i,j,kk)  = max(0.,prhIB(i,j,kk) - snfhIB(i,j,kk))
            mehIB(i,j,kk)  = (-1.)*wem_IB(i,j,1) - meh0IB(i,j)
            meh0IB(i,j)    = (-1.)*wem_IB(i,j,1)
            suhIB(i,j,kk)  = wee_IB(i,j,1,3) - suh0IB(i,j)
            suh0IB(i,j)    = wee_IB(i,j,1,3)
 
                       k=1 ! only snow
            wet_IB(i,j,k)  = 0.
            DO nk=nsno,1,-1
            wet_IB(i,j,k)  = rosSNo(i,j,k,nk) * dzsSNo(i,j,k,nk)
     .                     * 1.d3/ ro_Wat     *(1.+0.*wasSNo(i,j,k,nk))
     .                     + wet_IB(i,j,k)
            ENDDO
            wet_IB(i,j,k)  = wet_IB(i,j,k)   !+      SWaSNo(i,j,k) 
            smbhIB(i,j,kk) = wet_IB(i,j,k)    -      smbh0IB(i,j)
            smbh0IB(i,j)   = wet_IB(i,j,k)
 
            swhIB(i,j,kk)  = SWaSNo(i,j,k)    -      swh0IB(i,j)
            swh0IB(i,j)    = SWaSNo(i,j,k)
 
            ruhIB(i,j,kk)  = weu_IB(i,j,k)    -      ruh0IB(i,j)
            ruh0IB(i,j)    = weu_IB(i,j,k)
            lwc1mhIB(i,j,kk) = 0.
            lwc2mhIB(i,j,kk) = 0.
            nx = 1 ! Sector 1
            nk = max(1,nssSNo(i,j,nx))
            depthSNo = dzsSNo(i,j,nx,nk)/2.
            do while(depthSNo.le.1. .and. nk.gt.1)
                lwc1mhIB(i,j,kk) = lwc1mhIB(i,j,kk) +
     .                          0.1               * ! bug 11/10/2017
     .                          wasSNo(i,j,nx,nk) *
     .                          rosSNo(i,j,nx,nk) * dzsSNo(i,j,nx,nk)
                depthSNo = depthSNo +
     .                   (dzsSNo(i,j,nx,nk) + dzsSNo(i,j,nx,nk-1))/2.
                nk = nk - 1
            enddo
            nk = max(1,nssSNo(i,j,nx))
            depthSNo = dzsSNo(i,j,nx,nk)/2.
            do while(depthSNo.le.2. .and. nk.gt.1)
                lwc2mhIB(i,j,kk) = lwc2mhIB(i,j,kk) +
     .                          0.1               * ! bug 11/10/2017
     .                          wasSNo(i,j,nx,nk) *
     .                          rosSNo(i,j,nx,nk) * dzsSNo(i,j,nx,nk)
                depthSNo = depthSNo +
     .                   (dzsSNo(i,j,nx,nk) + dzsSNo(i,j,nx,nk-1))/2.
                nk = nk - 1
            enddo
        enddo
        enddo
      endif
      enddo
 

 
C +--2.2.8 Slush and Superimposed Ice
C +  --------------------------------
 
c      DO k= 1,nsx
c
c        DO j=jp11,my1
c        DO i=ip11,mx1
c
c            WKxy1(i,j) = 1.
c
c          DO kk=1,nssSNo(i,j,k)
c
c            WKxy3(i,j)     = max( 0., sign(1., rosSNo(i,j,k,kk)-roCdSV))
c     .                     * max( 0., sign(1.,-wasSNo(i,j,k,kk)+epsi  ))
c
c            WKxy1(i,j)     = min( WKxy1(i,j) ,  WKxy3(i,j)
c     .                     + max( 0., sign(1.,-nsiiIB(i,j,k)   +epsi)))
c
c            nsiiIB (i,j,k) = max(nsiiIB(i,j,k),
c     .                     kk*int(WKxy1(i,j)  * WKxy3(i,j)))
c            siiceIB(i,j,k) =    siiceIB(i,j,k)+dzsSNo(i,j,k,kk)
c     .                           *WKxy1(i,j)  * WKxy3(i,j)
c          END DO
c
c            siIB   (i,j,k)   =   siiceIB(i,j,k)  +    siIB(i,j,k)
c            slushIB(i,j,k)   =         0.
c            nsluIB (i,j,k)   =    nsiiIB(i,j,k)
c        END DO
c        END DO
c      END DO
c
c      DO k=    1,nsx
c
c        DO j=jp11,my1
c        DO i=ip11,mx1
c
c            WKxy2(i,j) = 1.
c
c          DO kk=1,nssSNo(i,j,k)
c            WKxy3(i,j)= max(0., sign(1., rosSNo(i,j,k,kk)- roCdSV))
c     .                * max(0., sign(1., wasSNo(i,j,k,kk)- 0.1e0 ))
c     .                * max(0 , sign(1 , kk-max(1,nsiiIB(i,j,k) )))
c            WKxy2(i,j)= min(WKxy2(i,j) ,  WKxy3(i,j)
c     .                + max(0., sign(1., nsiiIB(i,j,k)
c     .                                  -nsluIB(i,j,k) + epsi)))
c            nsluIB (i,j,k) = max(nsluIB(i,j,k),
c     .                     kk*int(WKxy2(i,j)*WKxy3(i,j)))
c            slushIB(i,j,k) =    slushIB(i,j,k)
c     .                     +     dzsSNo(i,j,k,kk)
c     .                           *WKxy2(i,j)*WKxy3(i,j)
c          END DO
c
c            suIB   (i,j,k)   =   slushIB(i,j,k)  +    suIB(i,j,k)
c            nsluIB (i,j,k)   =    nsluIB(i,j,k)  -  nsiiIB(i,j,k)
c        END DO
c        END DO
c      END DO
 
 
C +       snow      (with ice lenses perhaps ! )
C + - - - - - - - -
C +      slush      (ro > 830kg/m3 and wa > 0.1)
C + - - - - - - - -
C + surimposed ice  (ro > 830kg/m3 and wa = 0  )
C + - - - - - - - -
C +       ice       (ro = 900kg/m3 and wa = 0  )
C + - - - - - - - -
C +      ground
 
 
C ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C +++  3. Output  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 
      IF ((iterun-itrdIB+nbr_call_outice)>=43200./real(OutdyIB*dt) .and.
     . jhurGE*60+minuGE>=abs(real(OutdyIB0)-0.5)*1440./real(OutdyIB)
     . .and.
     . jhurGE*60+minuGE< abs(real(OutdyIB0)-0.5)*1440./real(OutdyIB)
     .                       +nbr_call_outice*dt/60) then
 
        iyrrIB=iyrrGE
        mmarIB=mmarGE
        jdarIB=jdarGE
        jhurIB=jhurGE
        minuIB=minuGE
 
      ENDIF
 
      IF ((iterun-itrdIB+nbr_call_outice)>=86400./real(OutdyIB*dt).and.
     .     jhurGE*60+minuGE>=OutdyIB0*1440./real(OutdyIB)          .and.
     .     jhurGE*60+minuGE< OutdyIB0*1440./real(OutdyIB)
     .                       +nbr_call_outice*dt/60.) then
 
cXF WARNING: it does not work if MAR starts at 6h or 12h !!!
cXF if it is the case, nbr_call_outice=1 and
cXF IF ((iterun-itrdIB)*dt+unun.gt.86400.0/real(OutdyIB))  THEN
 
 
        if(dt_ICE2.gt. iterun-itrdIB) then
         print *,"OUTice error: dt_ICE2 > iterun-itrdIB",
     .   dt_ICE2,iterun-itrdIB
        endif
 
C +--3.0 Snapshot of Snow Height
C +  ===========================
 
        DO k=1,nsx
        DO j=1,my
        DO i=1,mx
            zn1IB (i,j,k) = 1.
            zn2IB (i,j,k) = 0.
            zn3IB (i,j,k) = 0.
            wet_IB(i,j,k) = 0.
        END DO
        END DO
        END DO
 
        DO kk=nsno,1,-1
        DO k=1,nsx
        DO j=1,my
        DO i=1,mx
            zn3IB (i,j,k) = dzsSNo(i,j,k,kk)   + zn3IB(i,j,k)
            wet_IB(i,j,k) = rosSNo(i,j,k,kk)   * dzsSNo(i,j,k,kk)
     .                    * 1.d3/ ro_Wat       *(1.+0.*wasSNo(i,j,k,kk))
     .                    + wet_IB(i,j,k)
            zn1IB (i,j,k) = zn1IB(i,j,k)
     .                    * max(zero,sign(unun,
     .                      ro_ice-20.-rosSNo(i,j,k,kk)))
            zn2IB (i,j,k) = dzsSNo(i,j,k,kk)   * zn1IB(i,j,k)
     .                                         + zn2IB(i,j,k)
        END DO
        END DO
        END DO
        END DO
 
        DO k=1,nsx
        DO j=1,my
        DO i=1,mx
            wet_IB(i,j,k)  = wet_IB(i,j,k) +     SWaSNo(i,j,k)
            zn2IB (i,j,k)  = zn2IB (i,j,k) * (1.- zn1IB(i,j,k))
            zn1IB (i,j,k)  = zn3IB (i,j,k)      - zn0IB(i,j,k)
            mbIB  (i,j,k)  = wet_IB(i,j,k)      - mb0IB(i,j,k)
        END DO
        END DO
        END DO
 
 
        DO j=1,my
        DO i=1,mx
 
 
C +--3.1 Computation of Averaged Values
C +  ==================================
 
          qbrIB(i,j)      = qbrIB(i,j)      / real(dt_ICE2)
          pddIB(i,j)      = pddIB(i,j)      / real(dt_ICE2)
 
        DO kk =1,ml
          ttIB( i,j,kk)   = ttIB (i,j,kk)   / real(dt_ICE2)
          tdIB( i,j,kk)   = tdIB (i,j,kk)   / real(dt_ICE2)
          uuIB (i,j,kk)   = uuIB (i,j,kk)   / real(dt_ICE2)
          vvIB (i,j,kk)   = vvIB (i,j,kk)   / real(dt_ICE2)
          uvIB (i,j,kk)   = uvIB (i,j,kk)   / real(dt_ICE2)
          wwIB (i,j,kk)   = wwIB (i,j,kk)   / real(dt_ICE2)
          ruuIB (i,j,kk)  = ruuIB(i,j,kk)   / real(dt_ICE2)
          rvvIB (i,j,kk)  = rvvIB(i,j,kk)   / real(dt_ICE2)
          ruvIB (i,j,kk)  = ruvIB(i,j,kk)   / real(dt_ICE2)
          qqIB (i,j,kk)   = qqIB (i,j,kk)   / real(dt_ICE2)
          rolvIB(i,j,kk)  =rolvIB (i,j,kk)  / real(dt_ICE2)
          rhIB (i,j,kk)   = rhIB (i,j,kk)   / real(dt_ICE2)
          zzIB (i,j,kk)   = zzIB (i,j,kk)   / real(dt_ICE2)
          tkeIB(i,j,kk)   = tkeIB(i,j,kk)   / real(dt_ICE2)
          lqsIB(i,j,kk)   = lqsIB(i,j,kk)   / real(dt_ICE2)
          lqiIB(i,j,kk)   = lqiIB(i,j,kk)   / real(dt_ICE2)
          lqrIB(i,j,kk)   = lqrIB(i,j,kk)   / real(dt_ICE2)
          lqwIB(i,j,kk)   = lqwIB(i,j,kk)   / real(dt_ICE2)
          qsbIB(i,j,kk)   = qsbIB(i,j,kk)   / real(dt_ICE2)
          lsbIB(i,j,kk)   = lsbIB(i,j,kk)   / real(dt_ICE2)
          swn3DIB(i,j,kk) = swn3DIB(i,j,kk) / real(dt_ICE2)
          lwn3DIB(i,j,kk) = lwn3DIB(i,j,kk) / real(dt_ICE2)
          swnc3DIB(i,j,kk) = swnc3DIB(i,j,kk) / real(dt_ICE2)
          lwnc3DIB(i,j,kk) = lwnc3DIB(i,j,kk) / real(dt_ICE2)
          cod3DIB(i,j,kk) = cod3DIB(i,j,kk) / real(dt_ICE2)
          cc3DIB(i,j,kk)  = cc3DIB(i,j,kk)  / real(dt_ICE2)
        ENDDO
        DO kp =1,mp
          if(nbpIB(i,j,kp).gt.0) then
              ttpIB (i,j,kp)  = ttpIB (i,j,kp)  / nbpIB(i,j,kp)
              uupIB (i,j,kp)  = uupIB (i,j,kp)  / nbpIB(i,j,kp)
              vvpIB (i,j,kp)  = vvpIB (i,j,kp)  / nbpIB(i,j,kp)
              wwpIB (i,j,kp)  = wwpIB (i,j,kp)  / nbpIB(i,j,kp)
              uvpIB (i,j,kp)  = uvpIB (i,j,kp)  / nbpIB(i,j,kp)
              qqpIB (i,j,kp)  = qqpIB (i,j,kp)  / nbpIB(i,j,kp)
              zzpIB (i,j,kp)  = zzpIB (i,j,kp)  / nbpIB(i,j,kp)
          else
              ttpIB (i,j,kp)  = NF_FILL_REAL
              uupIB (i,j,kp)  = NF_FILL_REAL
              vvpIB (i,j,kp)  = NF_FILL_REAL
              wwpIB (i,j,kp)  = NF_FILL_REAL
              uvpIB (i,j,kp)  = NF_FILL_REAL
              qqpIB (i,j,kp)  = NF_FILL_REAL
              zzpIB (i,j,kp)  = NF_FILL_REAL
          endif
        ENDDO
        DO kz =1,mztq
          ttzIB( i,j,kz)  = ttzIB (i,j,kz)  / real(dt_ICE2)
          qqzIB (i,j,kz)  = qqzIB (i,j,kz)  / real(dt_ICE2)
        ENDDO
        DO kz =1,mzuv
          uuzIB( i,j,kz)  = uuzIB (i,j,kz)  / real(dt_ICE2)
          vvzIB (i,j,kz)  = vvzIB (i,j,kz)  / real(dt_ICE2)
          u2zIB( i,j,kz)  = u2zIB (i,j,kz)  / real(dt_ICE2)
          v2zIB (i,j,kz)  = v2zIB (i,j,kz)  / real(dt_ICE2)
          uvzIB (i,j,kz)  = uvzIB (i,j,kz)  / real(dt_ICE2)
          rozIB( i,j,kz)  = rozIB (i,j,kz)  / real(dt_ICE2)
        ENDDO
        DO k=1,nsx
          pblIB(i,j,k)    = pblIB(i,j,k)    / real(dt_ICE2)
          al1IB(i,j,k)    = al1IB(i,j,k)    / max(swdIB(i,j),epsi)
          al2IB(i,j,k)    = al2IB(i,j,k)    / real(dt_ICE2)
        ENDDO
           alIB(i,j)      =  alIB(i,j)      / real(dt_ICE2)
         as1_IB(i,j)      =as1_IB(i,j)      / real(dt_ICE2)
         as2_IB(i,j)      =as2_IB(i,j)      / real(dt_ICE2)
         as3_IB(i,j)      =as3_IB(i,j)      / real(dt_ICE2)
          swdIB(i,j)      = swdIB(i,j)      / real(dt_ICE2)
          swuIB(i,j)      = swuIB(i,j)      / real(dt_ICE2)
          lwdIB(i,j)      = lwdIB(i,j)      / real(dt_ICE2)
          lwuIB(i,j)      = lwuIB(i,j)      / real(dt_ICE2)
         swdtIB(i,j)      =swdtIB(i,j)      / real(dt_ICE2)
         swutIB(i,j)      =swutIB(i,j)      / real(dt_ICE2)
         lwutIB(i,j)      =lwutIB(i,j)      / real(dt_ICE2)
          shfIB(i,j)      = shfIB(i,j)      / real(dt_ICE2)
          lhfIB(i,j)      = lhfIB(i,j)      / real(dt_ICE2)
          spIB (i,j)      = spIB (i,j)      / real(dt_ICE2)
          slpIB(i,j)      = slpIB(i,j)      / real(dt_ICE2)
         if (mw .eq. 5) then
       gradTIB (i,j)      = gradTIB (i,j)   / real(dt_ICE2) !*CL*
       gradQIB (i,j)      = gradQIB (i,j)   / real(dt_ICE2) !*CL*
          endif
          ccIB (i,j)      = ccIB (i,j)      / real(dt_ICE2)
          cuIB (i,j)      = cuIB (i,j)      / real(dt_ICE2)
          cmIB (i,j)      = cmIB (i,j)      / real(dt_ICE2)
          cdIB (i,j)      = cdIB (i,j)      / real(dt_ICE2)
          qwIB (i,j)      = qwIB (i,j)      / real(dt_ICE2)
          qiIB (i,j)      = qiIB (i,j)      / real(dt_ICE2)
          qsIB (i,j)      = qsIB (i,j)      / real(dt_ICE2)
          qrIB (i,j)      = qrIB (i,j)      / real(dt_ICE2)
          wvpIB(i,j)      = wvpIB(i,j)      / real(dt_ICE2)
          cwpIB(i,j)      = cwpIB(i,j)      / real(dt_ICE2)
          iwpIB(i,j)      = iwpIB(i,j)      / real(dt_ICE2)
          codIB(i,j)      = codIB(i,j)      / real(dt_ICE2)
          stIB (i,j)      = stIB (i,j)      / real(dt_ICE2)
          sicIB(i,j)      = sicIB(i,j)      / real(dt_ICE2)
	  if (coupling_ao .eqv. .true.) then
	   sicaoIB (i,j)     = sicaoIB(i,j) / real(dt_ICE2)  
	   sitaoIB (i,j)     = sitaoIB(i,j) / real(dt_ICE2)
	   sntaoIB (i,j)     = sntaoIB(i,j) / real(dt_ICE2)
	   DO k=1,nsx
	   st2aoIB (i,j,k)= st2aoIB(i,j,k)  / real(dt_ICE2) 
	   albaoIB (i,j,k)= albaoIB(i,j,k)  / real(dt_ICE2)
	   ENDDO
	  endif 

        DO k=1,nsx
          frvIB(i,j,k)    = frvIB(i,j,k)    / real(dt_ICE2)
          st2IB(i,j,k)    = st2IB(i,j,k)    / real(dt_ICE2)
          z0IB (i,j,k)    = z0IB (i,j,k)    / real(dt_ICE2)
          r0IB (i,j,k)    = r0IB (i,j,k)    / real(dt_ICE2)
          uusIB(i,j,k)    = uusIB(i,j,k)    / real(dt_ICE2)
          uusthIB(i,j,k)  = uusthIB(i,j,k)  / real(dt_ICE2)
          utsIB(i,j,k)    = utsIB(i,j,k)    / real(dt_ICE2)
          uqsIB(i,j,k)    = uqsIB(i,j,k)    / real(dt_ICE2)
          ussIB(i,j,k)    = ussIB(i,j,k)    / real(dt_ICE2)
          if (mw .eq. 5) then
          tt_intIB(i,j,k) = tt_intIB(i,j,k) / real(dt_ICE2)     !*CL*
          qq_intIB(i,j,k) = qq_intIB(i,j,k) / real(dt_ICE2)     !*CL*
          endif
          DO kk=1,llx
          sltIB(i,j,k,kk) = sltIB (i,j,k,kk)/ real(dt_ICE2)
          slqIB(i,j,k,kk) = slqIB (i,j,k,kk)/ real(dt_ICE2)
          ENDDO
          DO kk =1,mi
          agIB (i,j,k,kk) = agIB (i,j,k,kk) / real(dt_ICE2)
          g1IB (i,j,k,kk) = g1IB (i,j,k,kk) / real(dt_ICE2)
          g2IB (i,j,k,kk) = g2IB (i,j,k,kk) / real(dt_ICE2)
          roIB (i,j,k,kk) = roIB (i,j,k,kk) / real(dt_ICE2)
          tiIB (i,j,k,kk) = tiIB (i,j,k,kk) / real(dt_ICE2)-TfSnow
          waIB (i,j,k,kk) = waIB (i,j,k,kk) / real(dt_ICE2)
          END DO
        END DO
 
        END DO
        END DO
 
        dt_ICE2= -1
 
 
C +  3.2 Save in a netcdf file
C +  =========================
 
        dt_ICE = dt_ICE + 1
 
        IF (iterun.gt.1)                                          THEN !
 
C +       ************
          CALL UNwopen (fnamNC_tmp,ID__nc_ice)
C +       ************
 
        END IF ! Re-Open file IF already created.
 
        write(6,398) trim(fnamNC_tmp),
     .  jdarGE,mmarGE,iyrrGE,jhurGE,minuGE,jsecGE
 
  398   format (' Writing of OUTice in ',a19, ':'
     .          ,i2,'/',i2,'/',i4,' ',i2,':',i2,':',i2)
 
 
C +--3.2.1 Write Time-dependent variables
C +  ------------------------------------
 
 
        IF (nDFdim(0).eq.0)                  THEN  !
 
c       date      = (351+(iyrrGE  -1902) *365  ! Nb Days before iyrrGE
c    .            +(iyrrGE  -1901) /  4        ! Nb Leap Years
c    .            + njyrGE(mmarGE)             ! Nb Days before mmarGE
c    .            + njybGE(mmarGE)             ! (including Leap Day)
c    .            * max(0,1-mod(iyrrGE,4))     !
c    .            + jdarGE -1 )*  24           !
c    .            + jhurGE                     !
c    .            +(minuGE *60 +jsecGE)/3600   !
 
                        date = nint(real(itexpe)*dt/60.)
        IF(OutdyIB<=24) date = nint(real(itexpe)*dt/(3600.))
        IF(OutdyIB==1)  date = nint(real(itexpe)*dt/(3600.*24.))
 
                        dater= real(date0+date)/2.
                        date0= date
 
C +     ************
        CALL UNwrite(ID__nc_ice,'time', dt_ICE,  1,  1,  1, dater)
C +     ************
 
        END IF
 
C +     ************
        CALL UNwrite(ID__nc_ice,'DATE', dt_ICE,  1,  1,  1,
     .               dateNC_ice(dt_ICE))
        CALL UNwrite(ID__nc_ice,'year', dt_ICE,  1,  1,  1,
     .               yearNC_ice(dt_ICE))
C +     ************
 
        if(iyrrIB==0.or.OutdyIB>1) then
         iyrrIB=iyrrGE
         mmarIB=mmarGE
         jdarIB=jdarGE
         jhurIB=jhurGE
         minuIB=minuGE
        endif
 
C +     ************
        dater=iyrrIB
        CALL UNwrite (ID__nc_ice, 'YYYY',dt_ICE, 1, 1, 1,dater)
        dater=mmarIB
        CALL UNwrite (ID__nc_ice, 'MM'  ,dt_ICE, 1, 1, 1,dater)
        dater=jdarIB
        CALL UNwrite (ID__nc_ice, 'DD'  ,dt_ICE, 1, 1, 1,dater)
        dater=jhurIB
        CALL UNwrite (ID__nc_ice, 'HH'  ,dt_ICE, 1, 1, 1,dater)
        dater=minuIB
        CALL UNwrite (ID__nc_ice, 'MIN' ,dt_ICE, 1, 1, 1,dater)
C +     ************
 
C +--3.2.0.a X-hourly Variables
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~
 
        if(OutdyIB==1) then
C +     ************
        CALL UNwrite (ID__nc_ice,'TIMEh',dt_ICE,mlhh, 1, 1,timehIB)
        CALL UNwrite (ID__nc_ice, 'SPh', dt_ICE,mx, my, mlhh,sphIB)
        CALL UNwrite (ID__nc_ice, 'STh', dt_ICE,mx, my, mlhh,sthIB)
        CALL UNwrite (ID__nc_ice, 'TTh', dt_ICE,mx, my, mlhh,tthIB)
        CALL UNwrite (ID__nc_ice, 'TXh', dt_ICE,mx, my, mlhh,txhIB)
        CALL UNwrite (ID__nc_ice, 'TNh', dt_ICE,mx, my, mlhh,tnhIB)
        CALL UNwrite (ID__nc_ice, 'QQh', dt_ICE,mx, my, mlhh,qqhIB)
        CALL UNwrite (ID__nc_ice, 'UUh', dt_ICE,mx, my, mlhh,uuhIB)
        CALL UNwrite (ID__nc_ice, 'VVh', dt_ICE,mx, my, mlhh,vvhIB)
        CALL UNwrite (ID__nc_ice,'SWDh', dt_ICE,mx, my, mlhh,swdhIB)
        CALL UNwrite (ID__nc_ice,'LWDh', dt_ICE,mx, my, mlhh,lwdhIB)
        CALL UNwrite (ID__nc_ice,'LWUh', dt_ICE,mx, my, mlhh,lwuhIB)
        CALL UNwrite (ID__nc_ice,'SHFh', dt_ICE,mx, my, mlhh,shfhIB)
        CALL UNwrite (ID__nc_ice,'LHFh', dt_ICE,mx, my, mlhh,lhfhIB)
        CALL UNwrite (ID__nc_ice, 'ALh', dt_ICE,mx, my, mlhh,alhIB)
        CALL UNwrite (ID__nc_ice, 'CCh', dt_ICE,mx, my, mlhh,clhIB)
        CALL UNwrite (ID__nc_ice,'LWC1mh',dt_ICE,mx,my,mlhh,lwc1mhIB)
        CALL UNwrite (ID__nc_ice,'LWC2mh',dt_ICE,mx,my,mlhh,lwc2mhIB)
        CALL UNwrite (ID__nc_ice,'ICERHO',dt_ICE,mx,my,mlhh,irhoIB) !*AJT*
 
c       CALL UNwrite (ID__nc_ice,'TT50mh',dt_ICE,mx, my, mlhh,t5hIB)
c       CALL UNwrite (ID__nc_ice,'QQ50mh',dt_ICE,mx, my, mlhh,q5hIB)
c       CALL UNwrite (ID__nc_ice,'UU50mh',dt_ICE,mx, my, mlhh,u5hIB)
c       CALL UNwrite (ID__nc_ice,'VV50mh',dt_ICE,mx, my, mlhh,v5hIB)
c       CALL UNwrite (ID__nc_ice,'PP50mh',dt_ICE,mx, my, mlhh,p5hIB)
c
c	CALL UNwrite (ID__nc_ice,  'TDh', dt_ICE, mx, my, mlhh,tdh)
c	CALL UNwrite (ID__nc_ice, 'SLTh', dt_ICE, mx, my, mlhh,slth)
c	CALL UNwrite (ID__nc_ice,'SLQCh', dt_ICE, mx, my, mlhh,slqch)
c	CALL UNwrite (ID__nc_ice, 'WVPh', dt_ICE, mx, my, mlhh,wvph)
c	CALL UNwrite (ID__nc_ice,  'CPh', dt_ICE, mx, my, mlhh,cphIB)
c	CALL UNwrite (ID__nc_ice, 'CAPEh',dt_ICE, mx, my, mlhh,capeh)
c	CALL UNwrite (ID__nc_ice, 'WMTh', dt_ICE, mx, my, mlhh,tmh)
c	CALL UNwrite (ID__nc_ice, 'ZTDh', dt_ICE, mx, my, mlhh,ztdh)
c	CALL UNwrite (ID__nc_ice, 'ZHDh', dt_ICE, mx, my, mlhh,zhdh)
c	CALL UNwrite (ID__nc_ice, 'ZWDh', dt_ICE, mx, my, mlhh,zwdh)
        endif
 
c +     ************
 
C +--3.2.1.a Atmospheric Variables
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 
        ! daily mean
 
C +     ************
        CALL UNwrite (ID__nc_ice, 'TTmin', dt_ICE, mx, my, ml,mintIB)
        CALL UNwrite (ID__nc_ice, 'TTmax', dt_ICE, mx, my, ml,maxtIB)
        CALL UNwrite (ID__nc_ice, 'TTint', dt_ICE, mx, my, nsx,tt_intIB)
        CALL UNwrite (ID__nc_ice, 'TT'   , dt_ICE, mx, my, ml,  ttIB)
        CALL UNwrite (ID__nc_ice, 'TD'   , dt_ICE, mx, my, ml,  tdIB)
        CALL UNwrite (ID__nc_ice, 'UU'   , dt_ICE, mx, my, ml,  uuIB)
        CALL UNwrite (ID__nc_ice, 'VV'   , dt_ICE, mx, my, ml,  vvIB)
        CALL UNwrite (ID__nc_ice, 'UV'   , dt_ICE, mx, my, ml,  uvIB)
        CALL UNwrite (ID__nc_ice, 'UVmax', dt_ICE, mx, my, ml,maxwIB)
        CALL UNwrite (ID__nc_ice, 'WW'   , dt_ICE, mx, my, ml,  wwIB)
        CALL UNwrite (ID__nc_ice, 'XUU'  , dt_ICE, mx, my, ml, ruuIB)
        CALL UNwrite (ID__nc_ice, 'YVV'  , dt_ICE, mx, my, ml, rvvIB)
        CALL UNwrite (ID__nc_ice, 'XYUV' , dt_ICE, mx, my, ml,  ruvIB)
        CALL UNwrite (ID__nc_ice, 'QQ'   , dt_ICE, mx, my, ml,  qqIB)
        CALL UNwrite (ID__nc_ice, 'QQint', dt_ICE, mx, my, nsx,qq_intIB)
        CALL UNwrite (ID__nc_ice, 'RHO'   ,dt_ICE, mx, my, ml,rolvIB)
        CALL UNwrite (ID__nc_ice, 'RH'   , dt_ICE, mx, my, ml,  rhIB)
        CALL UNwrite (ID__nc_ice, 'ZZ'   , dt_ICE, mx, my, ml,  zzIB)
        CALL UNwrite (ID__nc_ice, 'PDD'  , dt_ICE, mx, my,  1, pddIB)
        CALL UNwrite (ID__nc_ice, 'SP'   , dt_ICE, mx, my,  1,  spIB)
        CALL UNwrite (ID__nc_ice, 'SLP'  , dt_ICE, mx, my,  1, slpIB)
        CALL UNwrite (ID__nc_ice, 'gradT'  , dt_ICE,mx, my,1,gradTIB)
        CALL UNwrite (ID__nc_ice, 'gradTmin',dt_ICE,mx, my,1,mingrTIB)
        CALL UNwrite (ID__nc_ice, 'gradTmax',dt_ICE,mx, my,1,maxgrTIB)
        CALL UNwrite (ID__nc_ice, 'gradQ'   ,dt_ICE,mx, my,1,gradQIB)
        CALL UNwrite (ID__nc_ice, 'gradQmin',dt_ICE,mx, my,1,mingrQIB)
        CALL UNwrite (ID__nc_ice, 'gradQmax',dt_ICE,mx, my,1,maxgrQIB)
        CALL UNwrite (ID__nc_ice, 'TKE'   , dt_ICE, mx, my, ml, tkeIB)
        CALL UNwrite (ID__nc_ice, 'LQS'   , dt_ICE, mx, my, ml, lqsIB)
        CALL UNwrite (ID__nc_ice, 'LQI'   , dt_ICE, mx, my, ml, lqiIB)
        CALL UNwrite (ID__nc_ice, 'LQR'   , dt_ICE, mx, my, ml, lqrIB)
        CALL UNwrite (ID__nc_ice, 'LQW'   , dt_ICE, mx, my, ml, lqwIB)
        CALL UNwrite (ID__nc_ice, 'SWN3D' , dt_ICE, mx, my, ml, swn3DIB)
        CALL UNwrite (ID__nc_ice, 'LWN3D' , dt_ICE, mx, my, ml, lwn3DIB)
        CALL UNwrite (ID__nc_ice, 'SWNC3D',dt_ICE, mx, my, ml, swnc3DIB)
        CALL UNwrite (ID__nc_ice, 'LWNC3D',dt_ICE, mx, my, ml, lwnc3DIB)
        CALL UNwrite (ID__nc_ice, 'COD3D' , dt_ICE, mx, my, ml, cod3DIB)
        CALL UNwrite (ID__nc_ice, 'CC3D'  , dt_ICE, mx, my, ml, cc3DIB)
 
 
        ttbIB = ttIB(:,:,1:mlb)
        txbIB = maxtIB(:,:,1:mlb)
        tnbIB = mintIB(:,:,1:mlb)
        qqbIB = qqIB(:,:,1:mlb)
        uubIB = uuIB(:,:,1:mlb)
        vvbIB = vvIB(:,:,1:mlb)
        uvbIB = uvIB(:,:,1:mlb)
        zzbIB = zzIB(:,:,1:mlb)
 
        CALL UNwrite (ID__nc_ice, 'TTb'   , dt_ICE, mx, my,mlb, ttbIB)
        CALL UNwrite (ID__nc_ice, 'TXb'   , dt_ICE, mx, my,mlb, txbIB)
        CALL UNwrite (ID__nc_ice, 'TNb'   , dt_ICE, mx, my,mlb, tnbIB)
        CALL UNwrite (ID__nc_ice, 'QQb'   , dt_ICE, mx, my,mlb, qqbIB)
        CALL UNwrite (ID__nc_ice, 'ZZb'   , dt_ICE, mx, my,mlb, zzbIB)
        CALL UNwrite (ID__nc_ice, 'UUb'   , dt_ICE, mx, my,mlb, uubIB)
        CALL UNwrite (ID__nc_ice, 'VVb'   , dt_ICE, mx, my,mlb, vvbIB)
        CALL UNwrite (ID__nc_ice, 'UVb'   , dt_ICE, mx, my,mlb, uvbIB)
 
        CALL UNwrite (ID__nc_ice, 'TTp'   , dt_ICE, mx, my, mp, ttpIB)
        CALL UNwrite (ID__nc_ice, 'QQp'   , dt_ICE, mx, my, mp, qqpIB)
        CALL UNwrite (ID__nc_ice, 'ZZp'   , dt_ICE, mx, my, mp, zzpIB)
        CALL UNwrite (ID__nc_ice, 'UUp'   , dt_ICE, mx, my, mp, uupIB)
        CALL UNwrite (ID__nc_ice, 'VVp'   , dt_ICE, mx, my, mp, vvpIB)
        CALL UNwrite (ID__nc_ice, 'WWp'   , dt_ICE, mx, my, mp, wwpIB)
        CALL UNwrite (ID__nc_ice, 'UVp'   , dt_ICE, mx, my, mp, uvpIB)
 
        CALL UNwrite (ID__nc_ice, 'TTz'   , dt_ICE, mx, my, mztq, ttzIB)
        CALL UNwrite (ID__nc_ice, 'QQz'   , dt_ICE, mx, my, mztq, qqzIB)
        CALL UNwrite (ID__nc_ice, 'UUz'   , dt_ICE, mx, my, mzuv, uuzIB)
        CALL UNwrite (ID__nc_ice, 'VVz'   , dt_ICE, mx, my, mzuv, vvzIB)
        CALL UNwrite (ID__nc_ice, 'U2z'   , dt_ICE, mx, my, mzuv, u2zIB)
        CALL UNwrite (ID__nc_ice, 'V2z'   , dt_ICE, mx, my, mzuv, v2zIB)
        CALL UNwrite (ID__nc_ice, 'UVz'   , dt_ICE, mx, my, mzuv, uvzIB)
        CALL UNwrite (ID__nc_ice, 'ROz'   , dt_ICE, mx, my, mzuv, rozIB)
C +     ************
 
C +--3.2.1.b Surface Variables
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~
 
        ! daily mean
 
C +     ************
        CALL UNwrite (ID__nc_ice, 'SWD'  , dt_ICE, mx, my,  1, swdIB)
        CALL UNwrite (ID__nc_ice, 'SWU'  , dt_ICE, mx, my,  1, swuIB)
        CALL UNwrite (ID__nc_ice, 'SUN'  , dt_ICE, mx, my,  1, sunIB)
        CALL UNwrite (ID__nc_ice, 'LWD'  , dt_ICE, mx, my,  1, lwdIB)
        CALL UNwrite (ID__nc_ice, 'LWU'  , dt_ICE, mx, my,  1, lwuIB)
 
        CALL UNwrite (ID__nc_ice, 'SWDT' , dt_ICE, mx, my,  1, swdtIB)
        CALL UNwrite (ID__nc_ice, 'SWUT' , dt_ICE, mx, my,  1, swutIB)
        CALL UNwrite (ID__nc_ice, 'LWUT' , dt_ICE, mx, my,  1, lwutIB)
 
        CALL UNwrite (ID__nc_ice, 'SHF'  , dt_ICE, mx, my,  1, shfIB)
        CALL UNwrite (ID__nc_ice, 'LHF'  , dt_ICE, mx, my,  1, lhfIB)
        CALL UNwrite (ID__nc_ice,  'AL'  , dt_ICE, mx, my,  1,  alIB)
        CALL UNwrite (ID__nc_ice, 'AS1'  , dt_ICE, mx, my,  1,aS1_IB)
        CALL UNwrite (ID__nc_ice, 'AS2'  , dt_ICE, mx, my,  1,aS2_IB)
        CALL UNwrite (ID__nc_ice, 'AS3'  , dt_ICE, mx, my,  1,aS3_IB)
        CALL UNwrite (ID__nc_ice, 'AL1'  , dt_ICE, mx, my,nsx, al1IB)
        CALL UNwrite (ID__nc_ice, 'AL2'  , dt_ICE, mx, my,nsx, al2IB)
        CALL UNwrite (ID__nc_ice,'FRV2'  , dt_ICE, mx, my,nsx, frvIB)
        CALL UNwrite (ID__nc_ice, 'SIC'  , dt_ICE, mx, my,  1, sicIB)
        CALL UNwrite (ID__nc_ice, 'FRA'  , dt_ICE, mx, my,nsx, frvIB)
        CALL UNwrite (ID__nc_ice, 'ST'   , dt_ICE, mx, my,  1,  stIB)
        CALL UNwrite (ID__nc_ice, 'ST2'  , dt_ICE, mx, my,nsx, st2IB)
        CALL UNwrite (ID__nc_ice, 'Z0'   , dt_ICE, mx, my,nsx,  Z0IB)
        CALL UNwrite (ID__nc_ice, 'R0'   , dt_ICE, mx, my,nsx,  R0IB)
        CALL UNwrite (ID__nc_ice, 'UUS'  , dt_ICE, mx, my,nsx, UUSIB)
        CALL UNwrite (ID__nc_ice, 'UUSTH', dt_ICE, mx, my,nsx, UUSTHIB)
        CALL UNwrite (ID__nc_ice, 'UTS'  , dt_ICE, mx, my,nsx, UTSIB)
        CALL UNwrite (ID__nc_ice, 'UQS'  , dt_ICE, mx, my,nsx, UQSIB)
        CALL UNwrite (ID__nc_ice, 'USS'  , dt_ICE, mx, my,nsx, USSIB)
        CALL UNwrite (ID__nc_ice, 'PBL'  , dt_ICE, mx, my,nsx, pblIB)
C +     ++++++++++++
 
        CALL UNwrite (ID__nc_ice, 'SICAO', dt_ICE, mx, my,1, sicaoIB)
        CALL UNwrite (ID__nc_ice, 'SITAO', dt_ICE, mx, my,1, sitaoIB)
        CALL UNwrite (ID__nc_ice, 'SNTAO', dt_ICE, mx, my,1, sntaoIB)
        CALL UNwrite (ID__nc_ice, 'ST2AO', dt_ICE, mx, my,nsx, st2aoIB)
        CALL UNwrite (ID__nc_ice, 'AL2AO', dt_ICE, mx, my,nsx, albaoIB)

C +--3.2.1.c Cloud Variables
C +  ~~~~~~~~~~~~~~~~~~~~~~~
 
        ! daily mean
 
C +     ++++++++++++
        CALL UNwrite (ID__nc_ice, 'CC'   , dt_ICE, mx, my,  1,  ccIB)
        CALL UNwrite (ID__nc_ice, 'CU'   , dt_ICE, mx, my,  1,  cuIB)
        CALL UNwrite (ID__nc_ice, 'CM'   , dt_ICE, mx, my,  1,  cmIB)
        CALL UNwrite (ID__nc_ice, 'CD'   , dt_ICE, mx, my,  1,  cdIB)
        CALL UNwrite (ID__nc_ice, 'QW'   , dt_ICE, mx, my,  1,  qwIB)
        CALL UNwrite (ID__nc_ice, 'QI'   , dt_ICE, mx, my,  1,  qiIB)
        CALL UNwrite (ID__nc_ice, 'QS'   , dt_ICE, mx, my,  1,  qsIB)
        CALL UNwrite (ID__nc_ice, 'QR'   , dt_ICE, mx, my,  1,  qrIB)
        CALL UNwrite (ID__nc_ice, 'COD'  , dt_ICE, mx, my,  1, codIB)
        CALL UNwrite (ID__nc_ice, 'WVP'  , dt_ICE, mx, my,  1, wvpIB)
        CALL UNwrite (ID__nc_ice, 'CWP'  , dt_ICE, mx, my,  1, cwpIB)
        CALL UNwrite (ID__nc_ice, 'IWP'  , dt_ICE, mx, my,  1, iwpIB)
        CALL UNwrite (ID__nc_ice, 'QBR'  , dt_ICE, mx, my,  1, qbrIB)
C +     ++++++++++++
 
C +--3.2.1.d Snow Pack Variables
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~
 
        DO k =1,nsx
 
        write(sector,'(i1)') k
 
        DO kk=1,mi
        DO j =1,my
        DO i =1,mx
           xymi1(i,j,kk) = g1IB(i,j,k,kk)
           xymi2(i,j,kk) = g2IB(i,j,k,kk)
           xymi3(i,j,kk) = agIB(i,j,k,kk)
           xymi5(i,j,kk) = roIB(i,j,k,kk)
           xymi6(i,j,kk) = tiIB(i,j,k,kk)
           xymi7(i,j,kk) = waIB(i,j,k,kk)
        ENDDO
        ENDDO
        ENDDO
 
        ! daily mean
 
C +     ++++++++++++
        CALL UNwrite (ID__nc_ice, 'AG'//sector,
     .                dt_ICE, mx, my, mi, xymi3)
        CALL UNwrite (ID__nc_ice, 'G1'//sector,
     .                dt_ICE, mx, my, mi, xymi1)
        CALL UNwrite (ID__nc_ice, 'G2'//sector,
     .                dt_ICE, mx, my, mi, xymi2)
        CALL UNwrite (ID__nc_ice, 'RO'//sector,
     .                dt_ICE, mx, my, mi, xymi5)
        CALL UNwrite (ID__nc_ice, 'TI'//sector,
     .                dt_ICE, mx, my, mi, xymi6)
        CALL UNwrite (ID__nc_ice, 'WA'//sector,
     .                dt_ICE, mx, my, mi, xymi7)
C +     ************
 
        END DO
 
        DO k=1,nsx
        DO j=1,my
        DO i=1,mx
            xynsx1(i,j,k)   = real(nssSNo(i,j,k))
            xynsx2(i,j,k)   = real(nisSNo(i,j,k))
            xynsx3(i,j,k)   = zn3IB(i,j,k)- zn0IB(i,j,k)
     .                       -zn6IB(i,j,k)
             zn6IB(i,j,k)   = zn3IB(i,j,k)- zn0IB(i,j,k)
        END DO
        END DO
        END DO
 
        ! Snapshot
 
C +     ************
        CALL UNwrite (ID__nc_ice, 'nSSN',
     .                dt_ICE, mx, my, nsx, xynsx1 )
        CALL UNwrite (ID__nc_ice, 'nISN',
     .                dt_ICE, mx, my, nsx, xynsx2 )
        CALL UNwrite (ID__nc_ice, 'SWSN' ,
     .                dt_ICE, mx, my, nsx, SWaSNo)
        CALL UNwrite (ID__nc_ice, 'ALSN',
     .                dt_ICE, mx, my,  1,  albeSL)
        CALL UNwrite (ID__nc_ice, 'MB',
     .                dt_ICE, mx, my, nsx, mbIB  )
        CALL UNwrite (ID__nc_ice, 'ZN',
     .                dt_ICE, mx, my, nsx, zn1IB )
        CALL UNwrite (ID__nc_ice, 'ZN1',
     .                dt_ICE, mx, my, nsx, zn1IB )
        CALL UNwrite (ID__nc_ice, 'ZN2',
     .                dt_ICE, mx, my, nsx, zn2IB )
        CALL UNwrite (ID__nc_ice, 'ZN3',
     .                dt_ICE, mx, my, nsx, zn3IB )
        CALL UNwrite (ID__nc_ice, 'ZN4',
     .                dt_ICE, mx, my, nsx, zn4IB )
        CALL UNwrite (ID__nc_ice, 'ZN5',
     .                dt_ICE, mx, my, nsx, zn5IB )
        CALL UNwrite (ID__nc_ice, 'ZN6',
     .                dt_ICE, mx, my, nsx, xynsx3)
        CALL UNwrite (ID__nc_ice,'ZN0',dt_ICE, mx, my, nsx, zn0IB)
        CALL UNwrite (ID__nc_ice,'MB0',dt_ICE, mx, my, nsx, mb0IB)
        CALL UNwrite (ID__nc_ice,'biomas',dt_ICE, mx, my, 1, biomass) !*AJT* biomass loading snapshot
C +     ************
 
        DO k =1,nsx
 
        write(sector,'(i1)') k
 
        DO kk=1,nsno
        DO j=1,my
        DO i=1,mx
          xynsno1(i,j,kk) =      agsSNo(i,j,k,kk)
          xynsno2(i,j,kk) =      dzsSNo(i,j,k,kk)
          xynsno3(i,j,kk) = real(nhsSNo(i,j,k,kk))
          xynsno4(i,j,kk) =      g1sSNo(i,j,k,kk)
          xynsno5(i,j,kk) =      g2sSNo(i,j,k,kk)
          xynsno6(i,j,kk) =      rosSNo(i,j,k,kk)
          xynsno7(i,j,kk) =      tisSNo(i,j,k,kk) - TfSnow
          xynsno8(i,j,kk) =      wasSNo(i,j,k,kk)
        END DO
        END DO
        END DO
 
        ! Snapshot
 
C +     ************
        CALL UNwrite (ID__nc_ice, 'agSN'//sector,
     .                dt_ICE, mx, my, nsno, xynsno1)
        CALL UNwrite (ID__nc_ice, 'dzSN'//sector,
     .                dt_ICE, mx, my, nsno, xynsno2)
        CALL UNwrite (ID__nc_ice, 'nhSN'//sector,
     .                dt_ICE, mx, my, nsno, xynsno3)
        CALL UNwrite (ID__nc_ice, 'g1SN'//sector,
     .                dt_ICE, mx, my, nsno, xynsno4)
        CALL UNwrite (ID__nc_ice, 'g2SN'//sector,
     .                dt_ICE, mx, my, nsno, xynsno5)
        CALL UNwrite (ID__nc_ice, 'roSN'//sector,
     .                dt_ICE, mx, my, nsno, xynsno6)
        CALL UNwrite (ID__nc_ice, 'tiSN'//sector,
     .                dt_ICE, mx, my, nsno, xynsno7)
        CALL UNwrite (ID__nc_ice, 'waSN'//sector,
     .                dt_ICE, mx, my, nsno, xynsno8)
C +     ************
 
        END DO
 
C +--3.2.1.e Soil Variables
C +  ~~~~~~~~~~~~~~~~~~~~~~
 
        DO j=1,my
        DO i=1,mx
        DO k=1,nsx
           slqcIB(i,j,k)    = 0
          DO kk=-nsol,0
           slqcIB(i,j,k)    = Eta_TV(i,j,k,1-kk)* dzAvSV(kk)
     .                      * ro_Wat         + slqcIB(i,j,k)
          ENDDO
        ENDDO
        END DO
        END DO
 
        CALL UNwrite (ID__nc_ice, 'SLQC',dt_ICE, mx, my, nvx, slqcIB)
 
        DO k =1,nvx
 
        write(sector,'(i1)') k
 
        DO kk=1,llx
        DO j=1,my
        DO i=1,mx
          xyllx1(i,j,kk)=TsolTV(i,j,k,kk) - TfSnow
          xyllx2(i,j,kk)=Eta_TV(i,j,k,kk)
          xyllx3(i,j,kk)=sltIB (i,j,k,kk)
          xyllx4(i,j,kk)=slqIB (i,j,k,kk)
        END DO
        END DO
        END DO
 
        ! Daily mean + snapshot
 
C +     ************
        CALL UNwrite (ID__nc_ice, 'SLTSN'//sector,
     .                dt_ICE, mx, my, llx, xyllx1)
        CALL UNwrite (ID__nc_ice, 'SLQSN'//sector,
     .                dt_ICE, mx, my, llx, xyllx2)
        CALL UNwrite (ID__nc_ice, 'SLT'//sector,
     .                dt_ICE, mx, my, llx, xyllx3)
        CALL UNwrite (ID__nc_ice, 'SLQ'//sector,
     .                dt_ICE, mx, my, llx, xyllx4)
C +     ************
 
        END DO
 
C +--3.2.1.f Mass Balance Variables
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 
        DO j=1,my
        DO i=1,mx
 
          WKxy1(i,j)     =  snowHY(i,j)   * 1000.
          WKxy2(i,j)     =  rainHY(i,j)   * 1000.
 
        END DO
        END DO
 
        ! Snapshot
 
C +     ************
c        CALL UNwrite (ID__nc_ice, 'SImass', dt_ICE, mx, my, nsx,SIm_IB)
c        CALL UNwrite (ID__nc_ice, 'SImadd', dt_ICE, mx, my, nsx,wei0IB)
c        CALL UNwrite (ID__nc_ice, 'S_mass', dt_ICE, mx, my, nsx,S_m_IB)
c        CALL UNwrite (ID__nc_ice, 'SI_hhh', dt_ICE, mx, my, nsx,SIh_IB)
c        CALL UNwrite (ID__nc_ice, 'S__hhh', dt_ICE, mx, my, nsx,S_h_IB)
c        CALL UNwrite (ID__nc_ice, 'SS_hhh', dt_ICE, mx, my, nsx,SSh_IB)
c        CALL UNwrite (ID__nc_ice, 'MBsubl', dt_ICE, mx, my, nsx,wes_IB)
c        CALL UNwrite (ID__nc_ice, 'MBmelt', dt_ICE, mx, my, nsx,wem_IB)
c        CALL UNwrite (ID__nc_ice, 'MBrefr', dt_ICE, mx, my, nsx,wer_IB)
c        CALL UNwrite (ID__nc_ice, 'MBruno', dt_ICE, mx, my, nsx,weu_IB)
c        CALL UNwrite (ID__nc_ice, 'MBsrfW', dt_ICE, mx, my, nsx,SWaSNo)
c        CALL UNwrite (ID__nc_ice, 'MBevap', dt_ICE, mx, my, nsx,wee_IB)
c        CALL UNwrite (ID__nc_ice, 'MBsnow', dt_ICE, mx, my,   1, WKxy1)
c        CALL UNwrite (ID__nc_ice, 'MBrain', dt_ICE, mx, my,   1, WKxy2)
c        CALL UNwrite (ID__nc_ice, 'MBroff', dt_ICE, mx, my,   1,runoTV)
C +     ************
 
c +     factim = (86400.0/real(OutdyIB)) / abs((itrdIB-iterun) *dt )
        factim =      1.0
C +...  factim :  Conversion Factor (x /elapsed time --> x /Days)


        DO j=1,my
        DO i=1,mx
        DO k=1,nsx

          xynsx1(i,j,k)  = (CaWaTV(i,j,k) - wec0IB(i,j,k)) * factim
          wec0IB(i,j,k)  =  CaWaTV(i,j,k) ! Canopy water content
 
          xynsx2(i,j,k)  = (weacIB(i,j,k) - weac0IB(i,j,k)) * factim
          weac0IB(i,j,k) =  weacIB(i,j,k) ! BS acc
 
          xynsx3(i,j,k)  = (weerIB(i,j,k) - weer0IB(i,j,k)) * factim
          weer0IB(i,j,k) =  weerIB(i,j,k) ! BS erosion

          xynsx4(i,j,k)  = (slqcIB(i,j,k) - wel0IB(i,j,k)) * factim
          wel0IB(i,j,k)  =  slqcIB(i,j,k) ! Soil water content

          xynsx5(i,j,k)  = (wem_IB(i,j,k) - wem0IB(i,j,k)) * factim
     .                   + (wer_IB(i,j,k) - wer0IB(i,j,k)) * factim
          xynsx5(i,j,k)  = (-1.)*xynsx6(i,j,k)
                                          ! Refreezing - Melting
          xynsx6(i,j,k)  = (wer_IB(i,j,k) - wer0IB(i,j,k)) * factim
          wer0IB(i,j,k)  =  wer_IB(i,j,k) ! Refreezing
        END DO
        END DO
        END DO

        CALL UNwrite (ID__nc_ice, 'MBcc', dt_ICE, mx, my, nsx,xynsx1)
        CALL UNwrite (ID__nc_ice, 'MBac', dt_ICE, mx, my, nsx,xynsx2)
        CALL UNwrite (ID__nc_ice, 'MBer', dt_ICE, mx, my, nsx,xynsx3)
        CALL UNwrite (ID__nc_ice, 'MBsc', dt_ICE, mx, my, nsx,xynsx4)
        CALL UNwrite (ID__nc_ice, 'MBmt', dt_ICE, mx, my, nsx,xynsx5)
        CALL UNwrite (ID__nc_ice,  'MBr', dt_ICE, mx, my, nsx,xynsx6)


        DO j=1,my
        DO i=1,mx
        DO k=1,nsx
          xynsx1(i,j,k)  = (wet_IB(i,j,k) - wet0IB(i,j,k)) * factim
          wet0IB(i,j,k)  =  wet_IB(i,j,k)   ! Total
 
          xynsx2(i,j,k)  = (wem_IB(i,j,k) - wem0IB(i,j,k)) * factim
          xynsx2(i,j,k)  = (-1.)*xynsx2(i,j,k)
          wem0IB(i,j,k)  =  wem_IB(i,j,k)   ! Only Melting
 
          xynsx3(i,j,k)  = (weu_IB(i,j,k) - weu0IB(i,j,k)) * factim
          weu0IB(i,j,k)  =  weu_IB(i,j,k)   ! Run-off

          xynsx4(i,j,k)  = (weo_IB(i,j,k,2) - weo0IB(i,j,k,2)) * factim
          weo0IB(i,j,k,2)=  weo_IB(i,j,k,2) ! Run-off from snow

          xynsx5(i,j,k)  = (wee_IB(i,j,k,3) - wee0IB(i,j,k,3)) * factim
          wee0IB(i,j,k,3)=  wee_IB(i,j,k,3) ! Subli from snow

          xynsx6(i,j,k)  = (wee_IB(i,j,k,1) - wee0IB(i,j,k,1)) * factim
          wee0IB(i,j,k,1)=  wee_IB(i,j,k,1) ! Evaporation

          xynsx7(i,j,k)  = (wee_IB(i,j,k,2) - wee0IB(i,j,k,2)) * factim
          wee0IB(i,j,k,2)=  wee_IB(i,j,k,2) ! Evapotranspiratuon

          xynsx8(i,j,k)  = (wee_IB(i,j,k,4) - wee0IB(i,j,k,4)) * factim
          wee0IB(i,j,k,4)=  wee_IB(i,j,k,4) ! Subli from soil

          xynsx9(i,j,k)  = (SWaSNo(i,j,k) - wesw0IB(i,j,k))* factim
          wesw0IB(i,j,k) =  SWaSNo(i,j,k)  ! Superficial Water

        END DO
 
 
          WKxy2(i,j)     = 0.
        DO k=1,nsx
          WKxy2(i,j)     =   WKxy2(i,j)
     .                   +  SLsrfl(i,j,k) * snohSN(i,j,k) /1000.
        END DO
 
          DO kk=1,ml
            snf_IB(i,j,kk) = (snfHY(i,j,mz-kk+1)-snf0IB(i,j,kk))*factim
            snf_IB(i,j,kk) = snf_IB(i,j,kk)*1000.
            snf0IB(i,j,kk) = snfHY(i,j,mz-kk+1)
            sbl_IB(i,j,kk) = (sblHY(i,j,mz-kk+1)-sbl0IB(i,j,kk))*factim
            sbl_IB(i,j,kk) = sbl_IB(i,j,kk)*1000.
            sbl0IB(i,j,kk) = sblHY(i,j,mz-kk+1)
            sbl_IB(i,j,kk) = (qssblHY(i,j,mz-kk+1)-qssbl0IB(i,j,kk))
     .                     * factim
          qssbl_IB(i,j,kk) = qssbl_IB(i,j,kk)*1000.
          qssbl0IB(i,j,kk) = qssblHY(i,j,mz-kk+1)
            dep_IB(i,j,kk) = (depHY(i,j,mz-kk+1)-dep0IB(i,j,kk))*factim
            dep_IB(i,j,kk) = dep_IB(i,j,kk)*1000.
            dep0IB(i,j,kk) = depHY(i,j,mz-kk+1)
            rnf_IB(i,j,kk) = (rnfHY(i,j,mz-kk+1)-rnf0IB(i,j,kk))*factim
            rnf_IB(i,j,kk) = rnf_IB(i,j,kk)*1000.
            rnf0IB(i,j,kk) = rnfHY(i,j,mz-kk+1)
            evp_IB(i,j,kk) = (evpHY(i,j,mz-kk+1)-evp0IB(i,j,kk))*factim
            evp_IB(i,j,kk) = evp_IB(i,j,kk)*1000.
            evp0IB(i,j,kk) = evpHY(i,j,mz-kk+1)
            smt_IB(i,j,kk) = (smtHY(i,j,mz-kk+1)-smt0IB(i,j,kk))*factim
            smt0ib(i,j,kk) =  smtHY(i,j,mz-kk+1)
          END DO
 
 
          WKxy2(i,j)     =  0. ! snowfall does not taken into account
                               ! the snow in the SISVAT bluffer
 
          WKxy1(i,j)     =  (snowHY(i,j)  - wesf0IB(i,j)
     .                                    -   WKxy2(i,j)) * factim
          WKxy1(i,j)     =  max(0.,WKxy1(i,j) * 1000.)
          wesf0IB(i,j)   =  snowHY(i,j)   -   WKxy2(i,j)  ! Snowfall
 
          WKxy2(i,j)     = (rainHY(i,j)   - werr0IB(i,j) ) * factim
          WKxy2(i,j)     =  max(0.,WKxy2(i,j) * 1000.)
          werr0IB(i,j)   =  rainHY(i,j)   ! Rain
 
          WKxy3(i,j)     = (rainCA(i,j) + snowCA(i,j)
     .                   - wecp0IB(i,j) ) * factim
          WKxy3(i,j)     =  max(0.,WKxy3(i,j) * 1000.)
          wecp0IB(i,j)   =  rainCA(i,j) + snowCA(i,j)   ! Convective precip.
 
          WKxy4(i,j)     = (runoTV(i,j)   - wero0IB(i,j) ) * factim
          wero0IB(i,j)   =  runoTV(i,j)   ! RunOFF
 
          WKxy5(i,j)     =  (crysHY(i,j)  - wecr0IB(i,j)) * factim
          WKxy5(i,j)     =  max(0.,WKxy5(i,j) * 1000.)
          wecr0IB(i,j)   =  crysHY(i,j)  ! Ice crystals
 
        END DO
        END DO
 
        DO j=1,my
        DO i=1,mx
        DO k=1,nsx
            xynsx0(i,j,k) = WKxy1(i,j) + WKxy2(i,j) -
     .                      xynsx4(i,j,k) - xynsx5(i,j,k) ! smb = snf + rnf - sb
            xynsx14(i,j,k)= WKxy2(i,j) + xynsx2(i,j,k) - xynsx9(i,j,k) ! rfz = r
        END DO
c            if (isolSL(i,j).le.2) then ! ocean and sea-ice
c                xynsx0 (i,j,1) = NF_FILL_REAL ! SMB
c                xynsx5 (i,j,1) = NF_FILL_REAL ! Sublimation
c                xynsx2 (i,j,1) = NF_FILL_REAL ! Melting
c                xynsx4 (i,j,1) = NF_FILL_REAL ! Run-off
c                xynsx14(i,j,1) = NF_FILL_REAL ! Refreezing
c            endif
        END DO
        END DO
 
        ! Sum
 
C +     ************
        CALL UNwrite (ID__nc_ice,  'smb', dt_ICE, mx, my, nsx,xynsx0)
        CALL UNwrite (ID__nc_ice,  'snf', dt_ICE, mx, my,   1, WKxy1)
        CALL UNwrite (ID__nc_ice,  'rnf', dt_ICE, mx, my,   1, WKxy2)
        CALL UNwrite (ID__nc_ice,  'cry', dt_ICE, mx, my,   1, WKxy5)
        CALL UNwrite (ID__nc_ice,  'sbl', dt_ICE, mx, my, nsx,xynsx5)
        CALL UNwrite (ID__nc_ice,  'mlt', dt_ICE, mx, my, nsx,xynsx2)
        CALL UNwrite (ID__nc_ice,  'rof', dt_ICE, mx, my, nsx,xynsx4)
        CALL UNwrite (ID__nc_ice,  'rfz', dt_ICE, mx, my, nsx,xynsx14)
        CALL UNwrite (ID__nc_ice, 'snf3D', dt_ICE, mx, my, ml,snf_IB)
        CALL UNwrite (ID__nc_ice, 'sbl3D', dt_ICE, mx, my, ml,sbl_IB)
        CALL UNwrite (ID__nc_ice, 'QSSBL', dt_ICE, mx, my, ml,qssbl_IB)
        CALL UNwrite (ID__nc_ice, 'rnf3D', dt_ICE, mx, my, ml,rnf_IB)
        CALL UNwrite (ID__nc_ice, 'dep3D', dt_ICE, mx, my, ml,dep_IB)
        CALL UNwrite (ID__nc_ice, 'evp3D', dt_ICE, mx, my, ml,evp_IB)
        CALL UNwrite (ID__nc_ice, 'smt3D', dt_ICE, mx, my, ml,smt_IB)
 
        CALL UNwrite (ID__nc_ice, 'MBto', dt_ICE, mx, my, nsx,xynsx1)
        CALL UNwrite (ID__nc_ice, 'MBic', dt_ICE, mx, my,   1, WKxy5)
        CALL UNwrite (ID__nc_ice, 'MBsf', dt_ICE, mx, my,   1, WKxy1)
        CALL UNwrite (ID__nc_ice, 'MBrr', dt_ICE, mx, my,   1, WKxy2)
        CALL UNwrite (ID__nc_ice, 'MBrf', dt_ICE, mx, my,   1, WKxy2)
        CALL UNwrite (ID__nc_ice, 'MBcp', dt_ICE, mx, my,   1, WKxy3)
        CALL UNwrite (ID__nc_ice, 'MBru', dt_ICE, mx, my,   1, WKxy4)

        CALL UNwrite (ID__nc_ice, 'MBsw', dt_ICE, mx, my, nsx,xynsx9)
        CALL UNwrite (ID__nc_ice,  'MBm', dt_ICE, mx, my, nsx,xynsx2)

        CALL UNwrite (ID__nc_ice,'MBru2', dt_ICE, mx, my, nsx,xynsx3)
        CALL UNwrite (ID__nc_ice,'MBro2', dt_ICE, mx, my, nsx,xynsx4)
        CALL UNwrite (ID__nc_ice,'MBru3', dt_ICE, mx, my, nsx,xynsx4)


        CALL UNwrite (ID__nc_ice, 'MBsn', dt_ICE, mx, my, nsx,xynsx5)
        CALL UNwrite (ID__nc_ice, 'MBep', dt_ICE, mx, my, nsx,xynsx6) 
        CALL UNwrite (ID__nc_ice, 'MBet', dt_ICE, mx, my, nsx,xynsx7)
        CALL UNwrite (ID__nc_ice, 'MBsl', dt_ICE, mx, my, nsx,xynsx8)
        CALL UNwrite (ID__nc_ice, 'MBe' , dt_ICE, mx, my, nsx,
     .                                   xynsx6+xynsx7)
        CALL UNwrite (ID__nc_ice, 'MBs' , dt_ICE, mx, my, nsx,
     .                                   xynsx5+xynsx8)


        DO j=1,my
        DO i=1,mx

        tmp3=0
        DO k=1,mlhh
         tmp3=tmp3+smbhIB(i,j,k)
        ENDDO
        DO k=1,mlhh
         if(abs(tmp3)>0.001) then 
          smbhIB(i,j,k)=smbhIB(i,j,k)
     .                * xynsx1(i,j,1)/tmp3
         endif
        END DO

        tmp3=0
        DO k=1,mlhh
         tmp3=tmp3+swhIB(i,j,k)
        ENDDO
        DO k=1,mlhh
         if(abs(tmp3)>0.001) then 
          swhIB(i,j,k)=swhIB(i,j,k)
     .                * xynsx9(i,j,1)/tmp3
         endif
        END DO

        tmp3=0
        DO k=1,mlhh
         tmp3=tmp3+mehIB(i,j,k)
        ENDDO
        DO k=1,mlhh
         if(abs(tmp3)>0.001) then 
          mehIB(i,j,k)=mehIB(i,j,k)
     .                * xynsx2(i,j,1)/tmp3
         endif
        END DO

        tmp3=0
        DO k=1,mlhh
         tmp3=tmp3+0.1*mehIB(i,j,k)+0.9*ruhIB(i,j,k)
        ENDDO
        DO k=1,mlhh
         if(abs(tmp3)>0.001) then 
          ruhIB(i,j,k)=(0.1*mehIB(i,j,k)+0.9*ruhIB(i,j,k))
     .                * xynsx4(i,j,1)/tmp3
         else
          if(abs(xynsx4(i,j,1))>0.001) then
           ruhIB(i,j,k)=xynsx4(i,j,1)/real(mlhh)
          else
           ruhIB(i,j,k)=0.
          endif
         endif
        END DO

        tmp3=0
        DO k=1,mlhh
         tmp3=tmp3+suhIB(i,j,k)
        ENDDO
        DO k=1,mlhh
         if(abs(tmp3)>0.001) then 
          suhIB(i,j,k)=suhIB(i,j,k)
     .                * xynsx5(i,j,1)/tmp3
         endif
        END DO

        tmp3=0
        DO k=1,mlhh
         tmp3=tmp3+snfhIB(i,j,k)
        ENDDO
        DO k=1,mlhh
         if(abs(tmp3)>0.001) then 
          snfhIB(i,j,k)=snfhIB(i,j,k)
     .                * WKxy1(i,j)/tmp3
         endif
        END DO

        tmp3=0
        DO k=1,mlhh
         tmp3=tmp3+rfhIB(i,j,k)
        ENDDO
        DO k=1,mlhh
         if(abs(tmp3)>0.001) then 
          rfhIB(i,j,k)=rfhIB(i,j,k)
     .                * WKxy2(i,j)/tmp3
         endif
         prhIB(i,j,k)= rfhIB(i,j,k)+ snfhIB(i,j,k)
        END DO

        END DO
        END DO 

        ! For snow only

        CALL UNwrite (ID__nc_ice, 'SMBh',dt_ICE,mx, my, mlhh,smbhIB)
        CALL UNwrite (ID__nc_ice, 'SWh', dt_ICE,mx, my, mlhh, swhIB)
        CALL UNwrite (ID__nc_ice, 'MEh', dt_ICE,mx, my, mlhh, mehIB)
        CALL UNwrite (ID__nc_ice, 'PRh', dt_ICE,mx, my, mlhh, prhIB)
        CALL UNwrite (ID__nc_ice, 'SUh', dt_ICE,mx, my, mlhh, suhIB)
        CALL UNwrite (ID__nc_ice, 'RUh', dt_ICE,mx, my, mlhh, ruhIB)
        CALL UNwrite (ID__nc_ice,'SNFh', dt_ICE,mx, my, mlhh,snfhIB)
        CALL UNwrite (ID__nc_ice, 'SFh', dt_ICE,mx, my, mlhh,snfhIB)
        CALL UNwrite (ID__nc_ice, 'RFh', dt_ICE,mx, my, mlhh, rfhIB)
         

C +     Conservation on ice sheet: MBSF+MBRR-MBS-MBRU~MBTO-MBSW
C +     ************
 
        DO j=1,my
        DO i=1,mx
        DO k=1,nsx
          xynsx6(i,j,k)  = (weo_IB(i,j,k,1) - weo0IB(i,j,k,1)) * factim
          weo0IB(i,j,k,1)=  weo_IB(i,j,k,1) ! Run-off
          xynsx8(i,j,k)  = (weo_IB(i,j,k,3) - weo0IB(i,j,k,3)) * factim
          weo0IB(i,j,k,3)=  weo_IB(i,j,k,3) ! Run-off
          xynsx9(i,j,k)  = (weo_IB(i,j,k,4) - weo0IB(i,j,k,4)) * factim
          weo0IB(i,j,k,4)=  weo_IB(i,j,k,4) ! Run-off
          xynsx10(i,j,k) = (weo_IB(i,j,k,5) - weo0IB(i,j,k,5)) * factim
          weo0IB(i,j,k,5)=  weo_IB(i,j,k,5) ! Run-off
          xynsx11(i,j,k) = (weo_IB(i,j,k,6) - weo0IB(i,j,k,6)) * factim
          weo0IB(i,j,k,6)=  weo_IB(i,j,k,6) ! Run-off
        END DO
c        WKxy1(i,j)=0
c        WKxy2(i,j)=0
c        WKxy3(i,j)=0
c        WKxy4(i,j)=0
c        WKxy5(i,j)=0
c        DO k=1,nsx
c         WKxy1(i,j)=WKxy1(i,j)+SLsrfl(i,j,k)*xynsx6(i,j,k)
c         WKxy2(i,j)=WKxy2(i,j)+SLsrfl(i,j,k)*xynsx7(i,j,k)
c         WKxy3(i,j)=WKxy3(i,j)+SLsrfl(i,j,k)*xynsx8(i,j,k)
c         WKxy4(i,j)=WKxy4(i,j)+SLsrfl(i,j,k)*xynsx9(i,j,k)
c         WKxy5(i,j)=WKxy5(i,j)+SLsrfl(i,j,k)*xynsx10(i,j,k)
c        END DO
        END DO
        END DO
 
        CALL UNwrite (ID__nc_ice,  'MBro1', dt_ICE, mx, my,mw,xynsx6)
        CALL UNwrite (ID__nc_ice,  'MBro3', dt_ICE, mx, my,mw,xynsx8)
        CALL UNwrite (ID__nc_ice,  'MBro4', dt_ICE, mx, my,mw,xynsx9)
        CALL UNwrite (ID__nc_ice,  'MBro5', dt_ICE, mx, my,mw,xynsx10)
        CALL UNwrite (ID__nc_ice,  'MBro6', dt_ICE, mx, my,mw,xynsx11)

 
C +--3.2.2 Work Arrays Reset
C +  -----------------------
 
        DO j=1,my
        DO i=1,mx
          WKxy0(i,j) =0.0
          WKxy1(i,j) =0.0
          WKxy2(i,j) =0.0
          WKxy3(i,j) =0.0
          WKxy4(i,j) =0.0
          WKxy5(i,j) =0.0
          WKxy6(i,j) =0.0
          WKxy7(i,j) =0.0
          WKxy8(i,j) =0.0
          WKxy9(i,j) =0.0
        END DO
        END DO

          call NCSNC  (ID__nc_ice, RCODE)

      IF              (ID__nc_ice.ne.-1)                         THEN
C +       ************
          call UNclose(ID__nc_ice)
C +       ************
                       ID__nc_ice=-1
      END IF
 
      If( snapshot.and.ss==1) then
       ss         = 0
       fnamNC_tmp = fnamNC_ics
       dt_ICE     = dt_ICE - 1
       goto 801
      endif
 
                              OutdyIB0=OutdyIB0+1
        if(OutdyIB0>=OutdyIB) OutdyIB0=0
 
        itrdIB = iterun
 
      END IF  !Daily
 
 
C +--3.2.3 NetCDF File Closure
C +  -------------------------
 
      IF              (ID__nc_ice.ne.-1)                         THEN
C +       ************
          call UNclose(ID__nc_ice)
C +       ************
                       ID__nc_ice=-1
      END IF
 
      return
      end
