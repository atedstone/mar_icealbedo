import xarray as xr
import numpy as np
import subprocess
import matplotlib.pyplot as plt
	
import mar_raster

mar = mar_raster.open_xr('/scratch/MAR_GRo_out/outputs_MARv3.10.0/hindcast_20190906/ICE.2016.04-09.o55.nc')

#popmin1 = -7.642 * mar.SH + 13170
popmin1 = -5.256 * mar.SH + 9639
popmin2 = np.maximum(popmin1, 179)
popmin3 = np.minimum(popmin2, 4000)

#slomod = -18.75 * np.arccos(mar.SLO) + 1
slomod = -35 * np.arccos(mar.SLO) + 1.35
slomod = np.maximum(0, np.minimum(1, slomod))
popmin4 = popmin3 * slomod





old_pth = '/scratch/MARv3.9.6-15km-ERAI/*nc'
melt_old = mar_raster.open_mfxr(old_pth, dim='TIME', transform_func=lambda ds: ds.ME)
mean_melt = melt_old.resample(TIME='1AS').sum(dim='TIME').mean(dim='TIME')

mean_melt.load()

startpop = 2.4775 * mean_melt - 1194.

startpop = np.minimum(4000, np.maximum(179, startpop))

startpop.name = 'startpop'

startpop = startpop.squeeze()
startpop = startpop.drop('SECTOR1_1')

startpop.to_netcdf('~/Dropbox/work/startpop.nc')

#!! Don't run this within python shell as mar_raster has bottomup flipped....
#cmd = 'gdal_merge.py -ul_lr -892500 1657500 832500 -1492500 -o ~/Dropbox/work/startpopGRo.cdf -of NetCDF -init 0 ~/Dropbox/work/startpop.nc'
#retcode = subprocess.call(cmd, shell=True)
#print(retcode)

#check = xr.open_dataset('~/Dropbox/work/startpopGRo.cdf')
#plt.figure()
#plt.show()
	


"""
1. Initial coordinates are the cell centres of an un-Concated ICE file 
(reported as xmin...ymax etc) using xarray
2. Add/substract 7500 m as appropriate to get entire width and height of domain

# Do it as a merge, not a warp
gdal_merge.py -ul_lr -892500 1657500 832500 -1492500 -o startpopGRo.cdf -of NetCDF -init 0 startpop.nc 
"""