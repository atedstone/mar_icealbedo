      
      real    ialMEh (mx,my) ! Temporary store of temperature 
      real    ialMEc (mx,my) ! Counter of positive hourly temp hours.
      real    ialPRh (mx,my)     ! Temporary store of photosynthetically-active radiation
      real    ialPRc (mx,my)     ! Counter of positive PAR hours
      real    ialRFd (mx,my)     ! Temporary store of rainfall
      integer lst_hr(mx,my)      ! Timestamp of last hourly average
      integer lst_da(mx,my)      ! Timestep of last day of pop update
      integer lstHIt(mx,my)      ! Iteration of last hourly average   

C       real    ialPHs(mx,my)      ! Productive hours proportion calculated at last run
C       real    ialRLs(mx,my)      ! Rainfall losses calculated at last run 
C       integer iallrt(mx,my)      ! iterun value of last run

      common /ialpopr/ ialMEh, ialPRh, ialRFd, ialMEc, ialPRc

      common /ialpopi/ lst_hr, lst_da, lstHIt
