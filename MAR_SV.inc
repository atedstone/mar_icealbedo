      integer   klonv    ,nsol  ,nsno
      parameter(klonv=  1,nsol=  6,nsno=  21)
      integer   nb_wri
      parameter(nb_wri= 10)
      integer   ttwinl   ! Temperature moving average window length in days (*AJT*) - for albedo scheme
      parameter(ttwinl=21)
      integer   nice     ! Number of ice weathering crust layers
      parameter(nice=9)
      real,parameter,dimension(nice) :: lyrthk = (/0.05, 0.05, 0.1, 0.1,
     .  0.1, 0.1, 0.15, 0.15, 0.2/)       ! Thickness of each vertical layer, m
