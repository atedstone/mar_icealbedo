 
      subroutine INIgen
 
C +------------------------------------------------------------------------+
C | MAR INPUT                                               5-10-2020  MAR |
C |   SubRoutine INIgen set up MAR Initialization Procedure                |
C |                                                                        |
C +------------------------------------------------------------------------+
 
 
c #AO USE mod_oasis !
c #AO USE mar_module      !TANGO modules

 
      IMPLICIT NONE
 
 
C +--Global Variables
C +  ================
 
      include 'MARCTR.inc'
      include 'MARphy.inc'
 
      include 'MARdim.inc'
      include 'MARgrd.inc'
      include 'MAR_GE.inc'
 
      include 'MARSND.inc'
 
      include 'MAR_DY.inc'
c #NH include 'MAR_NH.inc'
 
      include 'MAR_LB.inc'
      include 'MAR_UB.inc'
c #Di include 'MAR_DI.inc'
      include 'MARsIB.inc'
 
      include 'MAR_TE.inc'
      include 'MAR_TU.inc'
      include 'MAR_FI.inc'
 
      include 'MAR_RA.inc'
 
      include 'MAR_HY.inc'
      include 'MAR_CA.inc'
      include 'MAR_PB.inc'
c #TC include 'MAR_TC.inc'
 
      include 'MAR_SL.inc'
c #AO include 'MAR_AO.inc'
c #PO include 'MAR_PO.inc'
      include 'MAR_SV.inc'
c #AO include 'MAR_TV.inc'
c #sn include 'MAR_SN.inc'
c #BS include 'MAR_BS.inc'
 
c #OL include 'MAR_OL.inc'
 
      include 'MAR_IO.inc'
      include 'MARsSN.inc'      !*CL*

c #AO character*100 filein
c #AO character*3   mxc,myc
c #AO logical       file_exists
 
      include 'MAR_IB.inc'   !*AJT*
 
C +--Local Variables and DATA
C +  ========================
 
      external zext
      integer  zext
 
      logical  zoro
      logical  verti0
      integer  itever,itexpe2
c #NH integer  iyrONH,mmaONH,jdaONH,jhuONH
      integer  iyrCVA,mmaCVA,jdaCVA,jhuCVA
      integer  iyrHYD,mmaHYD,jdaHYD,jhuHYD
c #TC integer  iyrTCA,mmaTCA,jdaTCA,jhuTCA
      integer  iyrTUR,mmaTUR,jdaTUR,jhuTUR
      integer  iyrSOL,mmaSOL,jdaSOL,jhuSOL
c #PO integer  iyrPOL,mmaPOL,jdaPOL,jhuPOL
c #sn integer  iyrSNO,mmaSNO,jdaSNO,jhuSNO
      integer  imezdy,jmezdy,mzabs1,itizon
      integer  lo_CAU,ipri  ,iv_ini,n, isl
 
C +--Modified Time Step
C +  ~~~~~~~~~~~~~~~~~~
      integer  YYtmp ,MMtmp ,DDtmp
 
      real     dtdom ,zmin_0,aavu_0,bbvu_0,ccvu_0,ps_sig
c #HF real     hham  ,nham  ,hhac  ,thac
      real     aladyn,alodyn,gradTz,rhcrit,tstart
      real     sh2(mx,my), sh3(mx,my,mw-1)
 
      logical  res !*AJT* 
      integer  ii,jj !*AJT*
      character* 3 swich(0:1)
      data    swich/'OFF','ON '/
      data   ps_sig/100./
 
 
C ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C +++ INITIALISATION of the DOMAIN CHARACTERISTICS +++++++++++++++++++++++++
C ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 
 
      open (unit=1,status='old',file='MARdom.dat')
      rewind     1
       read     (1,141)  explIO
 141   format   (a3)
       read     (1,145)  GElat0,GElon0,GEddxx
       read     (1,1425) mma0GE,jda0GE,jhu0GE,itizon,iyr0GE
 1425  format   (4i4,i4)
       read     (1,1426) imez  ,jmez  ,maptyp,GEtrue
 1426  format   (3i4,28x,e13.6)
       read     (1,1427) igrdIO
 1427  format   (5i4)
       read     (1,1427) jgrdIO
       read     (1,1427) IO_gen
                         IO_loc=IO_gen
       read     (1,1427) mxw1IO,mxw2IO,ixw_IO
       read     (1,1427) myw1IO,myw2IO,iyw_IO
       read     (1,1427) mzw1IO,mzw2IO,izw_IO
       read     (1,1427) kkatIO,kmidIO
C +...                   +-> For output on NetCDF Files:
C +                          Assumed Levels of Bound.Layer / Mid Troposph.
       read     (1,145)  dx,dy,dtdom
 145   format   (4d13.6)
       read     (1,1450) verti0
 1450  format   (l3)
       read     (1,145)  ptopDY
C +...                   ptopDY: model top pressure
       read     (1,145)  zmin_0,aavu_0,bbvu_0,ccvu_0
       z__SBL          = zmin_0
       read     (1,145)  FIslot,FIslou,FIslop,FIkhmn
       read     (1,145)  TUkhff,TUkhmx
                                TUkhmx=TUkhmx*dtdom /dt
       read     (1,145)  tequil,dtquil
       read     (1,145)  zs_SL,zn_SL,zl_SL,cs2SL
       read     (1,145)  sst_SL
       read     (1,145)  dtagSL
       read     (1,145)  w20SL,wg0SL,wk0SL,wx0SL
 
       read     (1,1430)
 1430  format   (1x)
       read     (1,143)  isolSL
 143   format   ((10i13))
       read     (1,1430)
       read     (1,1432) sh2
       if(itexpe<=1) then
        do i=1,mx; do j=1,my
         sh(i,j)=sh2(i,j)
        enddo    ; enddo
       endif
 1432  format   ((10d13.6))
       read     (1,1430)
       read     (1,1432) SL_z0
       read     (1,1430)
       read     (1,1432) SL_r0
       read     (1,1430)
       read     (1,1432) ch0SL
       read     (1,1430)
       read     (1,1432) rsurSL
       read     (1,1430)
 
C +--Underlaying Surface Albedo (to which Surface alb0SL is set,
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~  unless NOT 1st Run, itexpe.ne.0)
       read     (1,1432) albsSL
       do j=1,my
       do i=1,mx
           alb0SL(i,j) = albsSL(i,j)
       end do
       end do
 
       read     (1,1430)
       read     (1,1432) eps0SL
       read     (1,1430)
       read     (1,1432) d1_SL
 
c #po  read     (1,1430)
c #po  read     (1,1432) uocnPO
c #po  read     (1,1430)
c #po  read     (1,1432) vocnPO
c #po  read     (1,1430)
c #po  read     (1,1432) aPOlyn
 
       IF (geoNST)                                                THEN
         read   (1,1430)
         read   (1,1432) ((GElonh(i,j),i=1,mx),j=1,my)
         read   (1,1430)
         read   (1,1432) ((GElatr(i,j),i=1,mx),j=1,my)
         read   (1,1430)
         read   (1,1432)    sigma
 
         DO j=1,my
         DO i=1,mx
           GElatr(i,j) = GElatr(i,j) * degrad  !  degrees --> radians
           GElonh(i,j) = GElonh(i,j) / 15.0    !  degrees --> hours
         END DO
         END DO
 
         DO k=1,mz
            zsigma(k) =-(sst_SL / 0.0065) *   ((1.e0+(sigma(k)     -1.)
     .                                        *(1.e2/ps_sig))
     .                                    ** (RDryAi*0.0065/gravit)-1.)
         END DO
 
         IF (IO_gen.ge.2)                                           THEN
           write(21,130)( sigma(k),k=1,mz)
 130       format(/,'  Sigma    Levels :',/,(1x,15f8.4))
           write(21,131)(zsigma(k),k=1,mz)
 131       format(/,'  Altitude Levels :',/,(1x,15f8.1))
         END IF
       END IF
 
C +--Orography Roughness
C +  ~~~~~~~~~~~~~~~~~~~
c #OR    DO k=1,mw
c #OR    DO j=1,my
c #OR    DO i=1,mx
c #OR       SLzoro(i,j,k) = SL_z0(i,j,k)
c #OR    END DO
c #OR    END DO
c #OR    END DO
c #OR       zoro = .TRUE.
c #RN       zoro = .FALSE.
c #OR    IF(zoro)
c #OR.      STOP '@#! Uses Andreas parameterization of z0_h with z0_oro'
      close(unit=1)
 
 
C + ***   *CL* Read subgrid topography   ***
      IF (mw .eq. 5) then
 
      open (unit=555,status='old',file='MARdom2.dat')
 
      print*, '***************************'
      print*, '*   Read of MARdom2.dat   *'
      print*, '***************************'
 
       read     (555,'(f12.6)') sh3
       if(itexpe<=1) then
        do i=1,mx; do j=1,my
        do k=1,nsx-1
         sh_int(i,j,k)=sh3(i,j,k)
        enddo
         sh_int(i,j,nsx) = sh(i,j)
        enddo   ; enddo
       endif
      close(unit=555)
 
      ENDIF


! + ***  AO_CK * Read Weight file (combine SSC from reanalysis and ocean model)   ***
c #AO print *, "check if coupling AO:", coupling_ao
       
c #AO if (coupling_ao) then
      
c #AO   write(mxc,'(i3)') mx
c #AO   if(mx<100) write(mxc,'(i2)') mx
c #AO   write(myc,'(i3)') my
c #AO   if(my<100) write(myc,'(i2)') my
	
c #AO   filein='TROUPLE-'//trim(mxc)//'x'//trim(myc)//'.cdf'
c #AO   inquire(file=trim(filein),exist=file_exists)

c #AO   if (file_exists) then
c #AO    call CF_READ2D(trim(filein),'WEIGHT',1,mx,my,1,weightao)
c #AO	 print *, 'Read ', trim(filein)
c #AO   else 
c #AO    print *, 'Define TROUPLE file before coupling!' 
c #AO       stop
c #AO   endif

	 
c #AO	do i=1,mx ; do j=1,my                 !NO NEMO field in the first 3 pixels from the relaxation zone
c #AO	 if (i .le. 3 )    weightao(i,j)=1.   !They MUST also be considered as LAND for OASIS
c #AO    if (j .le. 3 )    weightao(i,j)=1.   !even if they are ocean
c #AO	 if (i .ge. mx-2 ) weightao(i,j)=1.
c #AO	 if (j .ge. my-2 ) weightao(i,j)=1.
c #AO	enddo; enddo
	
	!USING variable from coupling
	!1= rean everywhere 0=NEMO everywhere
	! = weightao NEMO/REANALYSIS
	
c #AO	weightao_sst=weightao    !sst
c #AO	weightao_sic=weightao    !SIC
c #AO   weightao_st=weightao     !Sea ice/ Snow on ice surface temp
c #AO	weightao_al=weightao     !Sea/Snow on ice Albedo
c #AO	weightao_sit=weightao    !Sea ice thickness
c #AO	weightao_snt=weightao    !Snow on ice thickness
	
c #AO	print *, "If doubt(s) oncoupling, 
c #AO.            check weightao definition"
	
c #AO  endif 
 
C +--SVAT IO
C +  -------
 
        IF (vegmod.and.        .not.reaVAR)                       THEN
 
C +       ***********
          call SVAsav('read')
C +       ***********
 
        END IF
 
 
C +--Time Discretisation
C +  ===================
 
C +--Slow Dynamics
C +  -------------
 
      idt    = dt
      jdt    =(dt-idt)*100.
 
 
C +--Fast Dynamics
C +  -------------
 
      dtfast = dt     /(ntFast+1)
 
 
C +--Subgrids
C +  --------
 
      if (dtDiff.lt.dt)     then
       if(dtDiff.gt.0.)     then
          ntDiff =  dt / dtDiff
C +...    ntDiff :  Number of  Subgrid Scale Steps for 1 Dynamical Step
 
          dtDiff =  dt / ntDiff
C +...    dtDiff :  Calibrated Subgrid Scale Time Step
 
          jtDiff =   1
       else
          ntDiff =   1
          jtDiff =   nboucl * nprint + 1
       end if
      else
          jtDiff =       dtDiff / dt
          jtDiff =  max (jtDiff,1)
C +...    jtDiff :  Number of  Dynamical Steps for 1 Subgrid Scale Step
 
          dtDiff =  dt * jtDiff
C +...    dtDiff :  Calibrated Subgrid   Scale Time Step
 
          ntDiff =   1
      end if
      if (dtDiff.gt.0.)     then
cXF
          ntDiff =  1
          dtDiff =  dt
          jtDiff =  1
cXF
      end if
 
 
C +--Surface   Physics
C +  -----------------
 
      if (dtPhys.lt.dt) then
          ntPhys =  dt / dtPhys
C +...    ntPhys :  Number of  Surface Phys. Steps for 1 Dynamical Step
 
          dtPhys =  dt / ntPhys
C +...    dtPhys :  Calibrated Surface Phys. Time Step
 
          jtPhys =   1
      else
          jtPhys =       dtPhys / dt
          jtPhys =  max (jtPhys,1)
C +...    jtPhys :  Number of  Dynamical Steps for 1 Surface Phys. Step
 
          dtPhys =  dt * jtPhys
C +...    dtPhys :  Calibrated Surface Physics Time Step
 
          ntPhys =   1
      end if
 
cXF
          ntPhys =  1
          dtPhys =  dt
          jtPhys =  1
cXF
 
 
C +--Radiation Physics
C +  -----------------
 
      if (dtRadi.lt.dt) then
          write(6,*) ' NO Split Time Differ.  on dtRadi ',
     .               ' !!#\B9@|#@&##!!     EMERGENCY EXIT '
          STOP
          ntRadi =  dt / dtRadi
C +...    ntRadi :  Number of  Surface Phys. Steps for 1 Dynamical Step
 
          dtRadi =  dt / ntRadi
C +...    dtRadi :  Calibrated Surface Phys. Time Step
 
          jtRadi2 =  1
      else
          jtRadi2 =       dtRadi / dt
C +...    jtRadi :  Number of  Dynamical Steps for 1 Surface Phys. Step
 
          dtRadi  =  dt * jtRadi2
C +...    dtRadi :  Calibrated Surface Physics Time Step
 
          ntRadi =   1
      end if
 
 
C +--Other Constants
C +  ---------------
 
      t2SLtn = 1.0/dtPhys-0.50/cs2SL
      t2SLtd = 1.0/dtPhys+0.50/cs2SL
      fracSL =      dt /0.6e3
      fracSL = min(unun,fracSL)
C +...fracSL : Fractional Time (Blowing Snow Srf Flux Computation)
C +            Factor   1.8d3 is the Turbulence Time Scale (1/2 h)
C +            Factor   0.6d3 is used.
 
 
C +--Coriolis Parameter
C +  ==================
 
      fcorDY(imez,jmez) = 2.0*earthv*sin(GElat0*degrad)
C +...fcorDY            : Coriolis Parameter (Indicative Value)
 
 
C +--OUTPUT
C +  ======
 
C +   ---------------------
      if (IO_loc.ge.1) then
C +   ---------------------
 
       write(21,600) explIO,GElat0,GElon0,
     .               jda0GE,mma0GE,jhu0GE,itizon,
     .                      minuGE,jsecGE,fcorDY(imez,jmez),cz0_GE
  600  format(/,' SIMULATION ',a3,/,' ++++++++++++++',//,
     . ' Lat.',f5.1,3x,'Long.',f6.1,4x,'Date :',i3,'-',i2,
     . ' / ',i2,' h.UT +(',i3,')LT',i3,' min.',i3,' sec.',
     .  //,' f(Coriolis) = ',e12.5,
     .   /,' cos(Z) min  : ',e12.5)
 
       write(21,601)
 601   format(//,' CODE STATUS',/,' +++++++++++',/,
c #DP./,'#DP DOUBLE PRECISION -- DOUBLE PRECISION -- DOUBLE PRECISION ',
c #//./,'#// Parallelisation Set Up is activated   (software MPI used)',
c #HF./,'#HF Initialisation of Huang and Lynch 1993   (HAMMING Filter)',
 
c #NH./,'#NH DYNAMICS: Non-Hydrost. Code  (adapted from Laprise, 1992)',
c #nh./,'#nh DYNAMICS: Non-Hydrost. Code  (Slope            Contribut)',
c #DH./,'#DH DYNAMICS: Non-Hydrost. Code  (Diabatic Heating Contribut)',
c #ON./,'#ON DYNAMICS: Non-Hydrost. Corr. (Weisman &al.1997 MWR p.541)',
c #DD./,'#DD DYNAMICS: Mass Divergence Damper (Skamarock &Klemp, 1992)',
     ./,'#VN DYNAMICS: Variable Number of Leap-Frog Loops (Fast Waves)',
c #IL./,'#IL DYNAMICS: PGF: SBL Fraction with  Air = Surface Temperat.',
c #GE./,'#GE DYNAMICS: Geographic Coordinates may be red in MARdom.dat',
c #CC./,'#CC DYNAMICS: Constant Coriolis Parameter = fcorDY(imez,jmez)',
c #2Z./,'#2Z DYNAMICS: Zonally Averaged Model (latitude = x-direction)',
c #HE./,'#HE DYNAMICS: NORLAM       Vertical Discretisation(29 Levels)',
c #lm./,'#lm DYNAMICS: LMDZ   Model Vertical Discretisation(11 Levels)',
c #PA./,'#PA DYNAMICS: Parish Model Vertical Discretisation(10 Levels)',
c #PV./,'#PV DYNAMICS: Large Scale Flow conserves Pot. Vort. (2D ONLY)',
c #pv./,'#pv DYNAMICS: Large Scale Flow conserves Pot. Temp. (2D ONLY)',
c #UW./,'#UW DYNAMICS: Advect.  3rd Accurate in Space Upstream Scheme ',
c #UP./,'#UP DYNAMICS: Vertical 1st Accurate in Space Upstream Scheme ',
c #ZU./,'#ZU DYNAMICS: Vertical Advection: Cubic Spline (4th accurate)',
c #ZO./,'#ZO DYNAMICS: Vertical Advection: Cubic Spline (+Open  SrfBC)',
c #UR./,'#UR DYNAMICS: Vertical Advection/ Upper Radiating Bound.Cond.',
c #EP./,'#EP DYNAMICS: Lateral Sponge included in    Horizontal Filter',
c #RB./,'#RB DYNAMICS: Lateral BC: Carpenter(1982) Sommerfeld Modified',
     ./,'#DA DYNAMICS: Lateral BC: Davies (1976) BC on Wind // Lat.B. ',
c #da./,'#da DYNAMICS: Lateral BC: Davies (1976) BC: K, nu  computed. ',
c #FB./,'#FB DYNAMICS: Lateral BC: Fixed in Horizontal Cubic Spline   ',
c #OB./,'#OB DYNAMICS: Lateral BC: Zero Gradient                      ',
c #OG./,'#OG DYNAMICS: Lateral BC: (Left) NO Nudging if relaxg=.false.',
c #ob./,'#ob DYNAMICS: Lateral BC: Zero Gradient (Subroutine LBC000)  ',
     ./,'#RF DYNAMICS: Top BC: Rayleight Friction in the Top Sponge   ',
c #Di./,'#Di DYNAMICS: Top BC: Dirichlet  (fixed)                     ',
c #V+./,'#V+ DYNAMICS: Top BC: Von Neuman (prescrib.non zero-gradient)',
c #PS./,'#PS DYNAMICS: Domain Averaged Pressure Thickness   maintained',
c #DY./,'#DY DYNAMICS: OUTPUT: Components  lowest Level Forces Balance',
 
c _PE./,'_PE DIFFUSION:(%Grad.)   Slope      USE+ _HH or     (_HH #CR)',
c #PE./,'#PE DIFFUSION:(%Deform.) Slope      USE+ #DF or (#DF #DC #CR)',
c _HH./,'_HH DIFFUSION:(%Grad.)   Vert.Cor.  USE+ _PE                 ',
c #DF./,'#DF DIFFUSION:(%Deform.) Vert.Cor.  USE+ #PE or (#PE #DC #CR)',
c #DC./,'#DC DIFFUSION:(%Deform.)            USE+        (#DF #PE #CR)',
c #CR./,'#CR DIFFUSION: Cross Corr.    USE+ (_PE _HH) or (#DF #PE #DC)',
 
c #FE./,'#FE FILTERING: Digital Filtering of TKE                      ',
c #fe./,'#fe FILTERING: Digital Filtering of TKE  is   not vectorized ',
c #FO./,'#FO FILTERING: Digital Filtering of TKE (zero gradient at LB)',
c #KS./,'#KS FILTERING: Upper Sponge is solved by horizontal filtering',
 
c #BR./,'#BR TURBULENCE: 2.5 Level  2nd Order  (Brasseur         1997)',
     ./,'#CA CONVECTIVE  Adjustment (general                 Set Up)  ',
c #cA./,'#cA CONVECTIVE  Adjustment (no Double Counting      Set Up)  ',
     ./,'#ca CONVECTIVE  Adjustment (no Vector               Set Up)NV',
c #FC./,'#FC CONVECTIVE  Adjustment (Fritsch & Chappell 1980 Set Up)  ',
c #fc./,'#fc CONVECTIVE  Adjustment (Fritsch & Chappell 1980 Set Up)NV',
c #kf./,'#kf CONVECTIVE  Adjustment (Kain    & Fritsch  1990 Improvm.)',
c #IT./,'#IT CONVECTIVE  Adjustment (over 5km Adiabatics Starting Pts)',
c #AN./,'#AN CONVECTIVE  Adjustment (Subgrid Mountain Breeze included)',
c #WD./,'#WD CONVECTIVE  Adjustment (Water Detrainment       included)',
c #CG./,'#CG CONVECTIVE  Adjustment (Cloud Glaciation        included)',
c #ND./,'#ND CONVECTIVE  Adjustment (No Precip if LevFSink<LiftCond.L)',
c #vT./,'#vT CONVECTIVE  Adjustment (Virtual Temperature  is computed)',
     ./,'#PB CONVECTIVE  Adjustment (Peter Bechtold     2000 Set Up)  ',
     ./,'#pb CONVECTIVE  Adjustment (Peter Bechtold     2000 Set Up)NV',
c #KE./,'#KE CONVECTIVE  Adjustment (Emanuel & Zivkovic 1999 Set Up)  ',
 
c #LE./,'#LE TURBULENCE: K  : Louis                      (1979) BLM 17',
c #Kl./,'#Kl TURBULENCE: K-l: Therry & Lacarrere         (1983) BLM 25',
c #PD./,'#PD TURBULENCE: K-e: Original Duynkerke         (1988) JAS 45',
     ./,'#TA TURBULENCE: K-e: Dissipation + Advect.Horiz.TKE Transport',
     ./,'#TD TURBULENCE: K-e: Dissipation + Diffus.Horiz.TKE Transport',
c #AV./,'#AV TURBULENCE: K-e: Buoyancy includes      Aerosol Loading  ',
c #HR./,'#HR TURBULENCE: K-e: Huang & Raman              (1991) BLM 55',
c #KI./,'#KI TURBULENCE: K-e: Kitada                     (1987) BLM 41',
c #BH./,'#BH TURBULENCE: K-e: Kitada (modified)           USE with #KI',
     ./,'#KC TURBULENCE: T.K.E.(mz1) := T.K.E.(mz)                    ',
     ./,'#KA TURBULENCE: T.K.E. & e(T.K.E.) Filter along the vertical ',
c #AM./,'#AM TURBULENCE: u*   Time Mean (BOX Moving Average)          ',
c #AT./,'#AT TURBULENCE: u*T* Time Mean (BOX Moving Average)          ',
c #AS./,'#AS TURBULENCE: u*s* Time Mean (BOX Moving Average)          ',
c #VX./,'#VX TURBULENCE: u*q* limited to SBL Saturat. Specif. Humidity',
c #De./,'#De TURBULENCE: Top BC: Dirichlet (fixed) (ect_TE and eps_TE)',
c #WE./,'#WE TURBULENCE: T.K.E. OUTPUT on File MAR.TKE                ',
c #AE./,'#AE TURBULENCE: Aerosols Erosion / Turbulent Diffusion Coeff.',
     ./,'#SY TURBULENCE: Sea Spray Parameterization (Andreas, 199x) ON',
 
c #DU./,'#DU SBL: Univ.Funct.:    Duynkerke(1991)                     ',
c #BU./,'#BU SBL: Univ.Funct.:    Businger (1973)  USE with _NO OR #NO',
c _NO./,'_NO SBL: Univ.Funct.: NO Noilhan  (1987)  USE with #BU OR #DR',
c #NO./,'#NO SBL: Univ.Funct.:    Noilhan  (1987)  USE with #BU       ',
c #DR./,'#DR SBL: Univ.Funct.:    Dyer     (1974)  USE with _NO       ',
c #LP./,'#LP SBL: Blowing Snow Fric. Veloc. Thr. (Li and Pomeroy 1997)',
c #DS./,'#DS SBL: Blowing Snow SBL   Flux   (analytical Form of dq/dz)',
     ./,'#ZS SBL: Mom.: Roughn.Length= F(u*) Chamberlain (1983),  Sea ',
c #ZN./,'#ZN SBL: Mom.: Roughn.Length= F(u*) Shao  & Lin (1999), Snow ',
c #ZA./,'#ZA SBL: Mom.: Roughn.Length= F(u*) Andreas &al.(2004), Snow ',
c #RN./,'#RN SBL: Heat: Roughn.Length= F(u*,z0)  Andreas (1987)       ',
     ./,'#ZM SBL: M/H   Roughn.Length: Box Moving Average (in Time)   ',
c #OR./,'#OR SBL: Orography Roughness included from SL_z0 in MARdom   ',
     ./,'#SB Surface Boundary: modified externally (from Campain Data)',
     ./,'#TI Turbul. Heat Surface Flux: Implicit numerical Scheme     ',
     ./,'#QE Turbul. H2O  Surface Flux: Explicit numerical Scheme     ',
     ./,'#FI Turbul. Mom. Surface Flux: Implicit numerical Scheme     ',
c #BI./,'#BI Blowing Snow Surface Flux: Implicit numerical Scheme     ',
 
c #OL./,'#OL TEST:      Linear Mountain Wave: Specific IO    (2D ONLY)',
c #OM./,'#OM TEST: (Non)Linear Mountain Wave: Specific INPUT (2D ONLY)',
c #OS./,'#OS TEST:      Linear Mountain Wave: Specific IO    (2D ONLY)',
c #K1./,'#K1 TEST: LBC: Katab. Atmos.Warming                 (1D ONLY)',
c #EK./,'#EK TEST: EKMAN Spiral: Constant Vertical Turbul. Coefficient',
c #CL./,'#CL TEST: Convective Mixed Layer Test         (HS = 100 W/m2)',
c #NL./,'#NL TEST: Nearly   Neutral Layer Test         (HS =   0 W/m2)',
 
c #TC./,'#TC TraCer   Advection-Diffusion Equation        is turned ON',
c #tc./,'#tc TraCer   Filtering is  not vectorized                    ',
c #TO./,'#TO TraCer   Open Lateral Boundary Conditions on digit.Filter',
c #TS./,'#TS TraCer   Tracer Deposition diagnostic        is turned ON',
c #BD./,'#BD TraCer   Aeolian Erosion  Submodel           is turned ON',
c #DV./,'#DV TraCer   Aeolian Erosion  Submodel: Air Loading by Dust  ',
c #CH./,'#CH Chemical Atmospheric         Model       may be turned ON',
c #MV./,'#MV TraCer   Total Mass          Verification    is turned ON',
 
     ./,'#HY Explicit Cloud MICROPHYSICS              may be turned ON',
     ./,'#hy Explicit Cloud MICROPHYSICS: NO Vectorisation Optmization',
c #HM./,'#HM Explicit Cloud MICROPHYSICS: Hallett-Mossop Ice Multipl. ',
c #hm./,'#hm Explicit Cloud MICROPHYSICS: Hallett-Mossop Ice Mult.  NV',
c #LI./,'#LI Explicit Cloud MICROPHYSICS: Lin et al. (1983) Autoconv. ',
c #BS./,'#BS Explicit Cloud MICROPHYSICS: Blow. *(Snow)         Model ',
     ./,'#HV Explicit Cloud MICROPHYSICS: Air Loading by Hydrometeors ',
c #BV./,'#BV Explicit Cloud MICROPHYSICS: SBL Loading by all Water Sp.',
c #bv./,'#bv Explicit Cloud MICROPHYSICS: SBL Loading not vectorized  ',
c #SS./,'#SS Explicit Cloud MICROPHYSICS: Blow. *(Snow)  Linear Model ',
c #S0./,'#S0 Explicit Cloud MICROPHYSICS: Blow. *(Byrd)  Linear Model ',
c #EM./,'#EM Explicit Cloud MICROPHYSICS: de Montmollin Parameterizat.',
c #BW./,'#BW Explicit Cloud MICROPHYSICS: Blowing Snow Statistics     ',
c #b2./,'#b2 Explicit Cloud MICROPHYSICS: Blowing Snow Statistics (II)',
c #EV./,'#EV Explicit Cloud MICROPHYSICS: Snow Erosion Statistics     ',
     ./,'#HW Explicit Cloud MICROPHYSICS: OUTPUT of qr,qs, qw,qi on NC',
c #EW./,'#EW Explicit Cloud MICROPHYSICS: OUTPUT (Ener./Mass) (Unit 6)',
c #WH./,'#WH Explicit Cloud MICROPHYSICS: OUTPUT              (Unit 6)',
c #WQ./,'#WQ Explicit Cloud MICROPHYSICS: OUTPUT (Full Verif) (Unit 6)',
c #WB./,'#WB Explicit Cloud MICROPHYSICS: Water Conservation Controled',
c #WW./,'#WW Explicit Cloud MICROPHYSICS: Water Conservation Summary  ',
c #ww./,'#WW Explicit Cloud MICROPHYSICS: Water Conservation Summary +',
c #WF./,'#WF Explicit Cloud MICROPHYSICS: Water Conservation is Forced',
c #HO./,'#HO Explicit Cloud MICROPHYSICS: Zero-Gradient Lat.Bound.Cond',
 
c #MR./,'#MR PHYSICS: MARrad: Solar/Infrared     (Laurent LI set up)  ',
c #AZ./,'#AZ PHYSICS: Solar : Direct Radiation:   Surface Slope Impact',
c #MM./,'#MM PHYSICS: Solar : Direct Radiation:   Mountains Mask    ON',
c #TR./,'#TR PHYSICS: Solarn: Clear Sky, without Underlying Reflection',
     ./,'#EE PHYSICS: radCEP: ECMWF   routine    (cfr. JJ Morcrette)  ',
c #LL./,'#LL PHYSICS: radLMD: radlwsw routine    (Laurent LI set up)  ',
c #ll./,'#ll PHYSICS: radLMD: radlwsw routine    (Laurent LI set up)NV',
c #AR./,'#AR PHYSICS: radLMD: radlwsw routine Interactive Terr.Aerosol',
c #WL./,'#WL PHYSICS: radLMD: radlwsw routine IO (Laurent LI set up)  ',
 
c #SA./,'#SA PHYSICS: MAR Code behaves  as a Stand Alone Surface Model',
 
c #FR./,'#FR Surface Model: Force Restore (Deardorff) at least   is ON',
c #WG./,'#WG Soil Humidity: Force Restore (Deardorff) may be turned ON',
 
c #AO./,'#AO COUPLING with  NEMO  Ocean-Sea-Ice Model using OASIS     ',
 
c #PO./,'#PO POLYNYA Model                            may be turned ON',
c #FD./,'#FD POLYNYA Model: Sea-Ice Velocity is Free Drift            ',
c #HA./,'#HA POLYNYA Model: POLYNYA Surface Energy Balance:  2000 W/m2',
c #HI./,'#HI POLYNYA Model: Hibler (1979) Parameteriz. of Ice Strength',
c #CN./,'#CN POLYNYA Model: Prescription of a Local Avective Time Step',
c #ST./,'#ST EVOLUTIVE SST (Sea Surface Temperature/Swab Ocean)       ',
c #RE./,'#RE PRESCRIB. SST (Sea Surface Temperature/Reynolds DATA Set)',
 
     ./,'#SN SNOW Model                               may be turned ON',
c #AB./,'#AB SNOW Model: Interactive Albedo f(Grain) (Brun et al.1991)',
c #AG./,'#AG SNOW Model: Snow Aging Col de Porte     (Brun et al.1991)',
     ./,'#CZ SNOW Model: Zenithal Angle Correction  (Segal et al.1991)',
c #DG./,'#DG SNOW Model: Snow Settling when Melting | Minimum Density ',
c #Se./,'#Se SNOW Model: Energy Conserv. Verific.: Summary, Output    ',
c #SE./,'#SE SNOW Model: Energy Conserv. Verific.: Summary, Output+   ',
c #SF./,'#SF SNOW Model: Energy Conserv. Verific.: Forcing, Conduction',
c #SW./,'#SW SNOW Model: Water  Conserv. Verific.: Melting, Freezing  ',
c #HS./,'#HS SNOW Model: Hardened SNOW Pack Initialization            ',
c #MA./,'#MA SNOW Model: Increased polar B* Mobility (Mann et al.2000)',
c #NP./,'#NP SNOW Model: Fallen Snow Density = f(V)  (Kotlyakov, 1961)',
     ./,'#SD SNOW Model: Antarct.,Fallen Snow Density (NP must be OFF)',
c #RU./,'#RU SNOW Model: Slush:  Internal Run OFF of Water Excess     ',
c #GK./,'#GK SNOW Model: Interactive Albedo (Greuell &Konzelmann 1994)',
c #SL./,'#SL SNOW Model: Interactive Albedo (Zuo     &Oerlemans  1995)',
c #SM./,'#SM SNOW Model: Melting/Freezing Diagnostics                 ',
c #SZ./,'#SZ SNOW Model: Z0 Dependance on varying Sastrugi Height     ',
     ./,'#TZ SNOW Model: Z0 (Momentum) (typical value in polar models)',
c #CP./,'#CP SNOW Model: For Validation on Col de Porte Data          ',
     ./,'#GL SNOW Model: ETH-Camp & Greenland 3D simulations          ',
 
c #PP./,'#PP PROJECTION: Polar Stereographic Projection               ',
 
     ./,'#TV Soil /Vegetation Variables                  are used     ',
     ./,'#GP Soil /Vegetation Model: LAI, GLF Variations NOT prescrib.',
c #LN./,'#LN Soil /Vegetation Model: LAI(x,y,t) prescribed(MARglf.DAT)',
c #SV./,'#SV Soil /Vegetation Model (Koen De Ridder)  may be turned ON',
c #SH./,'#SH Soil /Vegetation Model: Hapex-Sahel   Vegetation     DATA',
c #V1./,'#V1 Soil /Vegetation Model: (KD) Vegetat. IGBP Classification',
c #V2./,'#V2 Soil /Vegetation Model: (KD) Vegetat. MAR  Classification',
 
c #GA./,'#GA SISVAT: Soil Humidity Geometric Average at Layer Interfac',
c #GF./,'#GF SISVAT: Gravitational Saturation Front          turned ON',
c #GH./,'#GH SISVAT: Gravitational Saturation Front - Horton turned ON',
c #OP./,'#OP SISVAT: Interactive Sea Surface Temperature     turned ON',
c #op./,'#op SISVAT: SST Nudging -->   prescribed values     turned ON',
     ./,'#IP SISVAT: Sea-Ice Fraction prescribed from SMMR and SSM/I  ',
     ./,'#SI SISVAT: Sea-Ice Fraction calculated from prescribed SST  ',
c #IA./,'#IA SISVAT: Sea-Ice Bottom   accretion  and  ocean cooling   ',
c #MT./,'#MT SISVAT: Monin-Obukhov Theory is linearized (Garrat schem)',
c #SR./,'#SR SISVAT: traces & OUTPUT a variable among called routines ',
c #WV./,'#WV SISVAT: performs OUTPUT on an ASCII File (1 file each pt)',
     ./,'#sa SISVAT: must be pre-processed, except for stand-alone run',
 
c #CS./,'#CS  INPUT: Constant Sounding during 1st Hours     (2-D ONLY)',
 
     ./,'#IB OUTPUT: Ice-Sheet Surface Mass Balance  (on MARphy File )',
     ./,'#ID OUTPUT: Main Dependant Variables        (on NetCDF File )',
     ./,'#UL OUTPUT: Time Dimension is UNLIMITED     (on NetCDF File )',
c #T2./,'#T2 OUTPUT: 2-m  Air Temperature            (on NetCDF File )',
c #W6./,'#W6 OUTPUT, Additional: Simulation Statistics      on MAR.log',
c #w6./,'#w6 OUTPUT, Additional: Simulation Statistics (NH) on MAR.log',
c #WA./,'#WA OUTPUT, Additional:                            DYNadv_ver',
c #WR./,'#WR OUTPUT, Additional: INIsnd, infra, SRFmod_sno, SRFmod_pol',
 
     ./,'#vL PORTABILITY: Vectorization enhanced                      ',
     ./,'#vN PORTABILITY: Vectorization enhanced: Leap Frog Counter   ',
     ./,'#vK PORTABILITY: Vectorization enhanced: TKE                 ',
     ./,'#vH PORTABILITY: Vectorization enhanced: Hydrological Cycle  ',
c #vB./,'#vB PORTABILITY: Vectorization enhanced: Blowing  Snow *     ',
c #vD./,'#vD PORTABILITY: Vectorization enhanced: Blowing  Dust .     ',
c #vR./,'#vR PORTABILITY: Vectorization enhanced: Sastrugi Height     ',
     ./,'#vS PORTABILITY: Vectorization enhanced: Snow     Model      ',
     ./,'#vV PORTABILITY: Vectorization enhanced: SVAT                ',
     ./,'#vZ PORTABILITY: Vectorization enhanced: Av.Roughness Length ',
     ./,'#HP PORTABILITY: Enables use of own    library on Linux Syst.',
     ./,'#NV PORTABILITY: Vectorization  is     turned  OFF           ',
     .   1x                                                            )
 
       write(21,602)
     .   reaVAR,swich(-zext(reaVAR)),reaLBC,swich(-zext(reaLBC)),
     .   safVAR,swich(-zext(safVAR)),hamfil,swich(-zext(hamfil))
 602   format(//,' OPTIONS',/,' +++++++',/,
     &     /,'  reaVAR=',l2,4x,' => Input: Prev.Dyn.Sim.(MAR/GCM) ',a3,
     &     /,'  reaLBC=',l2,4x,' => LBC:   Prev.Dyn.Sim.(MAR/GCM) ',a3,
     &     /,'  safVAR=',l2,4x,' => Saving    on Files MARxxx.DAT ',a3,
     &     /,'  hamfil=',l2,4x,' => Diabatic Initialisation       ',a3)
c #HF       if (hamfil) write(21,603) hham,nham,hhac,thac
  603  format(
     &       '  Hamming Filter Characteristics:',
     &     /,'   Time       =',f11.4,  '=> N(Hamming)= ',i12,
     &     /,'   Cutoff     =',f11.4,  '=> Frequency = ',f12.4,/,1x)
C +
       rhcrit = 0.0
       tstart = 0.0
       rhcrit = rhcrHY
       tstart = tim_HY
       write(21,604)
     .   conmas,swich(-zext(conmas)),potvor,swich(-zext(potvor)),
     .   brocam,swich(-zext(brocam)),turhor,swich(-zext(turhor)),
     .   convec,swich(-zext(convec)),
     .   micphy,swich(-zext(micphy)),1.d+2*rhcrit,tstart,
     .   fracld,swich(-zext(fracld)),
     .   chimod,swich(-zext(chimod)),
     .   physic,swich(-zext(physic)),
     .   snomod,swich(-zext(snomod)),polmod,swich(-zext(polmod)),fxlead,
     .   vegmod,swich(-zext(vegmod)),qsolSL,swich(-zext(qsolSL)),
     .   rxbase,rxfact
 604   format(
     &       '  conmas=',l2,4x,' => Mass  Conservation Constraint ',a3,
     &     /,'  potvor=',l2,4x,' => PV    Conservation Constraint ',a3,
     &     /,'  brocam=',l2,4x,' => Brown and Campana Time Scheme ',a3,
     &     /,'  turhor=',l2,4x,' => Horizontal Diffusion          ',a3,
     &     /,'  convec=',l2,4x,' => Mass Flux convective Scheme   ',a3,
     &     /,'  micphy=',l2,4x,' => Cloud Microphysics            ',a3,
     &     /,'  rhcrHY=',f6.0, ' %  Critical Relative Humidity Value  ',
     &     /,'  tim_HY=',f6.0, '    Cloud Microphysics Starting Time  ',
     &     /,'  fracld=',l2,4x,' => Fractional Cloudiness Scheme  ',a3,
     &     /,'  chimod=',l2,4x,' => Chemical Atmospheric  Model   ',a3,
     &     /,'  physic=',l2,4x,' => Atmosphere / Surface  Physics ',a3,
     &     /,'  snomod=',l2,4x,' => Interactive Snow      Model   ',a3,
     &     /,'  polmod=',l2,4x,' => Interactive Polynya   Model   ',a3,
     &     /,'  fxlead=',f5.2,'     Initial Minimal Lead   Fraction    ',
     &     /,'  vegmod=',l2,4x,' => Interactive SVAT      Model   ',a3,
     &     /,'  qsolSL=',l2,4x,' => Soil Humidity         Model   ',a3,
     &     /,'  rxbase=',f6.3, '    Nudging Coeff.(Anthes et al. 1989)',
     &     /,'  rxfact=',f6.1, '    Lateral Sponge Coefficient   (A89)')
C +
       write(21,605) dx,mx,dy,my,
     .               dt    ,dtfast,ntFast,
     .               center,nordps,staggr,
     .               dtRadi,jtRadi,ntRadi,
     .               dtPhys,jtPhys,ntPhys,
     .               dtDiff,jtDiff,ntDiff,
     .               FIslot,FIkhmn,TUkhff,TUkhmx,FIslou,FIslop
  605  format(//,' MAR DISCRETISATION',/,' ++++++++++++++++++',
     & //,'  dx         =',f8.1,' m   /  Nb Points : ',i12,
     &  /,'  dy         =',f8.1,' m   /  Nb Points : ',i12,
     &  /,'  dt         =',f8.1,' sec',
     &  /,'  dt Lamb    =',f8.1,' sec',
     &  /,'  nt Lamb    =',i6,
     &  /,'  p* Discret.=',l6,5x,  '  /  p* Precis.= ',i12,
     &  /,'  Vert.Stagg.=',l6,
     & //,'  dt Sol./IR =',f8.1,' sec => jt Sol./IR= ',i12,
     &  /,'                              nt Sol./IR= ',i12,
     & //,'  dt Surface =',f8.1,' sec => jt Surface= ',i12,
     &  /,'                              nt Surface= ',i12,
     &  /,                             ' CAUTION: := dt',
     & //,'  dt Turbul. =',f8.1,' sec => jt Turbul.= ',i12,
     &  /,'                           /  nt Turbul.= ',i12,
     &  /,                             ' CAUTION: := dt',
     & //,'  delta T    =',f11.4,  '  => Kh(delta) = ',e12.4,
     &  /,'              ', 11x ,  '  -- fac. (Kh) = ',f12.4,
     &  /,'  Absorbing Layer          -> Kh max    = ',e12.4,
     &  /,'  delta u    =',f11.4,
     &  /,'  delta p    =',f11.4)
C +
C +
C +   ------
      end if
C +   ------
C +
C +
C +--WARNINGS
C +  ========
C +
C +--Time Step
C +  ---------
C +
c     if (abs(dt-dtdom).gt.epsi) write(6,2) dt,dtdom
c2    format(/,' ***********************************',
c    .          '********************************',
c    .       /,' * CAUTION: dt(MARctr.dat)=',f8.2,'s',
c    .          ' .ne. dt(MARdom.dat)=',f8.2,'s *',
c    .       /,' ***********************************',
c    .          '********************************',/,1x)
C +
C +
C +--Topography
C +  ----------
C +
                                                 lo_CAU =  0
      IF (n7mxLB.gt.1)                                            THEN
        DO j=1,my
        DO i=1,n7mxLB-1
          if (abs(sh(i,j)-sh(ip1(i),j)).gt.epsi) lo_CAU =  1
        END DO
        END DO
      END IF
C +
      IF (n6mxLB.gt.0)                                            THEN
        DO j=1,my
        DO i=mx-n6mxLB+1,mx
          if (abs(sh(i,j)-sh(im1(i),j)).gt.epsi) lo_CAU =  1
        END DO
        END DO
      END IF
C +
      IF (n7myLB.gt.1)                                            THEN
        DO j=1,n7myLB-1
        DO i=1,mx
          if (abs(sh(i,j)-sh(i,jp1(j))).gt.epsi) lo_CAU =  1
        END DO
        END DO
      END IF
C +
      IF (n6myLB.gt.0)                                            THEN
        DO i=1,mx
        DO j=my-n6myLB+1,my
          if (abs(sh(i,j)-sh(i,jm1(j))).gt.epsi) lo_CAU =  1
        END DO
        END DO
      END IF
C +
       if (lo_CAU.eq.1)
     . write(6,1)
 1     format(' ***********************************',
     .         '*******************************',
     .      /,' * CAUTION: Lateral Sponge too large',
     .        ' OR Lateral Plateau too small *',
     .      /,' ***********************************',
     .         '*******************************',/,1x)
 
 
C ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C +++ INITIALISATION INCLUDING A SAVED STATE OF THE VARIABLES ++++++++++++++
C ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 
 
C +   +++++++++++                                                 ++++
      IF (reaVAR)                                                 THEN
C +   +++++++++++                                                 ++++
 
 
C +--Timing
C +  ======
 
          iterun = 0
 
 
C +--Dynamics
C +  ========
 
          open (unit=11,status='old',form='unformatted',
     .                               file='MARdyn.DAT')
          rewind     11
c         read      (11) itexpe2,jdh_LB ; itexpe=itexpe2  ! if itexpe is integer*8 but file in integer*4
          read      (11) itexpe ,jdh_LB                   ! if itexpe is integer*4/*8
c In the coupled MAR/ice sheet model simulation, itexpe must be an integer*8 in MARCTR.inc
c If you started your simulation with an interger*4, you need only 1 time to use 
c the "itexpe2" line to read an integer*4 but to save an integer*8 for the next simulation. 
c Afterwards, you have to use the "itexpe" line to read/write an interger*8 

          read      (11) iyrDYN,mmaDYN,jdaDYN,jhuDYN
C +...    Time    Parameters
 
C +--Modified Time Step (BEGIN)
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~
          YYtmp=-1 ; MMtmp=-1 ; DDtmp=-1 ; dt_old=-1 ; dt_new=-1
          open  (unit=51,status='old',file='MARtime.ctr',err=51)
          read  (51,*,err=51) YYtmp,MMtmp,DDtmp,dt_old,dt_new
 51       continue
          close (51)
 
          if( YYtmp .eq. iyrDYN .and. MMtmp .eq. mmaDYN .and.
     .        DDtmp .eq. jdaDYN .and.
     .        dt_old.gt. 0      .and. dt_new.gt. 0) then
           write(6,*) ' '
           write(6,*) 'WARNING--WARNING--WARING--WARING--WARNING'
           write(6,*) 'itexpe modified by MARtime.ctr'
           write(6,*) 'dt_old =',dt_old
           write(6,*) 'dt_new =',dt_new
           write(6,*) 'WARNING--WARNING--WARING--WARING--WARNING'
           write(6,*) ' '
           itexpe = (itexpe*dt_old)/dt_new
          endif
C +--Modified Time Step   (END)
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~
 
          read      (11) imezdy,jmezdy
          read      (11) aladyn,alodyn
C +...    Spatial Parameters
 
          read      (11) sigma,ptopDY,dx,dy
C +...    Discretisation
 
          read      (11) uairDY
          read      (11) vairDY
          read      (11) pktaDY
          read      (11) pstDY
          read      (11) qvDY
C +...    Dynamics
 
          read      (11) sh
 
c         MAR-GRISLI coupling
          call ice_sheet_model_coupling
 
          read      (11) pstDY1
 
          read      (11) iyr_LB,mma_LB,jda_LB,jhu_LB,jdh_LB
          read      (11) vaxgLB,vaxdLB,vayiLB,vaysLB
C +...    Lateral Boundary Conditions
 
          read      (11) sst_LB
 
          read      (11) uairUB,vairUB,pktaUB
C +...    Upper   Sponge   Reference State
 
          IF (jdh_LB.le.0)                                        THEN
c           write(6,6000)jdh_LB
 6000       format(/,'############################################',
     .             /,'# CAUTION: previous jdh_LB =',i6,' set:= 1 #',
     .             /,'############################################',/)
 
                         jdh_LB = 1
          END IF
 
          IF (itexpe.GT.0)                                        THEN
              read  (11) pstDYn
              read  (11) RAd_ir
              read  (11) IRsoil
              read  (11)  virDY
              read  (11) tim1LB,v1xgLB,v1xdLB,v1yiLB,v1ysLB
              read  (11) tim2LB,v2xgLB,v2xdLB,v2yiLB,v2ysLB
              read  (11) sst1LB,sst2LB
              read  (11) ua1_UB,ua2_UB
              read  (11) va1_UB,va2_UB
              read  (11) pkt1UB,pkt2UB
            IF (  my.EQ.1)                                        THEN
              read  (11) ugeoDY
              read  (11) vgeoDY
c #PV       stop '    ?!&~@|@[#@#  PV not conserved! EMERGENCY STOP'
            END IF
          END IF
 
          close(unit=11)
 
 
C +--sigma-levels Height
C +  -------------------
 
          DO k=1,mz
             zsigma(k) =-(sst_SL / 0.0065) *   ((1.e0+(sigma(k)     -1.)
     .                                         *(1.e2/ps_sig))
     .                                     ** (RDryAi*0.0065/gravit)-1.)
          END DO
 
          IF (IO_gen.ge.2)                                          THEN
            write(21,130)( sigma(k),k=1,mz)
            write(21,131)(zsigma(k),k=1,mz)
          END IF
 
 
C +--Auxiliary Grid Parameters
C +  -------------------------
 
C +       ***********
          call GRDmar
C +       ***********
 
 
C +--Geographical Coordinates
C +  ------------------------
 
C +       ***********
          call GRDgeo
C +       ***********
 
 
C +--Local Time of the Model Center
C +  ------------------------------
 
C +       ***********
          call TIMcur
          call TIMgeo
C +       ***********
 
 
C +--LBC Coefficients
C +  ----------------
 
          IF (itexpe.EQ.0)                                        THEN
          tim1LB=ou2sGE(iyr_LB,mma_LB,jda_LB,jhu_LB,0,0)
          tim2LB=   tim1LB
 
          DO   iv_ini=1,5
            DO i=1,n7mxLB
            DO k=1,mz
            DO j=1,my
              v1xgLB(i,j,k,iv_ini) = vaxgLB(i,j,k,iv_ini)
              v2xgLB(i,j,k,iv_ini) = vaxgLB(i,j,k,iv_ini)
            END DO
            END DO
            END DO
 
            DO i=mx-n6mxLB,mx
            DO k=1,mz
            DO j=1,my
              v1xdLB(i,j,k,iv_ini) = vaxdLB(i,j,k,iv_ini)
              v2xdLB(i,j,k,iv_ini) = vaxdLB(i,j,k,iv_ini)
            END DO
            END DO
            END DO
 
            DO j=1,n7myLB
            DO k=1,mz
            DO i=1,mx
              v1yiLB(i,j,k,iv_ini) = vayiLB(i,j,k,iv_ini)
              v2yiLB(i,j,k,iv_ini) = vayiLB(i,j,k,iv_ini)
            END DO
            END DO
            END DO
 
            DO j=my-n6myLB,my
            DO k=1,mz
            DO i=1,mx
              v1ysLB(i,j,k,iv_ini) = vaysLB(i,j,k,iv_ini)
              v2ysLB(i,j,k,iv_ini) = vaysLB(i,j,k,iv_ini)
            END DO
            END DO
            END DO
          END DO
          END IF
 
C +       ***************
          call LBCnud_ini
C +       ***************
 
C +       ***************
          call LBCnud_par
C +       ***************
 
 
C +--Soil Model
C +  ==========
 
          open (unit=11,status='old',form='unformatted',
     .                               file='MARsol.DAT')
          rewind     11
          read      (11) itever
          read      (11) iyrSOL,mmaSOL,jdaSOL,jhuSOL
          if (itever.ne.itexpe.or.
     .        iyrSOL.ne.iyrDYN.or.
     .        mmaSOL.ne.mmaDYN.or.
     .        jdaSOL.ne.jdaDYN.or.
     .        jhuSOL.ne.jhuDYN)
     .     write(6,817)
 817       format(' ++WARNING++ MARsol improperly specified ')
 
          read      (11) nSLsrf
          read      (11) SLsrfl
          read      (11) TairSL
          read      (11) tsrfSL
          read      (11) alb0SL,eps0SL
          read      (11) SaltSL
          read      (11) ro_SL0
          read      (11) ro_SL
          read      (11) d1_SL
          read      (11) t2_SL
          read      (11) w2_SL,wg_SL
          read      (11) roseSL
          read      (11) qvapSL
          read      (11) hsnoSL
          read      (11) hmelSL
 
          read      (11) SLuusl,SL_z0
          read      (11) SLutsl,SL_r0
 
          IF (itexpe.GT.0)                                        THEN
            read    (11) pktaSL
            read    (11) sicsIB
            read    (11) sic1sI,sic2sI
            read    (11) albeSL
            read    (11) SLuus ,SLuts
            read    (11) SLuqs ,SLuqsl
            read    (11) duusSL
            read    (11) dutsSL
            read    (11) cdmSL ,cdhSL
            read    (11) V_0aSL
            read    (11) dT0aSL
c #AM       read    (11) u_0aSL
c #AT       read    (11) uT0aSL
c #AS       read    (11) us0aSL
c #VX       read    (11) WV__SL
            read    (11) SLlmo ,SLlmol
c #BV       read    (11)  virSL
          END IF
 
 
 
 
          close(unit=11)
 
 
          IF (itexpe.eq.0)                                          THEN
            open (unit=11,status='old',form='unformatted',
     .            file='MARsic.DAT')
            rewind     11
            read      (11) iyr_sI,mma_sI,jda_sI,jhu_sI,jdh_sI
            read      (11) sicsIB
            close(unit=11)
          END IF
 
 
C +--Update of Soil Parameters
C +  -------------------------
 
                Tfr_LB = tfrwat
c #RE           Tfr_LB = tfrwat +0.15 +epsi
 
          DO j=1,my
          DO i=1,mx
 
          IF   (isolSL(i,j).le.2)                                 THEN
 
C +---1. Open Water
C +   ~~~~~~~~~~~~~
            IF (sst_LB(i,j).ge. Tfr_LB)                           THEN
                isolSL(i,j)   = 1
                 d1_SL(i,j)   = 2.09e+8
                albeSL(i,j)   = 0.10
                eps0SL(i,j)   = 0.97
                 SL_z0(i,j,1) =          zs_SL
                 SL_r0(i,j,1) = 0.1     *zs_SL
                 ch0SL(i,j)   = 0.00132
                rsurSL(i,j)   = 0.0
 
C +---2. Sea Ice
C +   ~~~~~~~~~~
            ELSE
                isolSL(i,j)   = 2
                 d1_SL(i,j)   = 1.05e+5
                albeSL(i,j)   = 0.70
                eps0SL(i,j)   = 0.97
                 SL_z0(i,j,1) =          zn_SL
                 SL_r0(i,j,1) = 0.1     *zn_SL
                 ch0SL(i,j)   = 0.0021
C +...          (Kondo and Yamazaki, 1990, JAM 29, p.376)
                rsurSL(i,j)   = 0.0
            END IF
          END IF
 
          END DO
          END DO
 
 
C +--SVAT Model
C +  ==========
 
        IF (vegmod)                                               THEN
 
C +       ***********
          call SVAsav('read')
C +       ***********
 
        END IF
 
 
C +--Ocean Model
C +  ===========
!AO_CK 20/02/2020
C +--cpl Get fields from oasis initialisation files
C +  ----------------------------------------------
 
c #AO il_time_secs = 0                         !temps depuis le debut du run
c #AO                                          ! (au pas de temps precedant)
 
C +--read fields from NEMO at 1st time step of this run
C +  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
c #AO write(6,*) '     CALL fromcpl, itexpe =', itexpe
c #AO write(6,*) '     CALL fromcpl, il_time_secs =', il_time_secs
 
c #AO      CALL fromcpl(il_time_secs, srftAO(:,:,1),aoss,
c #AO.     sicsAO,aogla,albAO(:,:,2),aoalb, srftAO(:,:,2),aotic,
c #AO.     hicAO,aohic, hsnoAO, aohsn,UoceAO,ao_uo, VoceAO,ao_vo, 
c #AO.                  UiceAO,ao_ui, ViceAO,ao_vi)
C +        *******
 
c #AO tocken_AO=1       !not to call fromcpl further at the first time step
 
!No need to check if fields have been updated as they came from restart files (=update)
c #AO  print *, "AO/coupling: warning on your first restart files"
c #AO  do j=1,my; do i=1,mx

C +--NEMO temperature
c #AO  	if ( weightao_sst(i,j) .ne. 1) then	  
c #AO	 srftAO(i,j,1) = min(max(srftAO(i,j,1)/(1-sicsAO(i,j))
c #AO.                       ,271.01),300.15)
c #AO      sst_LB(i,j)= (1.-weightao_sst(i,j))*srftAO(i,j,1)
c #AO.                  +(weightAO_sst(i,j)*sst_LB(i,j))
c #AO   endif

C +--NEMO SIC
c #AO   if (weightao_sic(i,j) .ne. 1) then
c #AO    sicsIB(i,j) = (1.-weightao_sic(i,j))* sicsAO(i,j)
c #AO.               + (weightAO_sic(i,j)*sicsIB(i,j))
c #AO   endif
c #AO  end do; end do

C +--NEMO other parameters:
! snow and sea ice thickness, albedo
!=> not coupled at ini of MAR to increase MAR stability
!(one thing at a time) #CK
!SST/ Sea ice Surface temperature cf below

 
C +--Open Water Albedo
C +  -----------------
 
c #AO IF (itexpe.eq.0)  THEN
 
c #AO  DO i=1,mx
c #AO  DO j=1,my
c #AO    albAO(i,j,1)=0.066 !open water albedo prescribed at first time step
c #AO                       ! clear sky value in flx_core.h90 (NEMO)
c #AO  ENDDO
c #AO  ENDDO
 
c #AO ELSE
 !ck not sure that is still needed
c #AO  DO i=1,mx
c #AO  DO j=1,my
c #AO    IF (sicsIB(i,j).eq.0)  THEN
c #AO            albAO(i,j,1)= albeSL(i,j)  !where no sea ice, albedo begin with
c #AO    ELSE                               !value (taking into account solar ze
c #AO            albAO(i,j,1)= 0.066        !cloud cover); elsewhere, albedo ove
c #AO    ENDIF                              !and albedo over ice is NEMO's albed
c #AO  ENDDO                                !by sending solar zenith angle and c
c #AO  ENDDO                                !so that it computes himself ocean a
 
c #AO ENDIF
 
c #AO  do j=1,my
c #AO  do i=1,mx
c #AO    if (isolSL(i,j).le.2) then
c #AO        albsSL(i,j) =     albAO(i,j,1) * (1. - sicsAO(i,j))
c #AO.                     +   albAO(i,j,2) *       sicsAO(i,j)
c #AO        albeSL(i,j) =  albsSL(i,j)
c #AO    endif
c #AO  end do
c #AO  end do
  
c #AO  DO i=1,mx
c #AO  DO j=1,my
c #AO    IF (isolSL(i,j).le.2)  THEN
c #AO        maskSL(i,j) = 1  !ocean/sea-ice
	   
c #AO      DO n=1,2
	   
c #AO	    if (i .eq. 1  ) tsrfSL(i,j,n)=tairDY(i,j,mz) !to be sure that they have a value => tairdy(i,j,mz)
c #AO       if (j .eq. 1  ) tsrfSL(i,j,n)=tairDY(i,j,mz) 
c #AO	    if (i .eq. mx ) tsrfSL(i,j,n)=tairDY(i,j,mz)
c #AO	    if (j .eq. my ) tsrfSL(i,j,n)=tairDY(i,j,mz)

c #AO	     tsrfSL(i,j,n)=   srftAO(i,j,n)*(1.-weightao_st(i,j))
c #AO.                     + (weightAO_st(i,j)*tsrfSL(i,j,n))
c #AO
c #AO        DO isl =   -nsol,0
c #AO	     if (n .eq. 1) then !ocean
c #AO	     TsolTV(i,j,n,1-isl) =  tsrfSL(i,j,n)   ! Prescribed   SST   
c #AO	     SLsrfl(i,j,n) = 1.-sicsIB(i,j)
c #AO	     endif
	     
c #AO	     if (n .eq. 2) then !sea ice
c #AO	     TsolTV(i,j,n,1-isl) = 271.2       !Prescribed SST beneath Ice
c #AO	     SLsrfl(i,j,n) = sicsIB(i,j)
c #AO	     endif

c #AO        ENDDO 
	     
c #AO       ENDDO
	
c #AO    ELSE
c #AO        maskSL(i,j) = 0  !land
c #AO    END IF

c #AO     ENDDO
c #AO     ENDDO
      
c #AO 
C +    ================                                           ====
       IF (itexpe.gt.0)                                           THEN
C +    ================                                           ====
C +
C +
C +--Non-Hydrostatic Dynamics
C +  ========================
C +
c #NH        open (unit=11,status='unknown',form='unformatted',
c #NH.             file='MARonh.DAT')
c #NH        rewind     11
c #NH        read      (11) itever
c #NH        read      (11) iyrONH,mmaONH,jdaONH,jhuONH
c #NH      IF (itever.ne.itexpe.or.
c #NH.         iyrONH.ne.iyrDYN.or.
c #NH.         mmaONH.ne.mmaDYN.or.
c #NH.         jdaONH.ne.jdaDYN.or.
c #NH.         jhuONH.ne.jhuDYN)
c #NH.       write(6,810)
 810         format(' ++WARNING++ MARonh improperly specified ')
C +...       Time    Parameters
C +
c #NH        read      (11) ua0_NH
c #NH        read      (11) va0_NH
c #NH        read      (11) wa0_NH
c #NH        read      (11) wairNH
c #NH        read      (11) pairNH
C +...       Dynamics
C +
c #NH        close(unit=11)
C +
C +
C +--Mass Flux convective Scheme
C +  ===========================
C +
         IF (convec)                                              THEN
           open (unit=11,status='old',form='unformatted',
     .                                file='MARcva.DAT')
           rewind     11
           read      (11) itever
           read      (11) iyrCVA,mmaCVA,jdaCVA,jhuCVA
           IF (itever.ne.itexpe.or.
     .         iyrCVA.ne.iyrDYN.or.
     .         mmaCVA.ne.mmaDYN.or.
     .         jdaCVA.ne.jdaDYN.or.
     .         jhuCVA.ne.jhuDYN)
     .       write(6,811)
 811         format(' ++WARNING++ MARcva improperly specified ')
C +...       Time    Parameters
C +
             read      (11) adj_CA
             read      (11) int_CA
             read      (11) dpktCA
             read      (11) dqv_CA
             read      (11) dqw_CA
             read      (11) dqi_CA
             read      (11) drr_CA
             read      (11) dss_CA
             read      (11) dsn_CA
             read      (11) rainCA
             read      (11) snowCA
             read      (11) tau_CA
C +
             read      (11) Kstep1
             read      (11) K_CbT1
             read      (11) K_CbB1
             read      (11) P_CH_0
             read      (11) PdCH_1
             read      (11) PdTa_1
             read      (11) PdQa_1
             read      (11) PdQw_1
             read      (11) PdQi_1
             read      (11) Pdrr_1
             read      (11) Pdss_1
             read      (11) PuMF_1
             read      (11) PdMF_1
             read      (11) Pfrr_1
             read      (11) Pfss_1
             read      (11) Pcape1
C +
         END IF
C +
C +
C +--Microphysics
C +  ============
C +
         IF (micphy)                                              THEN
C +
           open (unit=11,status='old',form='unformatted',
     .                                file='MARcld.DAT')
           rewind     11
           read      (11) itever
           read      (11) iyrHYD,mmaHYD,jdaHYD,jhuHYD
           IF (itever.ne.itexpe.or.
     .         iyrHYD.ne.iyrDYN.or.
     .         mmaHYD.ne.mmaDYN.or.
     .         jdaHYD.ne.jdaDYN.or.
     .         jhuHYD.ne.jhuDYN)
     .       write(6,812)
 812         format(' ++WARNING++ MARcld improperly specified ')
C +
           read      (11) turnHY
           read      (11) ccniHY
           read      (11) qiHY
           read      (11) qsHY
C +HG      read      (11) qgHY
           read      (11) qwHY
           read      (11) qrHY
           read      (11) rainHY,rai0HY
           read      (11) snowHY,sno0HY,sfa0HY
           read      (11) crysHY
           read      (11) rainCA
c #BS      read      (11) uss_HY
C +
           close(unit=11)
C +
         END IF
C +
C +
C +--Atmospheric Tracers
C +  ===================
C +
c #TC      open (unit=11,status='old',form='unformatted',
c #TC.                                file='MARtca.DAT')
c #TC      rewind     11
c #TC      read      (11) itever
c #TC      read      (11) iyrTCA,mmaTCA,jdaTCA,jhuTCA
c #TC      read      (11) dt_ODE,dt2ODE,nt_ODE,jt_ODE
c #TC      IF (itever.ne.itexpe.or.
c #TC.         iyrTCA.ne.iyrDYN.or.
c #TC.         mmaTCA.ne.mmaDYN.or.
c #TC.         jdaTCA.ne.jdaDYN.or.
c #TC.         jhuTCA.ne.jhuDYN)
c #TC.       write(6,813)
 813         format(' ++WARNING++ MARtca improperly specified ')
C +
c #TC      read      (11) qxTC
c #TC      read      (11) qsTC
c #TC      read      (11) uqTC
C +
c #TC      close(unit=11)
C +
C +
C +--Polynya Model
C +  =============
C +
c #PO    IF (polmod)                                              THEN
C +
c #PO      open (unit=11,status='old',form='unformatted',
c #PO.                                file='MARpol.DAT')
c #PO      rewind     11
c #PO      read      (11) itever
c #PO      read      (11) iyrPOL,mmaPOL,jdaPOL,jhuPOL
c #PO      if (itever.ne.itexpe.or.
c #PO.         iyrPOL.ne.iyrDYN.or.
c #PO.         mmaPOL.ne.mmaDYN.or.
c #PO.         jdaPOL.ne.jdaDYN.or.
c #PO.         jhuPOL.ne.jhuDYN)
c #PO.       write(6,814)
 814         format(' ++WARNING++ MARpol improperly specified ')
C +
c #PO      read      (11) isolSL
c #PO      read      (11) iPO1,iPO2,jPO1,jPO2,iPO3,iPO4,jPO3,jPO4
c #PO      read      (11) hfraPO,vgriPO,uocnPO,vocnPO,swsaPO,focnPO
c #PO      read      (11) silfPO,hicePO,aicePO,uicePO,vicePO,dtPO
C +
c #PO      close(unit=11)
C +
c #PO    END IF
C +
C +
C +--Snow Model
C +  ==========
C +
         IF (snomod.AND..NOT.VSISVAT)                             THEN
C +
           open (unit=11,status='old',form='unformatted',
     .                                file='MARsno.DAT')
           rewind     11
c #sn      read      (11) itever,ntSNo ,ntwaSN
c #sn      read      (11)        dtSNo ,dtwaSN
c #sn      read      (11) iyrSNO,mmaSNO,jdaSNO,jhuSNO
c #sn      IF (itever.ne.itexpe.or.
c #sn.         iyrSNO.ne.iyrDYN.or.
c #sn.         mmaSNO.ne.mmaDYN.or.
c #sn.         jdaSNO.ne.jdaDYN.or.
c #sn.         jhuSNO.ne.jhuDYN)
c #sn.       write(6,815)
 815         format(' ++WARNING++ MARsno improperly specified ')
C +
c #sn      read      (11) agSNow
c #sn      read      (11) tiSNow
c #sn      read      (11) waSNow
c #sn      read      (11) roSNow
c #sn      read      (11) dzSNow
c #sn      read      (11) g1SNow
c #sn      read      (11) g2SNow
c #sn      read      (11) nhSNow
c #sn      read      (11) nsSNow
c #sn      read      (11) niSNow
c #sn      read      (11) smbalSN0
c #sn      read      (11) znSNow0
c #sn      read      (11) maskSN
c #sn      read      (11) slopSN
c #sn      read      (11) waSNru
c #BS      read      (11) FacRBS,FacSBS,FacTBS,FacUBS,
c #BS.                    g1__BS,g2__BS,sheaBS
C +
           close(unit=11)
C +
         END IF
C +
C +
C +--Turbulence
C +  ==========
C +
           open (unit=11,status='old',form='unformatted',
     .                                file='MARtur.DAT')
           rewind     11
           read      (11) itever
           read      (11) iyrTUR,mmaTUR,jdaTUR,jhuTUR
           if (itever.ne.itexpe.or.
     .         iyrTUR.ne.iyrDYN.or.
     .         mmaTUR.ne.mmaDYN.or.
     .         jdaTUR.ne.jdaDYN.or.
     .         jhuTUR.ne.jhuDYN)
     .      write(6,816)
 816        format(' ++WARNING++ MARtur improperly specified ')
C +
           read      (11) ect_TE
           read      (11) eps_TE
           read      (11) tranTE
C +...     TURBULENT KINETIC ENERGY (TKE) and DISSIPATION (e)
C +
           read      (11) TUkvm
           read      (11) TUkvh
C +...     TURBULENT DIFFUSION COEFFICIENT
C +
           close(unit=11)
C +
C +

C *AJT*         
           inquire( file='MARbio.DAT', exist=res )
           if (res .eq. .true.) then 
           write(6,*) 'Reading MARbio.DAT'
           open (unit=11,status='old',form='unformatted',
     .                                 file='MARbio.DAT')
           rewind     11
           read      (11) biomass
C          Print to stdout           
C           write(6,*) biomass

           close(unit=11)
           else
           write(6,*) 'MARbio.DAT does not exist'
           biomass(1:mx,1:my) = 0
           endif


C *AJT*         
           inquire( file='MARwcr.DAT', exist=res )
           if (res .eq. .true.) then 
           write(6,*) 'Reading MARwcr.DAT'
           open (unit=11,status='old',form='unformatted',
     .                                 file='MARwcr.DAT')
           rewind     11
           read      (11) wcrhos
           read      (11) wctice

           close(unit=11)
           else
           write(6,*) 'MARwcr.DAT does not exist'
           DO jj=1,my
           DO ii=1,mx
             wcrhos(ii,jj,1:nice) = [350.,350.,430., 521., 596., 660., 
     .        715., 784., 847.]
           ENDDO
           ENDDO
           wctice(1:mx,1:my) = -1
           endif
   


C +    ====
       else
C +    ====
C +
C +
C +--Timing
C +  ======
C +
        itexpe = 0
        iterun = 0
C +
C +
C +--Atmosphere
C +  ==========
C +
        DO j=1,my
        DO i=1,mx
         pstDYn(i,j) = pstDY(i,j)
        END DO
        END DO
C +
 
C +          ******
        call DYNgpo_mp
C +          ******
 
C +
C +
C +--Microphysics and Surface
C +  ========================
C +
C +      ***********
         call INIphy
C +      ***********
C +
C +
C +--Water Vapor and Precipitation Loading
C +  -------------------------------------
C +
C +      ***********
         call DYNloa
C +      ***********
C +
C +
C +--Surface Albedo (set to underlaying Soil Albedo)
C +  --------------
C +
         DO j=1,my
         DO i=1,mx
           alb0SL(i,j) = max(albsSL(i,j),alb0SL(i,j))
           albeSL(i,j) = max(albsSL(i,j),albeSL(i,j))
         END DO
         END DO
C +
C +    ======
       end if
C +    ======
C +
C +   ++++
      else
C +   ++++
C +
C +
C ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C +++ INITIALISATION INCLUDING A SOUNDING ++++++++++++++++++++++++++++++++++
C ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C +
C +
C +--Timing
C +  ======
C +
        itexpe = 0
        iterun = 0
C +
C +
C +--Dynamics
C +  ========
C +
C +
C +--Vertical  Grid Initialisation
C +  -----------------------------
C +
C +                      ***********
        IF (.NOT.geoNST) call GRDsig(zmin_0,aavu_0,bbvu_0,ccvu_0,verti0)
C +                      ***********
C +
C +
C +--Auxiliary Grid Parameters
C +  -------------------------
C +
C +     ***********
        call GRDmar
C +     ***********
C +
C +
C +--Geographical Coordinates
C +  ------------------------
C +
C +     ***********
        call GRDgeo
C +     ***********
C +
C +
C +--Local Time of the Model Center
C +  ------------------------------
C +
C +     ***********
        call TIMcur
        call TIMgeo
C +     ***********
C +
C +
C +--Initialisation assuming horizontal Homogeneity
C +  ----------------------------------------------
C +
C +     ***********
        call INIsnd
C +     ***********
C +
C +     ***************
        call LBCnud_ini
C +     ***************
C +
C +     ***************
        call LBCnud_par
C +     ***************
C +
C +
C +--Leapfrog Auxiliary Variables
C +  ============================
C +
        DO j=1,my
        DO i=1,mx
         pstDYn(i,j) = pstDY(i,j)
        END DO
        END DO
C +
C +
C +--Microphysics and Surface
C +  ========================
C +
C +     ***********
        call INIphy
C +     ***********
C +
C +
C +--Water Vapor and Precipitation Loading
C +  -------------------------------------
C +
C +     ***********
        call DYNloa
C +     ***********
C +
C +
C +--Surface Albedo (set to underlaying Soil Albedo)
C +  --------------
C +
        DO j=1,my
        DO i=1,mx
           alb0SL(i,j) = max(albsSL(i,j),alb0SL(i,j))
           albeSL(i,j) = max(albsSL(i,j),albeSL(i,j))
        END DO
        END DO
C +
C +   ++++++
      end if
C +   ++++++
C +
C +
C ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C +++ AUXILIARY VARIABLES INITIALISATION +++++++++++++++++++++++++++++++++++
C ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C +
C +
C +--Parameters
C +  ==========
C +
      ipri = 0
C +... print parameter used in Model  Validation
C +                         on Linear Mountain Waves Simulations
C +
C +--Atmospheric variables
C +  =====================
C +
C +
C +--Leapfrog Auxiliary Variables
C +  ----------------------------
C +
      DO k=1,mz
      DO j=1,my
      DO i=1,mx
        ubefDY (i,j,k) =  uairDY(i,j,k)
        vbefDY (i,j,k) =  vairDY(i,j,k)
      END DO
      END DO
      END DO
C +
C +
C +--Surface Geopotential
C +  --------------------
C +
      DO j=1,my
      DO i=1,mx
        gplvDY(i,j,mzz) = gravit * sh(i,j)
      END DO
      END DO
C +
C +
C +--Exner Potential, Temperature and Level Geopotential
C +  ---------------------------------------------------
C +
C +            ******
          call DYNgpo_mp
C +            ******
C +
C +--Mid-Level Geopotential
C +  ----------------------
C +
           k=1
      DO j=1,my
      DO i=1,mx
        gpmiDY(i,j,k) = 0.5 *(3.5*gplvDY(i,j,  1)-0.5d0*gplvDY(i,j,2))
      END DO
      END DO
C +
      DO k=kp1(1),mzz
      DO j=1,my
      DO i=1,mx
        gpmiDY(i,j,k) = 0.5 *(    gplvDY(i,j,k-1)+      gplvDY(i,j,k))
      END DO
      END DO
      END DO
C +
         k=       mzz
      DO j=1,my
      DO i=1,mx
        gpmiDY(i,j,k) =(0.5 *     z__SBL         +          sh(i,j)  )
     .                *           gravit
      END DO
      END DO
C +
C +
C +--Vertical Temperature Gradient at Sponge Base
C +  --------------------------------------------
C +
          gradTz =     1.0e+3 *grvinv
      IF (mzabso.gt.1)                                             THEN
          mzabs1 = max(mzabso-1,1)
        DO j=1,my
        DO i=1,mx
          gradTz = min(gradTz,(tairDY(i,j,mzabs1)-tairDY(i,j,mzabso))
     .                 /(epsi+ gplvDY(i,j,mzabs1)-gplvDY(i,j,mzabso)))
        END DO
        END DO
      END IF
          gradTz =     gradTz *gravit *1.0d+3
C +
C +
C +--Sigma Surfaces Initial Altitudes for Linear Mountain Wave Experiments
C +  ---------------------------------------------------------------------
C +
c #OL DO k=1,mz
c #OL DO j=1,my
c #OL DO i=1,mx
c #OL   gp00OL(i,j,k) = gplvDY(i,j,k)
c #OL END DO
c #OL END DO
c #OL END DO
C +
C +
C +--Slopes of the Sigma Surfaces
C +  ----------------------------
C +
C +        ******
c _PE call INIpen
C +        ******
C +
C +        ******
c #PE call INIpen
C +        ******
C +
C +
C +--Top / Bottom Boundaries
C +  -----------------------
C +
c #Di DO j=1,my
c #Di DO i=1,mx
c #Di   qvtoDI(i,j) =   qvDY(i,j,1)
c #Di   pkttDI(i,j) = pktaDY(i,j,1)
c #Di   uairDI(i,j) = uairDY(i,j,1)
c #Di   vairDI(i,j) = vairDY(i,j,1)
c #Di END DO
c #Di END DO
C +
C +
C +--Specific Mass
C +  -------------
C +
C +    ***********
       call DYNrho
C +    ***********
C +
C +
C +--Surface     Variables
C +  =====================
C +
C +--Land-Sea Mask
C +  -------------
C +
      DO j=1,my
      DO i=1,mx
        IF (isolSL(i,j).le.2)                                     THEN
            maskSL(i,j) = 1
        ELSE
            maskSL(i,j) = 0
        END IF
      END DO
      END DO
 
 
C +--OUTPUT
C +  ======
 
C +   ---------------------
      if (IO_loc.ge.1) then
C +   ---------------------
 
       write(21,606) jdaMAR,jhaMAR,itexpe,nboucl,nprint,
     .               0.1*pSND(1,1),pstSND,sst_SL,dtagSL,
     .               zs_SL,zn_SL,gradtz,mzabso
  606  format(//,' EXECUTION STATUS',/,' ++++++++++++++++',/,
     .  /,' Preceding Execution stopped after',i5,' day(s)',
     .                                         i5,' hour(s)',
     .  /,'  itexpe =',i8,
     . //,'  nboucl =',i8,8x,'nprint =',i8,
     . //,'  p MSL  =',f8.1,' cb',5x,'p* 0   =',f8.1,' cb',
     .  /,'  SST    =',f8.1,' K ',5x,'T0-Tg  =',f8.1,' K ',
     .  /,'  zs     =',f8.5,' m ',5x,'zn     =',f8.5,' m ',
     .  /,'  dT/dz >=',f8.2,' K/km / mzabso =',i8,' Sponge Base')
       IF (gradtz.lt.-4.0) write(6,607) gradtz,mzabso
  607  format(/,' WARNING: dT/dz Min =',f8.2,' K/km',
     .          ' at Sponge Base (k=mzabso=',i2,')')
 
C +   ------
      end if
C +   ------
 
      return
      end

      integer function zext(logvar)
      logical logvar
C +
      if                   (logvar) then
                       zext=-1
      else
                       zext= 0
      end if
C +
      return
      end

