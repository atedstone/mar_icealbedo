

      subroutine icewcr

C +------------------------------------------------------------------------+
C | ICE WEATHERING CRUST                                 19-07-2019-AT MAR |
C |   SubRoutine icewcr is used to simulate bare ice sub-surface density   |
C |      via the balance between shortwave net radiation versus longwave   |
C |      and turbulent heat fluxes.                                        |
C |                                                                        |
C |    Hard-coded for mosaic pixel == 1                                    |
C |    Based on Schuster (2001), Weathering crust processes on melting     |
C |    glacier ice (Alberta, Canada). Wilfrid Laurier University. 489.     |  
C +------------------------------------------------------------------------+    

      implicit none


C +--Global Variables
C +  ================    

      include 'MARphy.inc'
      include 'MARdim.inc'
      include 'MAR_SV.inc'
      include 'MARxSV.inc' ! SISVAT px indexes
      include 'MARySV.inc'

!$OMP threadprivate(/xSISVAT_I/,/xSISVAT_R/,/ySISVAT_I/,/ySISVAT_R/)    


C +--Local  Variables
C +  ================

      real snh1, snh2
      integer index_ice, isn, ikl
      real SnHmax ! Snow height beneath which WC dynamics begin
      real rhoMin, rhoMax      ! Minimum and maximum densities, kg m-3
      integer iz               ! Loop pointer
      integer flg1, flg2
      real SignRo, SnowOK
      real K_star, K_star_new
      real e
      real e_bal
      real j_bal
      real t_chg
      
      real spinup(nice)
      real V_o(nice)          ! Volume of each vertical layer, m3
      real mass(nice)         ! Mass of each vertical layer, m3

      real Q_Mi(nice)         ! SWnet absorption per layer, W m-2
      real a(nice)            ! Absorption coefficient m-1
      real Q_Ma               ! Energy inputs for surface lowering
      real Q_r                ! Rainfall energy inputs for surface lowering
      real M_i(nice)          ! Mass of ice lost through internal melt, kg
      real M_a                ! Mass of ice lost through surface lowering, kg
      real M_c(nice)          ! Mass of ice lost through layer collapse, kg
      real V_a                ! Volume of ice lost through surface lowering, kg
      real V_c(nice)          ! Volume of ice lost through surface collapse, kg
      real V_loss(nice)       ! Cumulative volume of ice lost through column, kg
      real rho_old(nice)      ! Rho from last timestep kg m-3
      real currho(nice)
      real rho_under(nice+1)

      real t_flgs(nice)

      data    SnHmax / 0.1   /
      data    rhoMin / 350.0 /
      data    rhoMax / 920.0 /
      data    e      / 2.718281828459045 /
      
      ! Also prescribed in inigen.f
      spinup(1:9) = [350.,350.,430., 521., 596., 660., 715., 784., 847.]

      ! Fix ikl=1 (following XF email 27.06.2022)
      ikl=1

      ! Calculate snow thickness at this pixel
      snh1 = 0
      index_ice = 1
      do isn=1, isnoSV(ikl)
        if(ro__SV(ikl,isn)+10. .ge. ro_Ice) index_ice = isn
      enddo
      if (index_ice < isnoSV(ikl)) then
        do isn=index_ice, isnoSV(ikl)
          snh1 = snh1 + dzsnSV(ikl,isn)
        enddo
      endif
  
      ! Is snow thicker than tolerable threshold?
      snh2 = max(zero, (sign(unun, snh1 - SnHmax) * -1) )
       ! -- snh2 now =0 if snow present above threshold, =1 if bare/below threshold

      ! (Re-)set WC rho to spinup values if thick snow covering
C       if (snh2 .lt. unun) then
C         wcrhsv(ikl,:) = spinup
C         wctisv(ikl) = -1.
C       endif

      ! Check whether surface bare ice
      isn    =  max(min(isnoSV(ikl) ,ispiSV(ikl)),0)
      SignRo = sign(unun,ro_Ice-5.-ro__SV(ikl,isn))
      SnowOK =  max(zero,SignRo)
      ! --> evaluates to 0 if ice, 1 if snow.

C      if (SnowOK .lt. unun) then
      if (snh2 .gt. zero) then

C +--Execute model 
C +  =============

        ! Volume calculation assumes width and length 1 m
        V_o(1:nice) = lyrthk(1:nice)

        rho_old(1:nice) = wcrhsv(ikl,:)
      
        mass(1:nice) = V_o * rho_old

        ! Calculate SWnet energy absorption at each layer depth
        K_star = sol_SV(ikl) * (1 - albisv(ikl))

        ! Define extinction coefficients for sub-surface layers
        a(1) = 1.004e-5 * 
     .       ( biomSV(ikl) * MAX(0.4, MIN(unun, 
     .       (-122.56 * slopSV(ikl) + 2.6027))))
     .     + 4.2967
     .     - (0.81 * cld_SV(ikl)) !cld_SV varies 0--1. Cloudy coefficient is 0.8 less than clear.   (See clear_vs_cloudy_sky.csv)
        a(2:nice) = MAX(2., MIN(4., (-0.0036*rho_old(2:nice) + 5.2727))) !(where a=2 @900kgm3, a=4@350kgm3)

        do iz=1,nice
          K_star_new = K_star * e**( -a(iz) * lyrthk(iz) )
          Q_Mi(iz) = (K_star - K_star_new) * dt__SV
          K_star = K_star_new
        enddo

        ! Calculate surface lowering
        Q_Ma = ( (hlatSV(ikl)*-1)
     .           + (hsenSV(ikl)*-1) 
     .           + (IRd_SV(ikl)+IRupsv(ikl)) ) !upward LW is -ve
     .        * dt__SV

        ! Energy input due to rainfall
        ! Fitzpatrick et al. (2017, Frontiers) after Hay & Fitzharris (1988)
        Q_r = (1000.*4180. * drr_SV(ikl) 
     .             * (TaT_SV(ikl) - TfSnow)) 
     .           * dt__SV

        ! Temperatures of layers being used in WC model
        t_flgs = 0
        ! WC model is 0-indexed from surface, MAR 0 is at depth, so reverse...
        isn = isnoSV(ikl)-nice
        WHERE (TsisSV(1, isnoSV(ikl):isn:-1) >= 273.10) 
          t_flgs = 1
        END WHERE

        ! Mass of melting by radiative penetration at each layer
        M_i = (unun / Lf_H2O) * Q_Mi * t_flgs

        ! Mass of melting in uppermost layer, including by rainfall
        Q_Ma = Q_Ma * t_flgs(1)
        Q_Ma = Q_Ma + Q_r

        M_c(1:nice) = 0
        WHERE(rho_old <= rhoMin) 
          M_c = M_i
          M_i = zero
        END WHERE

        ! Surface lowering in uppermost layer
        M_a = MAX(zero, (unun / Lf_H2O) * Q_Ma) * t_flgs(1)

        ! Volume of ice lost through surface lowering
        V_a = M_a / rho_old(1)

        ! Volume of ice lost through layer collapse (all layers)
        V_c = M_c / rho_old

        ! Compute new values of rho
        currho = (mass-M_i) / V_o 

        ! Cumulative volume loss
        ! See Numerical Recipes in Fortran 90 Vol. 2 Pg 997
        V_loss(:) = zero
        V_loss(1) = V_c(1)
        do iz=2,nice
          V_loss(iz) = V_loss(iz-1) + V_c(iz)
        enddo  
        V_loss = V_loss + V_a

        ! Replenishment of each layer volume using volume from layer below
        rho_under(1:nice) = currho
        rho_under(nice+1) = rhoMax
        do iz=1,nice    
          ! Input density for the next time period
          wcrhsv(ikl,iz) =(currho(iz)*(unun-(V_loss(iz) / V_o(iz))))
     .                     + (rho_under(iz+1) * (V_loss(iz) / V_o(iz)))   
        enddo



      

CC    FOR DEBUGGING:      
C        if (ii__SV(ikl) .eq. 40 .and. jj__SV(ikl) .eq. 77) then
C         write(6,*) 'tsisSV:', TsisSV
C         write(6,*) 'isn', isnoSV(ikl)-nice
C         write(6,*) 'tsisSV WC', TsisSV(1, isnoSV(ikl):isn:-1)
C         write(6,*) 't_flgs', t_flgs
C         write(6,*) 'rho', wcrhsv(ikl,:)

C       write(6,*) 'dt__SV:', dt__SV
C       write(6,*) 'snh1,2:', snh1, snh2
C       write(6,*) 'snowpack:', ro__SV(ikl,isn), ispiSV(ikl), isnoSV(ikl)
C       write(6,*) 'SnowOK:', SnowOK
C       write(6,*) 'Fluxes:', (sol_SV(ikl) * (1 - albisv(ikl))), 
C      .  hlatSV(ikl)*-1, hsenSV(ikl)*-1, IRd_SV(ikl)+IRupsv(ikl)
C       write(6,*) 'cld_SV:', cld_SV(ikl)
C       write(6,*) 'a: ', a
C       write(6,*) 'Q_Mi:', Q_Mi
C       write(6,*) 'Q_Ma:', Q_Ma
C c      write(6,*) 'BlocT:', wctisv(ikl), 'chg:', t_chg
C       write(6,*) 'M_i:', M_i
C       write(6,*) 'M_c:', M_c
C       write(6,*) 'M_a:', M_a
C       write(6,*) 'V_loss:', V_loss
C       write(6,*) 'Old rho:', rho_old
C       write(6,*) 'Rho before replenishment:', currho
C       write(6,*) 'New rho:', wcrhsv
C       write(6,*) 'AVG:', SUM(wcrhsv(ikl,:)
C      .     * (unun / SUM(lyrthk) * lyrthk))
C       write(6,*) ' '
C       endif

      endif !SnowOK

      return
      end